our %settings =
	(
	 'blog_title'             => 'Mash Blog',			# タイトル
	 
	 'password_min'           => 4,						# パスワード 最小文字数
	 
	 'cookie_prefix'          => 'mash_blog',			# Cookie名接頭辞
	 
	 'template_dir'           => './template',			# テンプレートファイルが置かれているディレクトリ
	 'tag_module_dir'         => "./tag",				# タグモジュールファイルが置かれているディレクトリ

	 'db_root_path'           => './.db/',				# データベース保存場所ルートディレクトリのパス
	 
	 'login_auto'             => 'enable',				# 「自動ログイン」許可デフォルト('enable' or 'disable')
	 'login_auto_keep_day'    => 30,					# 「自動ログイン」時、Cookieを維持する日数
	 
	 'crypt_salt'             => 'MH',					# 暗号化キー（任意の2文字）
	 
	 'search_type'            => 'and',					# 検索タイプ デフォルト('and' or 'or')
	 'search_item'            => ['text','title'],		# 検索項目 デフォルト('text', 'title' or 'tag')
	 
	 'entry_public_type'      => 'public',				# エントリ 公開設定デフォルト('public','reserve' or 'private')
	 'entry_comment'          => 'approval',			# エントリ コメント許可デフォルト('enable','approval' or 'disable')
	 'entry_trackback'        => 'approval',			# エントリ トラックバック許可デフォルト('enable','approval' or 'disable')
	 'entry_title_min'        => 1,						# エントリ タイトル最小文字数(0の場合は未入力可)
	 'entry_title_max'        => 0,						# エントリ タイトル最大文字数(0の場合は無制限)
	 'entry_text_min'         => 1,						# エントリ 本文最小文字数(0の場合は未入力可)
	 'entry_text_max'         => 0,						# エントリ 本文最大文字数(0の場合は無制限)
	 'entry_category_num_min' => 0,						# エントリ カテゴリ最小数(0の場合は未入力可)
	 'entry_tag_num_min'      => 0,						# エントリ タグ最小数(0の場合は未入力可)
	 'entry_tag_min'          => 0,						# エントリ タグ最小文字数(0の場合は未入力可)
	 'entry_tag_max'          => 0,						# エントリ タグ最大文字数(0の場合は無制限)
	 
	 'entry_list_page_num'    => 5,						# エントリリスト 1ページあたりのエントリ表示数
	 
	 'comment_name'           => 'ゲスト',				# コメント 名前デフォルト
	 'comment_name_min'       => 1,						# コメント 名前最小文字数(0の場合は未入力可)
	 'comment_name_max'       => 100,					# コメント 名前最大文字数(0の場合は無制限)
	 'comment_mail_min'       => 0,						# コメント メールアドレス最小文字数(0の場合は未入力可)
	 'comment_mail_max'       => 100,					# コメント メールアドレス最大文字数(0の場合は無制限)
	 'comment_url_min'        => 0,						# コメント URL最小文字数(0の場合は未入力可)
	 'comment_url_max'        => 200,					# コメント URL最大文字数(0の場合は無制限)
	 'comment_title_min'      => 0,						# コメント タイトル最小文字数(0の場合は未入力可)
	 'comment_title_max'      => 100,					# コメント タイトル最大文字数(0の場合は無制限)
	 'comment_text_min'       => 1,						# コメント 本文最小文字数(0の場合は未入力可)
	 'comment_text_max'       => 2000,					# コメント 本文最大文字数(0の場合は無制限)
	 'comment_keep_day'       => 30,					# コメント 名前・メールアドレスのCookieへの保存を維持する日数
	 
	 'file_size_max'          => 10 * 1024 * 1024,		# ファイル 最大サイズ(0の場合は無制限、ただし非推奨)
	 'file_name_max'          => 100,					# ファイル 名前最大文字数(0の場合は無制限、ただし使用するOSの上限がある)
	 'file_comment_min'       => 0,						# ファイル コメント最小文字数(0の場合は未入力可)
	 'file_comment_max'       => 100,					# ファイル コメント最大文字数(0の場合は無制限)
	 'file_root_path'         => './.file/',			# ファイル 保存場所ルートディレクトリのパス
	 
	 'file_list_page_num'     => 50,					# ファイルリスト 1ページあたりのファイル情報表示数
	 
	 'log_db_root_path'       => './.log/',				# ログデータベース保存場所ルートディレクトリのパス
	 'log_exclusion_addr'     => [],					# ログ 指定アドレスからのアクセスは除外
	 													#  CIDR形式(aaa.bbb.ccc.ddd/xx)での指定可
	 													#  プライベートアドレスを除外する場合は "10.0.0.0/8", "172.16.0.0/12", "192.168.0.0/16" のいずれかを指定する
	 'log_exclusion_host'     => [],					# ログ 指定ホストからのアクセスは除外
	 													#  *込み(*.example.com)での指定可
	 'log_exclusion_mode'     => ['log'],				# ログ 指定モードのアクセスは除外
	 'log_exclusion_user'     => [],					# ログ 指定ユーザーのアクセスは除外
	 
	 'deny_access_addr'       => [],					# 指定アドレスからのアクセスは不可
	 													#  CIDR形式(aaa.bbb.ccc.ddd/xx)での指定可
	 'deny_access_host'       => [],					# 指定ホストからのアクセスは不可
	 													#  *込み(*.example.com)での指定可
	 
	 'deny_comment_addr'      => [],					# 指定アドレスからのコメントは不可
	 													#  CIDR形式(aaa.bbb.ccc.ddd/xx)での指定可
	 'deny_comment_host'      => [],					# 指定ホストからのコメントは不可
	 													#  *込み(*.example.com)での指定可
	 
	 'deny_trackback_addr'    => [],					# 指定アドレスからのトラックバックは不可
	 													#  CIDR形式(aaa.bbb.ccc.ddd/xx)での指定可
	 'deny_trackback_host'    => [],					# 指定ホストからのトラックバックは不可
	 													#  *込み(*.example.com)での指定可
	);

1;
