#!/usr/bin/perl -w

###############################################################################
#
#  blog.cgi
#
#    author : Masashi Haraki
#
###############################################################################

######## 設定・モジュール ########

use strict;
#use utf8;
#use open ":utf8";
#use open ":std";
#binmode STDOUT,":utf8";

BEGIN { unshift(@INC, './module'); }

use Encode;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
use File::Path;
use Mash::MashDB;
use Mash::MashTime;
use Mash::MashDebug;

use utility;
use cookie;
use database;
use template;

use log;
use account;
use category;
use entry;
use comment;
use trackback;
use file;

######## 定数・変数 ########

my $start_time = (times())[0];

#### 設定 ####

require 'settings.cgi';
our %settings;

require 'locale_settings.cgi';
our %locale_settings;

#### システム設定 ####

my $charset  = 'utf-8';					# 文字コード
my $language = 'ja';					# 言語

my $this_cgi = 'blog.cgi';
my $version  = 'Mash Blog Engine version 0.82';		# バージョン

my $crypt_salt = $settings{'crypt_salt'};	# 暗号化キー（任意の2文字）

#### リソース ####

require 'resource/string.cgi';
our %res_string;

#### エントリ ####

my $cur_entry_id   = '';

my $archive_type        = '';
my $archive_param       = '';
my $archive_page        = 0;
my $archive_search      = 0;
my $archive_search_type = $settings{'search_type'};
my @archive_search_item = @{$settings{'search_item'}};

#### コメント ####

my $cur_comment_id         = '';
my $comment_changed_status = '';

#### トラックバック ####

my $cur_trackback_id = '';

#### カテゴリ ####

my $cur_category_id = '';

my $category_regist_name        = '';
my $category_regist_parent_name = '';
my $category_rename_name        = '';
my $category_rename_before_name = '';
my $category_delete_name        = '';

#### アカウント ####

my $cur_account_id = '';

#### ファイル ####

my $file_regist_name = '';									# ファイル 登録名
my $file_size_max    = $settings{'file_size_max'};			# ファイル 最大サイズ(0の場合は無制限、ただし非推奨)
my $file_name_max    = $settings{'file_name_max'};			# ファイル 名前最大文字数(0の場合は無制限、ただし使用するOSの上限がある)
my $file_comment_min = $settings{'file_comment_min'};		# ファイル コメント最小文字数(0の場合は未入力可)
my $file_comment_max = $settings{'file_comment_max'};		# ファイル コメント最大文字数(0の場合は無制限)
my $file_root_path   = $settings{'file_root_path'};			# ファイル 保存場所ルートディレクトリのパス

my $file_list_page_num = $settings{'file_list_page_num'};	# ファイルリスト 1ページあたりのファイル情報表示数

#### ログ ####

my $cur_log_date = '';

#### ログイン ####

my $login_user_id   = '';							# ログインしているユーザーID
my $login_user_pass = '';							# ログインしているユーザパスワード
my $login_auto      = $settings{'login_auto'};		# 自動ログイン
my $login_name      = '';							# ログイン名
my $login_type      = '';							# ログインタイプ('admin','normal','guest' or 'error')

#### システムワーク ####

my $blog_title    = $settings{'blog_title'};		# ブログタイトル

my $output_str = '';			# 出力文字列

my $cgi_query;					# CGI.pm のオブジェクト

my $mode = '';					# モード
my $parse_permission = '';		# タグ解析時パーミッション

my $db_root_path     = $settings{'db_root_path'};			# データベース保存場所ルートディレクトリのパス
my $log_db_root_path = $settings{'log_db_root_path'};		# ログデータベース保存場所ルートディレクトリのパス

my $entry_title_min        = $settings{'entry_title_min'};			# エントリ タイトル最小文字数(0の場合は未入力可)
my $entry_title_max        = $settings{'entry_title_max'};			# エントリ タイトル最大文字数(0の場合は無制限)
my $entry_text_min         = $settings{'entry_text_min'};			# エントリ 本文最小文字数(0の場合は未入力可)
my $entry_text_max         = $settings{'entry_text_max'};			# エントリ 本文最大文字数(0の場合は無制限)
my $entry_category_num_min = $settings{'entry_category_num_min'};	# エントリ カテゴリ最小数(0の場合は未入力可)
my $entry_tag_num_min      = $settings{'entry_tag_num_min'};		# エントリ タグ最小数(0の場合は未入力可)
my $entry_tag_min          = $settings{'entry_tag_min'};			# エントリ タグ最小文字数(0の場合は未入力可)
my $entry_tag_max          = $settings{'entry_tag_max'};			# エントリ タグ最大文字数(0の場合は無制限)

my $entry_list_page_num    = $settings{'entry_list_page_num'};		# エントリリスト 1ページあたりのエントリ表示数

my $comment_name_min  = $settings{'comment_name_min'};				# コメント 名前最小文字数
my $comment_name_max  = $settings{'comment_name_max'};				# コメント 名前最大文字数
my $comment_mail_min  = $settings{'comment_mail_min'};				# コメント メールアドレス最小文字数
my $comment_mail_max  = $settings{'comment_mail_max'};				# コメント メールアドレス最大文字数
my $comment_url_min   = $settings{'comment_url_min'};				# コメント URL最小文字数
my $comment_url_max   = $settings{'comment_url_max'};				# コメント URL最大文字数
my $comment_title_min = $settings{'comment_title_min'};				# コメント タイトル最小文字数
my $comment_title_max = $settings{'comment_title_max'};				# コメント タイトル最大文字数
my $comment_text_min  = $settings{'comment_text_min'};				# コメント 本文最小文字数
my $comment_text_max  = $settings{'comment_text_max'};				# コメント 本文最大文字数

my $comment_name_mail_save = 'disable';								# コメント 名前・メールアドレス保存

my $password_min = $settings{'password_min'};					# パスワード 最小文字数

my @error_msg_list = ();		# エラーメッセージ

my %system_param =
	(
	 'this_cgi'     => \$this_cgi,
	 'version'      => \$version,
	 'crypt_salt'   => \$crypt_salt,
	 'blog_title'   => \$blog_title,
	 'mode'         => \$mode,
	 'permission'   => \$parse_permission,
	 
	 'db_root_path'     => \$db_root_path,
	 'log_db_root_path' => \$log_db_root_path,
	 
	 'cur_entry_id' => \$cur_entry_id,
	 
	 'archive_type'        => \$archive_type,
	 'archive_param'       => \$archive_param,
	 'archive_page'        => \$archive_page,
	 
	 'archive_search'      => \$archive_search,
	 'archive_search_type' => \$archive_search_type,
	 'archive_search_item' => \@archive_search_item,
	 
	 'cur_comment_id'         => \$cur_comment_id,
	 'comment_changed_status' => \$comment_changed_status,
	 
	 'entry_title_min'        => \$entry_title_min,
	 'entry_title_max'        => \$entry_title_max,
	 'entry_text_min'         => \$entry_text_min,
	 'entry_text_max'         => \$entry_text_max,
	 'entry_category_num_min' => \$entry_category_num_min,
	 'entry_tag_num_min'      => \$entry_tag_num_min,
	 'entry_tag_min'          => \$entry_tag_min,
	 'entry_tag_max'          => \$entry_tag_max,
	 
	 'entry_list_page_num'    => \$entry_list_page_num,
	 
	 'comment_name_mail_save' => \$comment_name_mail_save,
	 'comment_name_min'       => \$comment_name_min,
	 'comment_name_max'       => \$comment_name_max,
	 'comment_mail_min'       => \$comment_mail_min,
	 'comment_mail_max'       => \$comment_mail_max,
	 'comment_url_min'        => \$comment_url_min,
	 'comment_url_max'        => \$comment_url_max,
	 'comment_title_min'      => \$comment_title_min,
	 'comment_title_max'      => \$comment_title_max,
	 'comment_text_min'       => \$comment_text_min,
	 'comment_text_max'       => \$comment_text_max,
	 
	 'cur_trackback_id' => \$cur_trackback_id,
	 
	 'cur_account_id' => \$cur_account_id,

	 'login_type'      => \$login_type,
	 'login_user_id'   => \$login_user_id,
	 'login_user_pass' => \$login_user_pass,
	 'login_name'      => \$login_name,
	 'login_auto'      => \$login_auto,
	 
	 'password_min' => \$password_min,
	 
	 'cur_category_id' => \$cur_category_id,
	 
	 'category_regist_name'        => \$category_regist_name,
	 'category_regist_parent_name' => \$category_regist_parent_name,
	 'category_rename_name'        => \$category_rename_name,
	 'category_rename_before_name' => \$category_rename_before_name,
	 'category_delete_name'        => \$category_delete_name,
	 
	 'file_regist_name' => \$file_regist_name,
	 'file_size_max'    => \$file_size_max,
	 'file_name_max'    => \$file_name_max,
	 'file_comment_min' => \$file_comment_min,
	 'file_comment_max' => \$file_comment_max,
	 'file_root_path'   => \$file_root_path,
	 
	 'cur_log_date' => \$cur_log_date,
	 
	 'error_msg' => \@error_msg_list,
	);

our %mode_table =
	(
	 'archive'                => {'func' => \&mode_archive,               'template' => "archive_template.html",              'query_check' => 0, 'user_type' => [],                  },
	 
	 'login'                  => {'func' => \&mode_login,                 'template' => "login_template.html",                'query_check' => 0, 'user_type' => [],                  },
	 'logout'                 => {'func' => \&mode_logout,                'template' => "",                                   'query_check' => 0, 'user_type' => [],                  },
	 
	 'entry_edit'             => {'func' => \&mode_entry_edit,            'template' => "entry_edit_template.html",           'query_check' => 0, 'user_type' => ['admin', 'normal'], },
	 'entry_check'            => {'func' => \&mode_entry_check,           'template' => "entry_check_template.html",          'query_check' => 1, 'user_type' => ['admin', 'normal'], },
	 'entry_regist'           => {'func' => \&mode_entry_regist,          'template' => "entry_regist_template.html",         'query_check' => 0, 'user_type' => ['admin', 'normal'], },
	 
	 'comment_edit'           => {'func' => \&mode_comment_edit,          'template' => "comment_edit_template.html",         'query_check' => 0, 'user_type' => [],                  },
	 'comment_check'          => {'func' => \&mode_comment_check,         'template' => "comment_check_template.html",        'query_check' => 0, 'user_type' => [],                  },
	 'comment_regist'         => {'func' => \&mode_comment_regist,        'template' => "comment_regist_template.html",       'query_check' => 0, 'user_type' => [],                  },
	 
	 'comment_admin'          => {'func' => \&mode_comment_admin,         'template' => "comment_admin_template.html",        'query_check' => 0, 'user_type' => ['admin', 'normal'], },
	 'comment_status_change'  => {'func' => \&mode_comment_status_change, 'template' => "",                                   'query_check' => 1, 'user_type' => ['admin', 'normal'], },
	 
	 'category'               => {'func' => \&mode_category,              'template' => "category_template.html",             'query_check' => 0, 'user_type' => ['admin', 'normal'], },
	 'category_regist'        => {'func' => \&mode_category_regist,       'template' => "",                                   'query_check' => 1, 'user_type' => ['admin', 'normal'], },
	 'category_rename'        => {'func' => \&mode_category_rename,       'template' => "",                                   'query_check' => 1, 'user_type' => ['admin', 'normal'], },
	 'category_delete'        => {'func' => \&mode_category_delete,       'template' => "",                                   'query_check' => 1, 'user_type' => ['admin', 'normal'], },
	 
	 'account'                => {'func' => \&mode_user_common,           'template' => "account_template.html",              'query_check' => 0, 'user_type' => ['admin', 'normal'], },
	 'account_edit'           => {'func' => \&mode_account_edit,          'template' => "account_edit_template.html",         'query_check' => 0, 'user_type' => ['admin'],           },
	 'account_check'          => {'func' => \&mode_account_check,         'template' => "account_check_template.html",        'query_check' => 1, 'user_type' => ['admin'],           },
	 'account_rename'         => {'func' => \&mode_account_edit,          'template' => "account_edit_template.html",         'query_check' => 0, 'user_type' => ['admin', 'normal'], },
	 'account_rename_check'   => {'func' => \&mode_account_check,         'template' => "account_check_template.html",        'query_check' => 1, 'user_type' => ['admin', 'normal'], },
	 'account_regist'         => {'func' => \&mode_account_regist,        'template' => "account_regist_template.html",       'query_check' => 0, 'user_type' => ['admin', 'normal'], },
	 'account_delete_check'   => {'func' => \&mode_account_delete_check,  'template' => "account_delete_check_template.html", 'query_check' => 0, 'user_type' => ['admin', 'normal'], },
	 'account_delete'         => {'func' => \&mode_account_delete,        'template' => "account_delete_template.html",       'query_check' => 0, 'user_type' => [],                  },
	 
	 'file'                   => {'func' => \&mode_file,                  'template' => "file_template.html",                 'query_check' => 0, 'user_type' => ['admin', 'normal'], },
	 'file_regist'            => {'func' => \&mode_file_regist,           'template' => "",                                   'query_check' => 1, 'user_type' => ['admin', 'normal'], },
	 'file_update'            => {'func' => \&mode_file_update,           'template' => "",                                   'query_check' => 1, 'user_type' => ['admin', 'normal'], },
	 'file_delete'            => {'func' => \&mode_file_delete,           'template' => "",                                   'query_check' => 1, 'user_type' => ['admin', 'normal'], },
	 
	 'tag_list_show_entry'    => {'func' => \&mode_tag_list_show,         'template' => "tag_list_show_template.html",        'query_check' => 0, 'user_type' => ['admin', 'normal'], },
	 'tag_list_show_comment'  => {'func' => \&mode_tag_list_show,         'template' => "tag_list_show_template.html",        'query_check' => 0, 'user_type' => [],                  },
	 
	 'log'                    => {'func' => \&mode_log,                   'template' => "log_template.html",                  'query_check' => 0, 'user_type' => ['admin', 'normal'], },
	 
	 'startup'                => {'func' => \&mode_startup,               'template' => "startup_template.html",              'query_check' => 0, 'user_type' => [],                  },
	 'startup_account_edit'   => {'func' => \&mode_account_edit,          'template' => "account_edit_template.html",         'query_check' => 0, 'user_type' => [],                  },
	 'startup_account_check'  => {'func' => \&mode_account_check,         'template' => "account_check_template.html",        'query_check' => 0, 'user_type' => [],                  },
	 'startup_account_regist' => {'func' => \&mode_account_regist,        'template' => "account_regist_template.html",       'query_check' => 0, 'user_type' => [],                  },
	 
	 'error'                  => {'func' => \&mode_error,                 'template' => "error_template.html",                'query_check' => 0, 'user_type' => [],                  },
	);

our $mash_dbg = new Mash::MashDebug();

main();

=pod

$mash_dbg->add_log_value("\n-------- SYSTEM_PARAM --------\n");
$mash_dbg->add_log_hash(\%system_param, 0);
$mash_dbg->add_log_value("\n-------- ACCOUNT_DB --------\n");
$account_db->add_log(\$mash_dbg);
$mash_dbg->add_log_value("\n-------- CATEGORY_DB --------\n");
$category_db->add_log(\$mash_dbg);
$mash_dbg->add_log_value("\n-------- ENTRY_DB --------\n");
$entry_db->add_log(\$mash_dbg);
$mash_dbg->add_log_value("\n-------- COMMENT_DB --------\n");
$comment_db->add_log(\$mash_dbg);

=cut

print "<!--\n";

$mash_dbg->print_log();

my $cgi_time = (times())[0] - $start_time;
print "\ncgi_time = $cgi_time\n";

print "-->\n";

exit;

######## メイン ######## 

sub main
{
	$cgi_query = new CGI();
	$cgi_query->charset($charset);
	
	init_cookie($settings{'cookie_prefix'});
	
	init_template($settings{'template_dir'}, $settings{'tag_module_dir'});
	
	#for my $p ($cgi_query->param)
	#{
	#	my @v = map {decode_utf8($_)} $cgi_query->param($p);
	#	
	#	$cgi_query->param($p, @v);
	#}
	
	$mode = $cgi_query->param('mode');
	
	if(!(defined($mode)))
	{
		$mode = '';
	}
	
	my $login_success = 0;
	
	if($mode ne 'error')
	{
		if(make_dir($db_root_path) < 0)
		{
			print make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_dir_error');
			
			exit;
		}
		
		if(lock_db() != 0)
		{
			print make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_lock_error');
			
			exit;
		}
		
		if(init_account_db($db_root_path) != 0)
		{
			print make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_read_error');
			
			unlock_db();
			
			exit;
		}
		
		unlock_db();
		
		if($mode !~ m/^startup/)
		{
			if(get_account_db_list_size() == 0)
			{
				# アカウントデータが無い場合は強制的にスタートアップ画面へ
				$mode = 'startup';
			}
			elsif($mode eq '')
			{
				$mode = 'archive';
			}
		}
		
		if(check_login(${$mode_table{$mode}}{'query_check'}, \$output_str, @{${$mode_table{$mode}}{'user_type'}}) == 0)
		{
			$login_success = 1;
		}
	}
	
	update_log();
	
	if(!exists(${$mode_table{$mode}}{'func'}))
	{
		print make_location_str($this_cgi, 'mode' => 'error', 'type' => 'param_error');
		
		exit;
	}
	
	if($mode ne 'error')
	{
		if((check_addr($ENV{'REMOTE_ADDR'}, \@{$settings{'deny_access_addr'}})) || (check_host($ENV{'REMOTE_HOST'}, \@{$settings{'deny_access_host'}})))
		{
			print make_location_str($this_cgi, 'mode' => 'error', 'type' => 'deny_access_error');
			
			exit;
		}
		elsif($login_success == 0)
		{
			print make_location_str($this_cgi, 'mode' => 'error', 'type' => 'login_stat_error');
			
			exit;
		}
	}
	
	${$mode_table{$mode}}{'func'}(\$output_str);
	
	print $output_str;
}

######## 各モード処理 ######## 

sub mode_archive
{
	my $ref_output_str = $_[0];
	
	if(lock_db() != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_lock_error');
		
		return;
	}
	
	if((init_category_db($db_root_path)  != 0) ||
	   (init_entry_db($db_root_path)     != 0) ||
	   (init_comment_db($db_root_path)   != 0) ||
	   (init_trackback_db($db_root_path) != 0) ||
	   (init_file_db($db_root_path)      != 0))
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_read_error');
		
		unlock_db();
		
		return;
	}
	
	# 予約中から公開に変更されるエントリがあるかチェック
	check_reserve_entry();
	
	unlock_db();
	
	my $entry_id = $cgi_query->param('id');
	my $search   = $cgi_query->param('search');
	
	if((defined($entry_id)) && ($entry_id ne ''))
	{
		$archive_type = 'id';
		$cur_entry_id = $entry_id;
		
		my $comment_name = '';
		my $comment_mail = '';
		
		get_cookie_comment(\$comment_name, \$comment_mail);
		
		if($comment_name ne '')
		{
			set_current_comment_hash('name' => $comment_name, 'mail' => $comment_mail);
			
			$comment_name_mail_save = 'enable';
		}
		else
		{
			set_current_comment_hash('name' => $login_name);
		}
	}
	else
	{
		if((defined($search)) && ($search ne ''))
		{
			$archive_param       = $search;
			$archive_search      = 1;
			$archive_search_type = $cgi_query->param('search_type');
			@archive_search_item = $cgi_query->param('search_item');
		}
		else
		{
			my $user_id     = $cgi_query->param('user_id');
			my $category_id = $cgi_query->param('category_id');
			my $date        = $cgi_query->param('date');
			my $tag         = $cgi_query->param('tag');
			
			if((defined($user_id)) && ($user_id ne ''))
			{
				$archive_type  = 'user_id';
				$archive_param = $user_id;
			}
			elsif((defined($category_id)) && ($category_id ne ''))
			{
				$archive_type  = 'category_id';
				$archive_param = $category_id;
			}
			elsif((defined($date)) && ($date ne ''))
			{
				$archive_type  = 'date';
				$archive_param = $date;
			}
			elsif((defined($tag)) && ($tag ne ''))
			{
				$archive_type  = 'tag';
				$archive_param = $tag;
			}
		}
		
		$archive_page = $cgi_query->param('page');
		if(!(defined($archive_page)) || (($archive_page eq '') || ($archive_page =~ m/[^0-9]/)))
		{
			$archive_page = 0;
		}
	}
	
	$$ref_output_str .= make_content_type_str('charset' => $charset) . make_html($mode);
}

sub mode_login
{
	my $ref_output_str = $_[0];
	my $auto = '';
	
	$login_user_id   = $cgi_query->param('user_id');
	$login_user_pass = $cgi_query->param('user_pass');
	if($login_user_pass ne '')
	{
		$login_user_pass = substr(crypt($login_user_pass, $crypt_salt), 2);
	}
	
	$auto = $cgi_query->param('auto');
	
	get_login_param($login_user_id, $login_user_pass, \$login_type, \$login_name);
	
	if(($login_type eq 'admin') || ($login_type eq 'normal'))
	{
		# ログイン成功時はID・パスワード・自動ログインをCookieに保存
		$$ref_output_str .= make_cookie_login_success($login_user_id, $login_user_pass, $auto);
		
		# ログイン成功時はTOPにリダイレクト
		$$ref_output_str .= make_location_str($this_cgi);
	}
	else
	{
		if($login_type eq 'error')
		{
			add_error_msg(\$res_string{'login_error'});
		}
		
		$$ref_output_str .= make_content_type_str('charset' => $charset) . make_html($mode);
	}
}

sub mode_logout
{
	my $ref_output_str = $_[0];
	
	# ログインCookieをクリアしてTOPにリダイレクト
	$$ref_output_str .= make_cookie_login('', '', '', -1);
	
	$$ref_output_str .= make_location_str($this_cgi);
}

sub mode_entry_edit
{
	my $ref_output_str = $_[0];
	
	if(lock_db() != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_lock_error');
		
		return;
	}
	
	if((init_category_db($db_root_path) != 0) ||
	   (init_entry_db($db_root_path)    != 0))
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_read_error');
		
		unlock_db();
		
		return;
	}
	
	unlock_db();
	
	my $error_type = '';
	
	$cur_entry_id = $cgi_query->param('id');
	
	if($cur_entry_id >= 1)
	{
		# 作成済みエントリを編集
		my $entry_user_id = get_entry_data($cur_entry_id, 'user_id');
		
		if((!defined($entry_user_id)) || ($entry_user_id eq ''))
		{
			# エントリID指定エラー
			$error_type = "entry_id_error";
		}
		elsif($entry_user_id ne $login_user_id)
		{
			# ユーザーIDエラー（エントリを編集する権限がない）
			$error_type = "entry_auth_error";
		}
	}
	else
	{
		# 新規エントリ
		$cur_entry_id = -1;
	}
	
	if($error_type eq '')
	{
		my $return            = $cgi_query->param('return');
		my $entry_public_type = $cgi_query->param('public_type');
		my $entry_date        = $cgi_query->param('date');
		my $entry_time        = $cgi_query->param('time');
		my $entry_comment     = $cgi_query->param('comment');
		my $entry_trackback   = $cgi_query->param('trackback');
		my @category_id_list  = ();
		my @tag_list          = ();
		
		if($entry_public_type eq '')
		{
			$entry_public_type = $settings{'entry_public_type'};
		}
		
		if($entry_comment eq '')
		{
			$entry_comment = $settings{'entry_comment'};
		}
		
		if($entry_trackback eq '')
		{
			$entry_trackback = $settings{'entry_trackback'};
		}
		
		if(($entry_date eq '') || ($entry_time eq '') || ($cur_entry_id < 0))
		{
			Mash::MashTime::get_current_date_time(\$entry_date, \$entry_time);
		}
		
		if((defined($return)) && ($return eq 'true'))
		{
			my $tag_str = $cgi_query->param('tag');
			
			$tag_str =~ s/^\s*//;
			$tag_str =~ s/\s*$//;
			@tag_list = split(/\s+/, $tag_str);
			
			@category_id_list = $cgi_query->param('category_id');
			
			set_current_entry_data($cur_entry_id,
								   $login_user_id,
								   $entry_public_type,
								   $entry_date,
								   $entry_time,
								   $cgi_query->param('title'),
								   $cgi_query->param('text'),
								   \@category_id_list,
								   \@tag_list,
								   $entry_comment,
								   $entry_trackback,
								   0);
		}
		elsif($cur_entry_id >= 1)
		{
			set_current_entry_data_existing($cur_entry_id);
		}
		else
		{
			set_current_entry_data($cur_entry_id,
								   $login_user_id,
								   $entry_public_type,
								   $entry_date,
								   $entry_time,
								   '',
								   '',
								   \@category_id_list,
								   \@tag_list,
								   $entry_comment,
								   $entry_trackback,
								   0);
		}
		
		$$ref_output_str .= make_content_type_str('charset' => $charset) . make_html($mode)
	}
	else
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => $error_type);
	}
}

sub mode_entry_check
{
	my $tmp_str        = '';
	my $ref_output_str = $_[0];
	
	if(($cgi_query->param('regist') eq '') && ($cgi_query->param('check') eq ''))
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'unknown_error');
		
		return;
	}
	
	if(lock_db() != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_lock_error');
		
		return;
	}
	
	if((init_category_db($db_root_path) != 0) ||
	   (init_entry_db($db_root_path)    != 0) ||
	   (init_comment_db($db_root_path)  != 0) ||
	   (init_file_db($db_root_path)     != 0))
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_read_error');
		
		unlock_db();
		
		return;
	}
	
	my $result           = 0;
	my $entry_edit_id    = $cgi_query->param('id');
	my $entry_date       = $cgi_query->param('date');
	my $entry_time       = $cgi_query->param('time');
	my @category_id_list = $cgi_query->param('category_id');
	my $tag_str          = $cgi_query->param('tag');
	my @tag_list         = ();
	
	$tag_str =~ s/^\s*//;
	$tag_str =~ s/\s*$//;
	@tag_list = split(/\s+/, $tag_str);
	
	if(($entry_date eq '') || ($entry_time eq ''))
	{
		Mash::MashTime::get_current_date_time(\$entry_date, \$entry_time);
	}
	
	set_current_entry_data($entry_edit_id,
						   $login_user_id,
						   $cgi_query->param('public_type'),
						   $entry_date,
						   $entry_time,
						   $cgi_query->param('title'),
						   $cgi_query->param('text'),
						   \@category_id_list,
						   \@tag_list,
						   $cgi_query->param('comment'),
						   $cgi_query->param('trackback'),
						   time());
	
	$result = check_entry_data();
	
	if(($result == 0) && ($cgi_query->param('regist') ne ''))
	{
		# エントリを登録する
		$result = regist_entry_data(\$entry_edit_id);
	}
	
	unlock_db();
	
	if(($result == 0) && ($cgi_query->param('regist') ne ''))
	{
		$$ref_output_str .= make_cookie_temp('result' => $result, 'id' => $entry_edit_id);
		
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'entry_regist');
	}
	else
	{
		if($result != 0)
		{
			$mode = "entry_edit";
			
			entry_error_add_error_msg($result);
			
			if($result & ERROR_FLAG_ENTRY_ID)
			{
				set_current_entry_hash('id' => -1);
			}
			
			if($result & (ERROR_FLAG_ENTRY_DATE | ERROR_FLAG_ENTRY_TIME))
			{
				Mash::MashTime::get_current_date_time(\$entry_date, \$entry_time);
				
				set_current_entry_hash('date' => $entry_date, 'time' => $entry_time);
			}
		}
		
		$$ref_output_str .= make_content_type_str('charset' => $charset) . make_html($mode);
		
		$$ref_output_str .= $tmp_str;
	}
}

sub mode_entry_regist
{
	my $ref_output_str = $_[0];
	
	if(lock_db() != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_lock_error');
		
		return;
	}
	
	if((init_category_db($db_root_path) != 0) ||
	   (init_entry_db($db_root_path)    != 0))
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_read_error');
		
		unlock_db();
		
		return;
	}
	
	unlock_db();
	
	my %value = get_cookie_temp();
	
	if(exists($value{'id'}))
	{
		$cur_entry_id = $value{'id'};
	}
	
	$$ref_output_str .= make_content_type_str('charset' => $charset) . make_html($mode)
}

sub mode_comment_edit
{
	my $ref_output_str = $_[0];
	
	if((check_addr($ENV{'REMOTE_ADDR'}, \@{$settings{'deny_comment_addr'}})) || (check_host($ENV{'REMOTE_HOST'}, \@{$settings{'deny_comment_host'}})))
	{
		print make_location_str($this_cgi, 'mode' => 'error', 'type' => 'deny_comment_error');
	
		exit;
	}
	
	if(lock_db() != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_lock_error');
		
		return;
	}
	
	if((init_category_db($db_root_path) != 0) ||
	   (init_entry_db($db_root_path)    != 0) ||
	   (init_comment_db($db_root_path)  != 0))
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_read_error');
		
		unlock_db();
		
		return;
	}
	
	unlock_db();
	
	set_current_entry_hash('user_id' => $login_user_id);
	
	my $comment_user_id = $cgi_query->param('user_id');
	my $error_type      = '';
	
	if((defined($comment_user_id)) && ($comment_user_id ne '') && ($comment_user_id ne $login_user_id))
	{
		$error_type = 'comment_user_id_error';
	}
	
	if($error_type eq '')
	{
		my $return         = $cgi_query->param('return');
		my $comment_no     = $cgi_query->param('no');
		my $parent_edit_id = $cgi_query->param('parent_id');
		my $entry_id       = $cgi_query->param('entry_id');
		my $comment_name   = $cgi_query->param('name');
		my $comment_title  = $cgi_query->param('title');
		my $comment_text   = $cgi_query->param('text');
		my $comment_mail   = $cgi_query->param('mail');
		my $comment_url    = $cgi_query->param('url');
		
		if($comment_name eq '')
		{
			if($login_name ne '')
			{
				$comment_name = $login_name;
			}
			else
			{
				$comment_name = $settings{'comment_name'};
			}
		}
		
		if((defined($return)) && ($return eq 'true'))
		{
			set_current_comment_data(-1,
									 $comment_no,
									 $parent_edit_id,
									 $entry_id,
									 $comment_user_id,
									 '',
									 '',
									 $comment_name,
									 $comment_title,
									 $comment_text,
									 $comment_mail,
									 $comment_url,
									 '',
									 0);
		}
		else
		{
			if($parent_edit_id >= 1)
			{
				my $parent_index_no = get_comment_index($parent_edit_id);
				if($parent_index_no >= 0)
				{
					$entry_id = get_comment_data($parent_index_no , 'entry_id');
				}
			
				set_current_comment_data(-1,
										 $comment_no,
										 $parent_edit_id,
										 $entry_id,
										 $comment_user_id,
										 '',
										 '',
										 $comment_name,
										 $comment_title,
										 $comment_text,
										 $comment_mail,
										 $comment_url,
										 '',
										 0);
			}
			
			my $comment_name = '';
			my $comment_mail = '';
		
			get_cookie_comment(\$comment_name, \$comment_mail);
		
			if($comment_name ne '')
			{
				set_current_comment_hash('name' => $comment_name, 'mail' => $comment_mail);
			
				$comment_name_mail_save = 'enable';
			}
			else
			{
				set_current_comment_hash('name' => $login_name);
			}
		}
		
		$$ref_output_str .= make_content_type_str('charset' => $charset) . make_html($mode)
	}
	else
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => $error_type);
	}
}

sub mode_comment_check
{
	my $ref_output_str = $_[0];
	
	if((check_addr($ENV{'REMOTE_ADDR'}, \@{$settings{'deny_comment_addr'}})) || (check_host($ENV{'REMOTE_HOST'}, \@{$settings{'deny_comment_host'}})))
	{
		print make_location_str($this_cgi, 'mode' => 'error', 'type' => 'deny_comment_error');
	
		exit;
	}
	
	if(($cgi_query->param('regist') eq '') && ($cgi_query->param('check') eq ''))
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'unknown_error');
		
		return;
	}
	
	if(lock_db() != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_lock_error');
		
		return;
	}
	
	if((init_category_db($db_root_path) != 0) ||
	   (init_entry_db($db_root_path)    != 0) ||
	   (init_comment_db($db_root_path)  != 0) ||
	   (init_file_db($db_root_path)     != 0))
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_read_error');
		
		unlock_db();
		
		return;
	}
	
	set_current_entry_hash('user_id' => $login_user_id);
	
	my $comment_user_id = $cgi_query->param('user_id');
	my $error_type      = '';
	
	if((defined($comment_user_id)) && ($comment_user_id ne '') && ($comment_user_id ne $login_user_id))
	{
		$error_type = 'comment_user_id_error';
	}
	
	if($error_type eq '')
	{
		my $comment_edit_id = -1;
		my $parent_edit_id  = $cgi_query->param('parent_id');
		my $entry_id        = $cgi_query->param('entry_id');
		my $comment_no      = $cgi_query->param('no');
		my $comment_name    = $cgi_query->param('name');
		my $comment_title   = $cgi_query->param('title');
		my $comment_text    = $cgi_query->param('text');
		my $comment_mail    = $cgi_query->param('mail');
		my $comment_url     = $cgi_query->param('url');
		my $comment_date    = '';
		my $comment_time    = '';
		
		if($cgi_query->param('save') eq 'enable')
		{
			$comment_name_mail_save = 'enable';
		}
		
		Mash::MashTime::get_current_date_time(\$comment_date, \$comment_time);
		
		set_current_comment_data($comment_edit_id,
								 $comment_no,
								 $parent_edit_id,
								 $entry_id,
								 $comment_user_id,
								 $comment_date,
								 $comment_time,
								 $comment_name,
								 $comment_title,
								 $comment_text,
								 $comment_mail,
								 $comment_url,
								 '',
								 time());
		
		my $result = check_comment_data();
		
		if(($result == 0) && ($cgi_query->param('regist') ne ''))
		{
			# コメントを登録する
			$result = regist_comment_data(\$comment_edit_id);
		}
		
		unlock_db();
		
		if($result == 0)
		{
			if($cgi_query->param('save') eq 'enable')
			{
				# Cookieに名前・メールアドレスを保存
				$$ref_output_str .= make_cookie_comment($comment_name, $comment_mail, $settings{'comment_keep_day'});
			}
			else
			{
				# Cookieに保存されている名前・メールアドレスを削除
				$$ref_output_str .= make_cookie_comment('', '', -1);
			}
		}
		
		if(($result == 0) && ($cgi_query->param('regist') ne ''))
		{
			$$ref_output_str .= make_cookie_temp('result' => $result, 'id' => $comment_edit_id);
		
			$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'comment_regist');
		}
		else
		{
			if($result != 0)
			{
				$mode = "comment_edit";
			
				comment_error_add_error_msg($result);
			}
		
			$$ref_output_str .= make_content_type_str('charset' => $charset) . make_html($mode);
		}
	}
	else
	{
		unlock_db();
		
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => $error_type);
	}
}

sub mode_comment_regist
{
	my $ref_output_str = $_[0];
	
	if((check_addr($ENV{'REMOTE_ADDR'}, \@{$settings{'deny_comment_addr'}})) || (check_host($ENV{'REMOTE_HOST'}, \@{$settings{'deny_comment_host'}})))
	{
		print make_location_str($this_cgi, 'mode' => 'error', 'type' => 'deny_comment_error');
	
		exit;
	}
	
	if(lock_db() != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_lock_error');
		
		return;
	}
	
	if((init_category_db($db_root_path) != 0) ||
	   (init_entry_db($db_root_path)    != 0) ||
	   (init_comment_db($db_root_path)  != 0))
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_read_error');
		
		unlock_db();
		
		return;
	}
	
	unlock_db();
	
	my %value = get_cookie_temp();
	
	if(exists($value{'id'}))
	{
		$cur_comment_id = $value{'id'};
		
		my $comment_index_no = get_comment_index($cur_comment_id);
		if($comment_index_no >= 0)
		{
			$cur_entry_id = get_comment_data($comment_index_no, 'entry_id');
		}
	}
	
	$$ref_output_str .= make_content_type_str('charset' => $charset) . make_html($mode)
}

sub mode_comment_admin
{
	my $ref_output_str = $_[0];
	
	if(lock_db() != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_lock_error');
		
		return;
	}
	
	if((init_category_db($db_root_path) != 0) ||
	   (init_entry_db($db_root_path)    != 0) ||
	   (init_comment_db($db_root_path)  != 0) ||
	   (init_file_db($db_root_path)     != 0))
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_read_error');
		
		unlock_db();
		
		return;
	}
	
	unlock_db();
	
	my $entry_id   = $cgi_query->param('entry_id');
	my $error_type = '';
	
	if((defined($entry_id)) && ($entry_id ne ''))
	{
		my $entry_user_id = get_entry_data($entry_id, 'user_id');
		
		if((!defined($entry_user_id)) || ($entry_user_id eq ''))
		{
			# エントリID指定エラー
			$error_type = "entry_id_error";
		}
		elsif($entry_user_id ne $login_user_id)
		{
			# ユーザーIDエラー（コメントを管理する権限がない）
			$error_type = "entry_auth_error";
		}
	}
	
	if($error_type eq '')
	{
		my %value = get_cookie_temp();
		
		$cur_entry_id = $entry_id;
		
		if(exists($value{'comment_id'}))
		{
			$cur_comment_id = $value{'comment_id'};
		}
		
		if(exists($value{'status'}))
		{
			$comment_changed_status = $value{'status'};
		}
		
		if(exists($value{'result'}))
		{
			my $result = $value{'result'};
			
			comment_error_add_error_msg($result);
		}
		
		# テンポラリCookieをクリア
		$$ref_output_str .= make_cookie_temp();
		
		$$ref_output_str .= make_content_type_str('charset' => $charset) . make_html($mode)
	}
	else
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => $error_type);
	}
}

sub mode_comment_status_change
{
	my $ref_output_str = $_[0];
	
	if(lock_db() != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_lock_error');
		
		return;
	}
	
	if((init_category_db($db_root_path) != 0) ||
	   (init_entry_db($db_root_path)    != 0) ||
	   (init_comment_db($db_root_path)  != 0))
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_read_error');
		
		unlock_db();
		
		return;
	}
	
	my $result         = 0;
	my $entry_id       = $cgi_query->param('entry_id');
	my @comment_ids    = $cgi_query->param('comment_id');
	my $comment_id     = '';
	my $comment_status = '';
	
	foreach my $query_key ($cgi_query->param())
	{
		if($query_key =~ m/(public|private|delete|all_public|all_private|all_delete|select_public|select_private|select_delete)([0-9]*)/)
		{
			$comment_status = $1;
			$comment_id     = $2;
			
			last;
		}
	}
	
	$result = check_comment_status($comment_id, $entry_id, $comment_status);
	
	if($result == 0)
	{
		if(($comment_status eq 'public') || ($comment_status eq 'private') || ($comment_status eq 'delete'))
		{
			$result = update_comment_status($comment_id, $comment_status);
		}
		elsif($comment_status eq 'all_public')
		{
			$result = all_update_comment_status($entry_id, 'public');
		}
		elsif($comment_status eq 'all_private')
		{
			$result = all_update_comment_status($entry_id, 'private');
		}
		elsif($comment_status eq 'all_delete')
		{
			$result = all_update_comment_status($entry_id, 'delete');
		}
		elsif($comment_status eq 'select_public')
		{
			foreach $comment_id (@comment_ids)
			{
				$result = update_comment_status($comment_id, 'public');
			}
		}
		elsif($comment_status eq 'select_private')
		{
			foreach $comment_id (@comment_ids)
			{
				$result = update_comment_status($comment_id, 'private');
			}
		}
		elsif($comment_status eq 'select_delete')
		{
			foreach $comment_id (@comment_ids)
			{
				$result = update_comment_status($comment_id, 'delete');
			}
		}
	}
	
	unlock_db();
	
	$$ref_output_str .= make_cookie_temp('result' => $result, 'comment_id' => $comment_id, 'status' => $comment_status);
	
	$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'comment_admin', 'entry_id' => $entry_id);
}

sub mode_category
{
	my $ref_output_str = $_[0];
	
	if(lock_db() != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_lock_error');
		
		return;
	}
	
	if(init_category_db($db_root_path) != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_read_error');
		
		unlock_db();
		
		return;
	}
	
	unlock_db();
	
	my %value = get_cookie_temp();
	
	if(exists($value{'result'}))
	{
		my $result = $value{'result'};
		
		if(exists($value{'regist_name'}))
		{
			$category_regist_name        = $value{'regist_name'};
		}
		if(exists($value{'regist_parent_name'}))
		{
			$category_regist_parent_name = $value{'regist_parent_name'};
		}
		if(exists($value{'rename_name'}))
		{
			$category_rename_name        = $value{'rename_name'};
		}
		if(exists($value{'rename_before_name'}))
		{
			$category_rename_before_name = $value{'rename_before_name'};
		}
		if(exists($value{'delete_name'}))
		{
			$category_delete_name        = $value{'delete_name'};
		}
		
		category_error_add_error_msg($result);
	}
	
	# テンポラリCookieをクリア
	$$ref_output_str .= make_cookie_temp();
	
	$$ref_output_str .= make_content_type_str('charset' => $charset) . make_html($mode)
}

sub mode_category_regist
{
	my $ref_output_str = $_[0];
	
	if(lock_db() != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_lock_error');
		
		return;
	}
	
	if(init_category_db($db_root_path) != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_read_error');
		
		unlock_db();
		
		return;
	}
	
	my $result = 0;
	my $regist_name        = $cgi_query->param('new_name');
	my $parent_category_id = $cgi_query->param('parent_category_id');
	my $regist_parent_name = '';
	
	if($parent_category_id > -1)
	{
		my $parent_index_no = get_category_index($parent_category_id);
		if($parent_index_no >= 0)
		{
			$regist_parent_name = get_category_data($parent_index_no, 'name');
		}
	}
	
	set_current_category_data(-1,
							  $regist_name,
							  $parent_category_id,
							  0,
							  0);
	
	$result = regist_category_data();
	
	unlock_db();
	
	$$ref_output_str .= make_cookie_temp('result' => $result, 'regist_name' => $regist_name, 'regist_parent_name' => $regist_parent_name);
	
	$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'category');
}

sub mode_category_rename
{
	my $ref_output_str = $_[0];
	
	if(lock_db() != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_lock_error');
		
		return;
	}
	
	if(init_category_db($db_root_path) != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_read_error');
		
		unlock_db();
		
		return;
	}
	
	my $result = 0;
	my $category_id        = $cgi_query->param('category_id');
	my $rename_name        = $cgi_query->param('new_name');
	my $rename_before_name = '';
	
	if($category_id ne '')
	{
		my $category_index_no = get_category_index($category_id);
		
		if($category_index_no >= 0)
		{
			$rename_before_name = get_category_data($category_index_no, 'name');
		}
	}
	
	$result = rename_category_data($category_id, $rename_name);
	
	unlock_db();
	
	$$ref_output_str .= make_cookie_temp('result' => $result, 'rename_name' => $rename_name, 'rename_before_name' => $rename_before_name);
	
	$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'category');
}

sub mode_category_delete
{
	my $ref_output_str = $_[0];
	
	if(lock_db() != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_lock_error');
		
		return;
	}
	
	if(init_category_db($db_root_path) != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_read_error');
		
		unlock_db();
		
		return;
	}
	
	my $result            = 0;
	my $category_id       = $cgi_query->param('category_id');
	my $sub_delete_enable = $cgi_query->param('sub_delete');
	my $delete_name       = '';
	
	if($category_id ne '')
	{
		my $category_index_no = get_category_index($category_id);
		
		if($category_index_no >= 0)
		{
			$delete_name = get_category_data($category_index_no, 'name');
		}
	}
	
	$result = delete_category_data($category_id, $sub_delete_enable);
	
	unlock_db();
	
	$$ref_output_str .= make_cookie_temp('result' => $result, 'delete_name' => $delete_name);
	
	$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'category');
}

sub mode_account_edit
{
	my $ref_output_str   = $_[0];
	my $account_edit_id  = $cgi_query->param('id');
	my $account_index_no = -1;
	
	if($mode eq 'startup_account_edit')
	{
		if(get_account_db_list_size() > 0)
		{
			$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'startup_account_edit_error');
			
			return;
		}
	}
	else
	{
		if($mode eq 'account_rename')
		{
			if($account_edit_id ne '')
			{
				# 作成済みアカウントを編集
				$account_index_no = get_account_index($account_edit_id);
				
				if($account_index_no == -1)
				{
					# ユーザーID指定エラー
					$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'account_id_error');
					
					return;
				}
				
				if(($login_type ne 'admin') && ($login_user_id ne $account_edit_id))
				{
					# ユーザーID指定エラー
					$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'account_id_error');
					
					return;
				}
			}
			else
			{
				# ユーザーID指定エラー
				$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'account_id_error');
				
				return;
			}
		}
	}
	
	if($cgi_query->param('name') ne '')
	{
		set_current_account_data($account_edit_id, $cgi_query->param('name'), $cgi_query->param('password'), $cgi_query->param('repassword'), $cgi_query->param('type'), 0);
	}
	elsif($account_edit_id ne '')
	{
		set_current_account_data_existing($account_edit_id);
	}
	
	$$ref_output_str .= make_content_type_str('charset' => $charset) . make_html($mode)
}

sub mode_account_check
{
	my $ref_output_str = $_[0];
	
	if(($cgi_query->param('regist') eq '') && ($cgi_query->param('check') eq ''))
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'unknown_error');
		
		return;
	}
	
	if($mode eq 'startup_account_check')
	{
		if(get_account_db_list_size() > 0)
		{
			$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'startup_account_edit_error');
			
			return;
		}
	}
	
	if(lock_db() != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_lock_error');
		
		return;
	}
	
	if(init_account_db($db_root_path) != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_read_error');
		
		unlock_db();
		
		return;
	}
	
	my $result     = 0;
	my $account_id = $cgi_query->param('id');
	
	set_current_account_data($account_id, $cgi_query->param('name'), $cgi_query->param('password'), $cgi_query->param('repassword'), $cgi_query->param('type'), time());
	
	if($mode eq 'account_rename_check')
	{
		$result = check_rename_account();
	}
	else
	{
		$result = check_account_data();
	}
	
	if(($result == 0) && ($cgi_query->param('regist') ne ''))
	{
		# アカウントを登録する
		$result = regist_account_data();
	}
	
	unlock_db();
	
	if(($result == 0) && ($cgi_query->param('regist') ne ''))
	{
		$$ref_output_str .= make_cookie_temp('result' => $result, 'id' => $account_id);
		
		if($mode eq 'startup_account_check')
		{
			$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'startup_account_regist');
		}
		else
		{
			$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'account_regist');
		}
	}
	else
	{
		if($result != 0)
		{
			if($mode eq 'startup_account_check')
			{
				$mode = "startup_account_edit";
			}
			elsif($mode eq 'account_rename_check')
			{
				$mode = "account_rename";
			}
			else
			{
				$mode = "account_edit";
			}
			
			account_error_add_error_msg($result);
		}
		
		$$ref_output_str .= make_content_type_str('charset' => $charset) . make_html($mode)
	}
}

sub mode_account_regist
{
	my $ref_output_str = $_[0];
	
	if($mode eq 'startup_account_regist')
	{
		if(get_account_db_list_size() != 1)
		{
			$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'startup_account_edit_error');
			
			return;
		}
	}
	
	my %value = get_cookie_temp();
	
	if(exists($value{'id'}))
	{
		$cur_account_id = $value{'id'};
	}
	
	$$ref_output_str .= make_content_type_str('charset' => $charset) . make_html($mode)
}

sub mode_account_delete_check
{
	my $ref_output_str = $_[0];
	
	if(($cgi_query->param('regist') eq '') && ($cgi_query->param('check') eq ''))
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'unknown_error');
		
		return;
	}
	
	if(lock_db() != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_lock_error');
		
		return;
	}
	
	if(init_account_db($db_root_path) != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_read_error');
		
		unlock_db();
		
		return;
	}
	
	my $result = 0;
	my $account_edit_id  = $cgi_query->param('id');
	my $account_index_no = get_account_index($account_edit_id);
	
	if($account_index_no == -1)
	{
		# ユーザーID指定エラー
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'account_id_error');
		
		return;
	}
	
	set_current_account_data_existing($account_edit_id);
	
	$result = check_delete_account($account_edit_id);
	
	if(($result == 0) && ($cgi_query->param('regist') ne ''))
	{
		# アカウントを削除する
		$result = delete_account_data($account_edit_id);
	}
	
	unlock_db();
	
	if(($result == 0) && ($cgi_query->param('regist') ne ''))
	{
		$$ref_output_str .= make_cookie_temp('result' => $result, 'id' => $account_edit_id);
		
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'account_delete');
	}
	else
	{
		if($result != 0)
		{
			$mode = 'account';
			
			account_error_add_error_msg($result);
		}
		
		$$ref_output_str .= make_content_type_str('charset' => $charset) . make_html($mode)
	}
}

sub mode_account_delete
{
	my $ref_output_str = $_[0];
	
	my %value = get_cookie_temp();
	
	if(exists($value{'id'}))
	{
		$cur_account_id = $value{'id'};
		
		if($cur_account_id eq $login_user_id)
		{
			# ログインCookieをクリア
			$$ref_output_str .= make_cookie_login('', '', '', -1);
		}
	}
	
	$$ref_output_str .= make_content_type_str('charset' => $charset) . make_html($mode)
}

sub mode_file
{
	my $ref_output_str = $_[0];
	
	if(lock_db() != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_lock_error');
		
		return;
	}
	
	if(init_file_db($db_root_path) != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_read_error');
		
		unlock_db();
		
		return;
	}
	
	unlock_db();
	
	my %value = get_cookie_temp();
	
	if(exists($value{'file_name'}))
	{
		$file_regist_name = $value{'file_name'};
	}
	
	if(exists($value{'result'}))
	{
		my $result = $value{'result'};
		
		file_error_add_error_msg($result);
	}
	
	# テンポラリCookieをクリア
	$$ref_output_str .= make_cookie_temp();
	
	$$ref_output_str .= make_content_type_str('charset' => $charset) . make_html($mode)
}

sub mode_file_regist
{
	my $ref_output_str = $_[0];
	
	if(lock_db() != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_lock_error');
		
		return;
	}
	
	if(init_file_db($db_root_path) != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_read_error');
		
		unlock_db();
		
		return;
	}
	
	my $file_name = $cgi_query->param('upfile');
	
	my $result = regist_file_data($cgi_query->upload('upfile'), $file_name, $cgi_query->param('comment'));
	
	unlock_db();
	
	$$ref_output_str .= make_cookie_temp('result' => $result, 'file_name' => $file_name);
	
	$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'file');
}

sub mode_file_update
{
	my $ref_output_str = $_[0];
	
	if(lock_db() != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_lock_error');
		
		return;
	}
	
	if(init_file_db($db_root_path) != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_read_error');
		
		unlock_db();
		
		return;
	}
	
	my $result = update_file_data($cgi_query->param('id'), $cgi_query->param('comment'));
	
	unlock_db();
	
	$$ref_output_str .= make_cookie_temp('result' => $result);
	
	$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'file');
}

sub mode_file_delete
{
	my $ref_output_str = $_[0];
	
	if(lock_db() != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_lock_error');
		
		return;
	}
	
	if(init_file_db($db_root_path) != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_read_error');
		
		unlock_db();
		
		return;
	}
	
	my $result = delete_file_data($cgi_query->param('id'));
	
	unlock_db();
	
	$$ref_output_str .= make_cookie_temp('result' => $result);
	
	$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'file');
}

sub mode_tag_list_show
{
	my $ref_output_str = $_[0];
	
	$$ref_output_str .= make_content_type_str('charset' => $charset) . make_html($mode)
}

sub mode_log
{
	my $ref_output_str = $_[0];
	
	my $log_date = $cgi_query->param('date');
	
	if((defined($log_date)) && ($log_date ne ''))
	{
		$cur_log_date = $log_date;
	}
	
	$$ref_output_str .= make_content_type_str('charset' => $charset) . make_html($mode)
}

sub mode_startup
{
	my $ref_output_str = $_[0];
	
	$$ref_output_str .= make_content_type_str('charset' => $charset) . make_html($mode)
}

sub mode_error
{
	my $ref_output_str = $_[0];
	my $error_type = $cgi_query->param('type');
	
	# ログインCookieをクリア
	$$ref_output_str .= make_cookie_login('', '', '', -1);
	
	# テンポラリCookieをクリア
	$$ref_output_str .= make_cookie_temp();
	
	if(exists($res_string{$error_type}))
	{
		add_error_msg(\$res_string{$error_type});
	}
	else
	{
		add_error_msg(\$res_string{'unknown_error'});
	}
	
	$$ref_output_str .= make_content_type_str('charset' => $charset) . make_html($mode);
}

sub mode_user_common
{
	my $ref_output_str = $_[0];
	
	if(lock_db() != 0)
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_lock_error');
		
		return;
	}
	
	if((init_category_db($db_root_path)  != 0) ||
	   (init_entry_db($db_root_path)     != 0) ||
	   (init_comment_db($db_root_path)   != 0) ||
	   (init_trackback_db($db_root_path) != 0) ||
	   (init_file_db($db_root_path)      != 0))
	{
		$$ref_output_str .= make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_read_error');
		
		unlock_db();
		
		return;
	}
	
	unlock_db();
	
	$$ref_output_str .= make_content_type_str('charset' => $charset) . make_html($mode)
}

######## ログ ######## 

# ログ更新
sub update_log
{
	if((check_addr($ENV{'REMOTE_ADDR'}, \@{$settings{'log_exclusion_addr'}})) ||
	   (check_host($ENV{'REMOTE_HOST'}, \@{$settings{'log_exclusion_host'}})) ||
	   (check_str($login_user_id, \@{$settings{'log_exclusion_user'}}))       ||
	   (check_str($mode,          \@{$settings{'log_exclusion_mode'}})))
	{
		return;
	}
	
	if(make_dir($log_db_root_path) < 0)
	{
		print "Location: $this_cgi?mode=error&type=db_dir_error\r\n\r\n";
		
		exit;
	}
	
	my $log_date = '';
	my $log_time = '';
	
	Mash::MashTime::get_current_date_time(\$log_date, \$log_time);
	
	if(lock_db() != 0)
	{
		print make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_lock_error');
		
		exit;
	}
	
	if(init_log_db($log_db_root_path, $log_date) != 0)
	{
		print make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_read_error');
		
		unlock_db();
		
		exit;
	}
	
	set_current_log_data(-1,
						 $log_date,
						 $log_time,
						 $mode,
						 $login_user_id,
						 $ENV{'HTTP_REFERER'},
						 $ENV{'HTTP_USER_AGENT'},
						 $ENV{'REMOTE_HOST'},
						 $ENV{'REMOTE_ADDR'},
						 $ENV{'REQUEST_METHOD'},
						 $ENV{'QUERY_STRING'},
						 $ENV{'HTTP_VIA'},
						 $ENV{'HTTP_FORWARDED'},
						 $ENV{'HTTP_CACHE_INFO'},
						 $ENV{'HTTP_X_FORWARDED_FOR'},
						 $ENV{'HTTP_CACHE_CONTROL'},
						 $ENV{'HTTP_PROXY_CONNECTION'},
						 $ENV{'HTTP_FROM'});
	
	my $log_edit_id = -1;
	my $result = regist_log_data(\$log_edit_id);
	
	unlock_db();
	
	if($result != 0)
	{
		if($result == ERROR_FLAG_LOG_DB_REGIST)
		{
			print make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_regist_error');
		}
		else
		{
			print make_location_str($this_cgi, 'mode' => 'error', 'type' => 'db_write_error');
		}
		
		exit;
	}
}

# ログ日付リスト取得
#  param[in/out] : リストリファレンス
#  param[in]     : ソート('char', 'rchar' or 'none'(default))
sub get_log_date_list
{
	my ($ref_log_date_list, $sort) = @_;
	
	get_log_db_date_list($log_db_root_path, $ref_log_date_list);
	
	if(defined($sort))
	{
		if($sort eq 'char')
		{
			@$ref_log_date_list = sort { $a cmp $b } @$ref_log_date_list;
		}
		elsif($sort eq 'rchar')
		{
			@$ref_log_date_list = sort { $b cmp $a } @$ref_log_date_list;
		}
	}
}

######## ログイン ######## 

# Cookieのログイン状態チェック
#  param[in]    : Cookieに保存されているID・パスワードと、GET/POSTで渡されるID・パスワードとのチェック無効(0)/有効(1)
#  param[out]   : 出力文字列リファレンス
#  param[in]... : ログインを許可するユーザータイプ(空の場合は全てのタイプで許可)
#  return       : 成功(0)/失敗(-1)
sub check_login
{
	my ($query_check, $ref_output_str, @allow_type) = @_;
	my $auto = '';
	
	get_cookie_login(\$login_user_id, \$login_user_pass, \$auto);
	
	if(($query_check == 0) ||
	   (($cgi_query->param('login_user_id') eq $login_user_id) && ($cgi_query->param('login_user_pass') eq $login_user_pass)))
	{
		get_login_param($login_user_id, $login_user_pass, \$login_type, \$login_name);
		
		if(@allow_type == 0)
		{
			if(($login_type eq 'admin') || ($login_type eq 'normal'))
			{
				# ログイン成功時はID・パスワード・自動ログインをCookieに保存
				$$ref_output_str .= make_cookie_login_success($login_user_id, $login_user_pass, $auto);
			}
			
			return 0;
		}
		else
		{
			foreach(@allow_type)
			{
				if($_ eq $login_type)
				{
					# ログイン成功時はID・パスワード・自動ログインをCookieに保存
					$$ref_output_str .= make_cookie_login_success($login_user_id, $login_user_pass, $auto);
			
					return 0;
				}
			}
		}
	}
	
	return -1;
}

# ログインパラメータ取得
#  param[in]  : ログインID
#  param[in]  : ログインパスワード（暗号化済み）
#  param[out] : ログイン種別
#  param[out] : ログイン名
#  return     : 成功(0)/失敗(-1)
sub get_login_param
{
	my ($id, $pass, $ref_type, $ref_name) = @_;
	
	if((defined($id)) && ($id ne ''))
	{
		my $account_index_no = get_account_index($id);
		
		if($account_index_no >= 0)
		{
			if($pass eq get_account_data($account_index_no, 'password'))
			{
				$$ref_type = get_account_data($account_index_no, 'type');
				$$ref_name = get_account_data($account_index_no, 'name');
				
				return 0;
			}
		}
		
		$$ref_type = 'error';
		$$ref_name = '';
	}
	else
	{
		$$ref_type = 'guest';
		$$ref_name = $res_string{'guest_name'};
		
		return 0;
	}
	
	return -1;
}

######## エラー ########

# エラーメッセージ文字列追加
#  param[in] : 文字列リファレンス
sub add_error_msg
{
	my $ref_error_msg = $_[0];
	
	push(@error_msg_list, $$ref_error_msg);
}

######## その他 ######## 

# システムパラメータへのリファレンスを取得する
#  param[in] : パラメータ名
#  return    : リファレンス
sub get_system_param_ref
{
	my ($key) = @_;
	
	if(exists($system_param{$key}))
	{
		return $system_param{$key};
	}
	
	return undef;
}

__END__
