###############################################################################
#
#  parts_tag.pl
#
#    author : Masashi Haraki
#
###############################################################################

use strict;
#use utf8;
use Mash::MashDebug;

package parts_tag;

# タグテーブル
my %tags =
	(
	 'calender_table'   => { 'no' => 100, 'method' => \&method_calender_table,   'end' => 0, },
	);

# タグテーブル取得
sub get_tags
{
	return \%tags;
}

######## 変数 ########

######## タグメソッド ########

#### カレンダー ####

# 指定年月のカレンダーテーブルを生成する
sub method_calender_table
{
	my ($attr_str) = @_;
	my %attr = ();
	my $year     = 0;
	my $month    = 0;
	my $class    = '';
	my $cgi_name = ${main::get_system_param_ref('this_cgi')};
	my $ret_str  = '';
	
	system_tag::parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'date'}))
	{
		my $date = '';
		
		system_tag::get_param($attr{'date'}, \$date, 1);
		
		($year, $month) = split(/-/, $date);
	}
	
	if(exists($attr{'class'}))
	{
		system_tag::get_param($attr{'class'}, \$class, 1);
	}
	
	if(($year <= 0) || ($month < 1) || ($month > 12))
	{
		my ($m, $y) = (localtime())[4..5];
		
		$year  = $y + 1900;
		$month = $m + 1;
	}
	
	my $first_wday = Mash::MashTime::get_wday($year, $month, 1);
	my $day        = 1 - $first_wday;
	my $day_max    = Mash::MashTime::get_day_max($year, $month);
	my $wday       = 0;
	my %db_attr    = ('public_type' => 'public');
	
	while($day <= $day_max)
	{
		if($wday == 0)
		{
			$ret_str .= "<tr class = \"$class\">";
		}
		
		$ret_str .= "<td class = \"$class\">";
		
		if($day >= 1)
		{
			my $date_str = sprintf("%.4d-%.2d-%.2d", $year, $month, $day);
			my $day_entry_num = 0;
			
			$db_attr{'date'} = $date_str;
			
			main::init_entry_db_list(\%db_attr, 0, 0);
			
			$day_entry_num = main::get_entry_db_list_size();
			
			if($day_entry_num > 0)
			{
				$ret_str .= "<a href = \"$cgi_name?date=$date_str\">$day</a>";
			}
			else
			{
				$ret_str .= "$day";
			}
		}
		
		$ret_str .= "</td>";
		
		$day++;
		
		$wday++;
		if($wday >= 7)
		{
			$ret_str .= "</tr>";
			
			$wday = 0;
		}
	}
	
	while($wday > 0)
	{
		$ret_str .= "<td class = \"$class\"></td>";
		
		$wday++;
		if($wday >= 7)
		{
			$ret_str .= "</tr>";
			
			$wday = 0;
		}
	}
	
	return $ret_str;
}

1;
