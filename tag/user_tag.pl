###############################################################################
#
#  user_tag.pl
#
#    author : Masashi Haraki
#
###############################################################################

use strict;
#use utf8;
use Mash::MashDebug;

package user_tag;

# タグテーブル
my %tags =
(
	'font' =>
	{
		'no'          => 0,
		'method'      => \&method_font,
		'end'         => 1,
		'permission'  => [            'entry', 'entry_brief', 'comment'],
		'description' => 'フォントサイズ・カラー',
	  'usage'       => '[font {size = <var class = "tag_list_param">SIZE</var>}{;color = <var class = "tag_list_param">COLOR</var>}] ... [/font]',
	},
	'b' =>
	{
		'no'          => 1,
		'method'      => \&method_bold,
		'end'         => 1,
		'permission'  => [            'entry', 'entry_brief', 'comment'],
		'description' => '太字',
		'usage'       => '[b] ... [/b]',
	},
	'i' =>
	{
		'no'          => 2,
		'method'      => \&method_italy,
		'end'         => 1,
		'permission'  => [            'entry', 'entry_brief', 'comment'],
		'description' => '斜体',
		'usage'       => '[i] ... [/i]',
	},
	's' =>
	{
		'no'          => 3,
		'method'      => \&method_strike,
		'end'         => 1,
		'permission'  => [            'entry', 'entry_brief', 'comment'],
		'description' => '取消線',
		'usage'       => '[s] ... [/s]',
	},
	'quote' =>
	{
		'no'          => 4,
		'method'      => \&method_quote,
		'end'         => 1,
		'permission'  => [            'entry', 'entry_brief',          ],
		'description' => '引用',
		'usage'       => '[quote] ... [/quote]',
	},
	'code' =>
	{
		'no'          => 5,
		'method'      => \&method_code,
		'end'         => 1,
		'permission'  => [            'entry', 'entry_brief',          ],
		'description' => 'プログラムのソースコード等',
		'usage'       => '[code {lang = <var class = "tag_list_select">c</var>|<var class = "tag_list_select">c++</var>|<var class = "tag_list_select">perl</var>|<var class = "tag_list_select">java</var>}{;line = <var class = "tag_list_select">true</var>|<var class = "tag_list_select">false</var>}] ... [/code]',
	},
	'link' =>
	{
		'no'          => 6,
		'method'      => \&method_link,
		'end'         => 1,
		'permission'  => ['template', 'entry', 'entry_brief',          ],
		'description' => 'URLへのリンク',
		'usage'       => '[link {url = <var class = "tag_list_param">URL</var>|id = <var class = "tag_list_param">ENTRY_ID</var>}{;css_class = <var class = "tag_list_param">CSS_CLASS</var>}{;css_id = <var class = "tag_list_param">CSS_ID</var>}]',
	},
	'file' =>
	{
		'no'          => 7,
		'method'      => \&method_file,
		'end'         => 0,
		'permission'  => ['template', 'entry', 'entry_brief',          ],
		'description' => 'ファイルへのリンク',
		'usage'       => '[file id = <var class = "tag_list_param">FILE_ID</var>{;alt = <var class = "tag_list_param">ALT</var>}{;css_class = <var class = "tag_list_param">CSS_CLASS</var>}{;css_id = <var class = "tag_list_param">CSS_ID</var>}]<br>[file url = <var class = "tag_list_param">URL</var>{;alt = <var class = "tag_list_param">ALT</var>}{;css_class = <var class = "tag_list_param">CSS_CLASS</var>}{;css_id = <var class = "tag_list_param">CSS_ID</var>}]',
	},
	'image'        =>
	{
		'no'          => 8,
		'method'      => \&method_image,
		'end'         => 0,
		'permission'  => ['template', 'entry', 'entry_brief',          ],
		'description' => '画像イメージへのリンク',
		'usage'       => '[image file_id = <var class = "tag_list_param">FILE_ID</var>{;align = <var class = "tag_list_param">ALIGN</var>}{;css_class = <var class = "tag_list_param">CSS_CLASS</var>}{;css_id = <var class = "tag_list_param">CSS_ID</var>}]<br>[image url = <var class = "tag_list_param">URL</var>{;align = <var class = "tag_list_param">ALIGN</var>}{;css_class = <var class = "tag_list_param">CSS_CLASS</var>}{;css_id = <var class = "tag_list_param">CSS_ID</var>}]',
	},
	'applet'       =>
	{
		'no'          => 9,
		'method'      => \&method_applet,
		'end'         => 0,
		'permission'  => ['template', 'entry', 'entry_brief',          ],
		'description' => 'JAVAアプレット表示',
		'usage'       => '[applet file_id = <var class = "tag_list_param">FILE_ID</var>{;archive = <var class = "tag_list_param">true</var>}{;code = <var class = "tag_list_param">CODE</var>}{;codebase = <var class = "tag_list_param">CODEBASE</var>}{;width = <var class = "tag_list_param">WIDTH</var>}{;height = <var class = "tag_list_param">HEIGHT</var>}]',
	},
	'get_blog_url' =>
	{
		'no'          => 10,
		'method'      => \&method_get_blog_url,
		'end'         => 0,
		'permission'  => ['template', 'entry', 'entry_brief',          ],
		'description' => 'BlogのURL取得',
		'usage'       => '[get_blog_url id = <var class = "tag_list_param">ENTRY_ID</var>]',
	},
	'more'         =>
	{
		'no'          => 11,
		'method'      => \&method_more,
		'end'         => 2,
		'permission'  => [            'entry', 'entry_brief',          ],
		'description' => '「続きを読む」の表示',
		'usage'       => '[more]',
	},
	'head'         =>
	{
		'no'          => 12,
		'method'      => \&method_head,
		'end'         => 1,
		'permission'  => [            'entry', 'entry_brief',          ],
		'description' => '見出し',
		'usage'       => '[head {title = <var class = "tag_list_param">TITLE</var>}] ... [/head]',
	},
	'ul'           =>
	{
		'no'          => 13,
		'method'      => \&method_ul,
		'end'         => 1,
		'permission'  => ['template', 'entry', 'entry_brief',          ],
		'description' => '順序なしリスト',
		'usage'       => '[ul {css_class = <var class = "tag_list_param">CSS_CLASS</var>}{;css_id = <var class = "tag_list_param">CSS_ID</var>}] ... [/ul]',
	},
	'ol'           =>
	{
		'no'          => 14,
		'method'      => \&method_ol,
		'end'         => 1,
		'permission'  => ['template', 'entry', 'entry_brief',          ],
		'description' => '順序ありリスト',
		'usage'       => '[ol {css_class = <var class = "tag_list_param">CSS_CLASS</var>}{;css_id = <var class = "tag_list_param">CSS_ID</var>}] ... [/ol]',
	},
	'li'           =>
	{
		'no'          => 15,
		'method'      => \&method_li,
		'end'         => 0,
		'permission'  => ['template', 'entry', 'entry_brief',          ],
		'description' => 'リストアイテム',
		'usage'       => '[li {type = <var class = "tag_list_param">TYPE</var>}{;css_class = <var class = "tag_list_param">CSS_CLASS</var>}{;css_id = <var class = "tag_list_param">CSS_ID</var>}]',
	},
	'wrap_div'     =>
	{
		'no'          => 16,
		'method'      => \&method_wrap_div,
		'end'         => 1,
		'permission'  => [            'entry', 'entry_brief',          ],
		'description' => '&lt;div&gt;～&lt;/div&gt;による回り込み',
		'usage'       => '[wrap_div {float = <var class = "tag_list_select">left</var>|<var class = "tag_list_select">right</var>|<var class = "tag_list_select">both</var>|<var class = "tag_list_select">none</var>}{;margin = <var class = "tag_list_param">MARGIN</var>}{;padding = <var class = "tag_list_param">PADDING</var>}] ... {/wrap_div]',
	},
	'wrap_clear'   =>
	{
		'no'          => 17,
		'method'      => \&method_wrap_clear,
		'end'         => 0,
		'permission'  => [            'entry', 'entry_brief',          ],
		'description' => '回り込み解除',
		'usage'       => '[wrap_clear {clear = <var class = "tag_list_select">left</var>|<var class = "tag_list_select">right</var>|<var class = "tag_list_select">both</var>|<var class = "tag_list_select">none</var>}]',
	},
);

# タグテーブル取得
sub get_tags
{
	return \%tags;
}

# 文字列リソース
my %res_string =
	(
	 'more' => '続きを読む',
	);

######## 変数 ########

######## タグメソッド ########

#### HTMLタグ ####

# フォント
sub method_font
{
	my ($attr_str, $str) = @_;
	my %attr = ();
	my $return_str = '';
	my $size       = '';
	my $color      = '';
	
	system_tag::parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'size'}))
	{
		system_tag::get_param($attr{'size'}, \$size, 1);
	}
	
	if(exists($attr{'color'}))
	{
		system_tag::get_param($attr{'color'}, \$color, 1);
	}
	
	if(($size eq '') && ($color eq ''))
	{
		return $str;
	}
	
	$return_str = "<span style = \"";
	
	if($size ne '')
	{
		$return_str .= "font-size:" . $size . ";";
	}
	
	if($color ne '')
	{
		$return_str .= "color:" . $color;
	}
	
	$return_str .= "\">" . $str . "</span>";
	
	return $return_str;
}

# ボールド
sub method_bold
{
	my ($attr_str, $str) = @_;
	
	return "<span style = \"font-weight:bold\">$str</span>";
}

# イタリー
sub method_italy
{
	my ($attr_str, $str) = @_;
	
	return "<span style = \"font-style:italic\">$str</span>";
}

# 打ち消し線
sub method_strike
{
	my ($attr_str, $str) = @_;
	
	return "<span style = \"text-decoration:line-through\">$str</span>";
}

# 引用
sub method_quote
{
	my ($attr_str, $str) = @_;
	my $ret_str = '';
	
	$ret_str = "<blockquote class = \"default\">$str</blockquote>";
	
	return $ret_str;
}

#### プログラムのソースコード等 ####

sub method_code
{
	my ($attr_str, $str) = @_;
	my %attr = ();
	my $ret_str = '';
	my $line = 'false';
	
	my %langs =
	(
		'c'    => &parser_c,
		'c++'  => &parser_c,
		'perl' => &parser_perl,
		'java' => &parser_java,
	);
	
	system_tag::parse_attr_str($attr_str, \%attr);
	
	main::escape_string(\$str);
	
	if(exists($attr{'line'}))
	{
		system_tag::get_param($attr{'line'}, \$line, 1);
	}
	
	if(exists($attr{'lang'}))
	{
		my $lang = '';
		
		system_tag::get_param($attr{'lang'}, \$lang, 1);
		
		if(exists($langs{$lang}))
		{
#			$langs{$lang}(\$str);
		}
	}
	
	$ret_str = "<table class = \"code\">";
	
	$str =~ s/\t/    /g;			# 幅を取らないようにするため、タブをスペース4個にする
	
	my @strs = split(/\n/, $str);
	my $line_no = 1;
	
	foreach my $s (@strs)
	{
		$ret_str .= "<tr class = \"code\">";
		if($line eq 'true')
		{
			$ret_str .= "<th class = \"code\"><code class = \"code\">$line_no</code></td>";
		}
		$ret_str .= "<td class = \"code\"><code class = \"code\">$s</code></td>";
		$ret_str .= "</tr>";
		
		$line_no++;
	}
	
	$ret_str .= "</table>";
	
	return $ret_str;
}

# C/C++解析
#  param[in]  : 文字列リファレンス
sub parser_c
{
}

# perl解析
#  param[in]  : 文字列リファレンス
sub parser_perl
{
}

# JAVA解析
#  param[in]  : 文字列リファレンス
sub parser_java
{
}

#### リンク ####

# リンク
sub method_link
{
	my ($attr_str, $str) = @_;
	my %attr = ();
	my $return_str = '';
	my $css_class  = '';
	my $css_id     = '';
	my $url        = '';
	
	system_tag::parse_attr_str($attr_str, \%attr);
	
	get_css_class_id(\%attr, \$css_class, \$css_id);
	
	if(exists($attr{'url'}))
	{
		system_tag::get_param($attr{'url'}, \$url, 1);
	}
	else
	{
		$url = make_blog_url(\%attr);
	}
	
	if($url ne '')
	{
		if($str eq '')
		{
			$str = $url;
		}
		
		$return_str = "<a href = \"$url\" class = \"$css_class\" id = \"$css_id\">$str</a>";
	}
	
	return $return_str;
}

# ファイル
sub method_file
{
	my ($attr_str, $str) = @_;
	my %attr = ();
	my $return_str = '';
	my $url        = '';
	my $alt        = '';
	
	system_tag::parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'id'}))
	{
		my $file_id = '';
		
		system_tag::get_param($attr{'id'}, \$file_id, 1);
		
		if($file_id ne '')
		{
			$url = system_tag::method_get_file_path("id = $file_id");
			
			if($url ne '')
			{
				$alt = system_tag::method_get_file_data("id = $file_id;param = comment");
			}
		}
	}
	
	if($url ne '')
	{
		if(exists($attr{'alt'}))
		{
			system_tag::get_param($attr{'alt'}, \$alt, 1);
		}
		
		$return_str = "<p class = \"file_link\"><a class = \"file_link\" href = \"$url\">$alt</a></p>";
	}
	
	return $return_str;
}

# イメージ
sub method_image
{
	my ($attr_str, $str) = @_;
	my %attr = ();
	my $return_str = '';
	my $css_class  = '';
	my $css_id     = '';
	my $url        = '';
	my $align      = '';
	my $alt        = '';
	
	system_tag::parse_attr_str($attr_str, \%attr);
	
	get_css_class_id(\%attr, \$css_class, \$css_id);
	
	if(exists($attr{'url'}))
	{
		system_tag::get_param($attr{'url'}, \$url, 1);
	}
	elsif(exists($attr{'file_id'}))
	{
		my $file_id = '';
		
		system_tag::get_param($attr{'file_id'}, \$file_id, 1);
		
		if($file_id ne '')
		{
			$url = system_tag::method_get_file_path("id = $file_id");
			
			if($url ne '')
			{
				$alt = system_tag::method_get_file_data("id = $file_id;param = comment")
			}
		}
	}
	
	if(exists($attr{'align'}))
	{
		system_tag::get_param($attr{'align'}, \$align, 1);
	}
	
	if($url ne '')
	{
		if(exists($attr{'alt'}))
		{
			system_tag::get_param($attr{'alt'}, \$alt, 1);
		}
		
		$return_str = "<image src = \"$url\" alt = \"$alt\" class = \"$css_class\" id = \"$css_id\" align = \"$align\">";
	}
	
	return $return_str;
}

# アプレット
sub method_applet
{
	my ($attr_str, $str) = @_;
	my %attr = ();
	my $return_str = '';
	my $css_class  = '';
	my $css_id     = '';
	my $filename   = '';
	my $code       = '';
	my $codebase   = '';
	my $archive    = '';
	my $alt        = '';
	my $width      = '';
	my $height     = '';
	
	system_tag::parse_attr_str($attr_str, \%attr);
	
	get_css_class_id(\%attr, \$css_class, \$css_id);
	
	if(exists($attr{'file_id'}))
	{
		my $file_id = '';
		
		system_tag::get_param($attr{'file_id'}, \$file_id, 1);
		
		if($file_id ne '')
		{
			my $url = system_tag::method_get_file_path("id = $file_id");
			
			if($url ne '')
			{
				my @path = split(/\//, $url);
				
				local $" = '/';
				
				$filename = pop(@path);
				$codebase = "@path";
				
				$alt = system_tag::method_get_file_data("id = $file_id;param = comment")
			}
		}
	}
	
	if(exists($attr{'archive'}))
	{
		system_tag::get_param($attr{'archive'}, \$archive, 1);
		
		if($archive eq 'true')
		{
			$archive = $filename;
		}
	}
	
	if(exists($attr{'code'}))
	{
		system_tag::get_param($attr{'code'}, \$code, 1);
	}
	else
	{
		my @code_var = split(/\./, $filename);
		
		local $" = '.';
		
		pop(@code_var);
		
		$code = "@code_var";
	}
	
	if(exists($attr{'codebase'}))
	{
		system_tag::get_param($attr{'codebase'}, \$codebase, 1);
	}
	
	if($code ne '')
	{
		if(exists($attr{'width'}))
		{
			system_tag::get_param($attr{'width'}, \$width, 1);
		}
		
		if(exists($attr{'height'}))
		{
			system_tag::get_param($attr{'height'}, \$height, 1);
		}
		
		$return_str = "<applet code = \"$code\" codebase = \"$codebase\" archive = \"$archive\" width = \"$width\" height = \"$height\" alt = \"$alt\" class = \"$css_class\" id = \"$css_id\"></applet>";
	}
	
	return $return_str;
}

# BlogのURL取得
sub method_get_blog_url
{
	my ($attr_str, $str) = @_;
	my %attr = ();
	my $return_str = '';
	
	system_tag::parse_attr_str($attr_str, \%attr);
	
	$return_str = make_blog_url(\%attr);
	
	if($return_str eq '')
	{
		my $ref_this_cgi = main::get_system_param_ref('this_cgi');
	
		$return_str = $$ref_this_cgi;
	}
	
	if(exists($attr{'var'}))
	{
		if(system_tag::check_var_name($attr{'var'}) == 0)
		{
			$system_tag::user_var{$attr{'var'}} = $return_str;
		}
	}
	else
	{
		return $return_str;
	}
	
	return '';
}

# 「続きを読む」
sub method_more
{
	my ($attr_str, $str) = @_;
	my $permission_ref = main::get_system_param_ref('permission');
	
	if($$permission_ref eq 'entry_brief')
	{
		my $entry_id = $system_tag::user_var{'$entry_id'};
		
		if($entry_id >= 1)
		{
			my $url = method_get_blog_url("id = $entry_id");
			
			return "\n<p><br /></p>\n<p class = \"more_link\"><a href = \"$url#more\">$res_string{'more'}</a></p>\n";
		}
	}
	elsif($$permission_ref eq 'entry')
	{
		return "<a name = \"more\"></a>\n$str";
	}
	
	return $str;
}

#### 見出し ####

# 見出し
sub method_head
{
	my ($attr_str, $str) = @_;
	my %attr = ();
	my $title      = '';
	my $return_str = '';
	
	system_tag::parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'title'}))
	{
		system_tag::get_param($attr{'title'}, \$title, 1);
	}
	
	if($title ne '')
	{
		$return_str = "<a name = \"$title\"><h2 class = \"entry_head\">$str</h2></a>";
	}
	else
	{
		$return_str = "<h2 class = \"entry_head\">$str</h2>";
	}
	
	return $return_str;
}

#### リスト ####

my $default_ul_type = "";
my $default_ol_type = "";

# 順序なしリスト
sub method_ul
{
	my ($attr_str, $str) = @_;
	my %attr = ();
	my $css_class  = '';
	my $css_id     = '';
	my $type       = $default_ul_type;
	my $return_str = '';
	
	system_tag::parse_attr_str($attr_str, \%attr);
	
	get_css_class_id(\%attr, \$css_class, \$css_id);
	
	if(exists($attr{'type'}))
	{
		system_tag::get_param($attr{'type'}, \$type, 1);
	}
	
	$return_str = "<ul type = \"$type\" class = \"$css_class\" id = \"$css_id\">$str</ul>";
	
	return $return_str;
}

# 順序ありリスト
sub method_ol
{
	my ($attr_str, $str) = @_;
	my %attr = ();
	my $css_class  = '';
	my $css_id     = '';
	my $type       = $default_ol_type;
	my $return_str = '';
	
	system_tag::parse_attr_str($attr_str, \%attr);
	
	get_css_class_id(\%attr, \$css_class, \$css_id);
	
	if(exists($attr{'type'}))
	{
		system_tag::get_param($attr{'type'}, \$type, 1);
	}
	
	$return_str = "<ol type = \"$type\" class = \"$css_class\" id = \"$css_id\">$str</ol>";
	
	return $return_str;
}

# リストアイテム
sub method_li
{
	my ($attr_str, $str) = @_;
	my %attr = ();
	my $css_class  = '';
	my $css_id     = '';
	my $type       = $default_ol_type;
	my $return_str = '';
	
	system_tag::parse_attr_str($attr_str, \%attr);
	
	get_css_class_id(\%attr, \$css_class, \$css_id);
	
	if(exists($attr{'type'}))
	{
		system_tag::get_param($attr{'type'}, \$type, 1);
	}
	
	$return_str = "<li type = \"$type\" class = \"$css_class\" id = \"$css_id\">";
	
	return $return_str;
}

#### 回り込み ####

# divによる回り込み
sub method_wrap_div
{
	my ($attr_str, $str) = @_;
	my %attr = ();
	my $float      = '';
	my $margin     = '';
	my $padding    = '';
	my $return_str = '';
	
	system_tag::parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'float'}))
	{
		system_tag::get_param($attr{'float'}, \$float, 1);
	}
	
	if(exists($attr{'margin'}))
	{
		system_tag::get_param($attr{'margin'}, \$margin, 1);
	}
	
	if(exists($attr{'padding'}))
	{
		system_tag::get_param($attr{'padding'}, \$padding, 1);
	}
	
	if((!(defined($float))) || (($float ne 'left') && ($float ne 'right') && ($float ne 'both') && ($float ne 'none')))
	{
		$float = 'both';
	}
	
	$return_str = "<div style = \"float:$float;";
	
	if($margin ne '')
	{
		$return_str .= " margin:$margin;";
	}
	
	if($padding ne '')
	{
		$return_str .= " padding:$padding;";
	}
	
	$return_str .= "\">$str</div>";
	
	return $return_str;
}

# 回り込み解除
sub method_wrap_clear
{
	my ($attr_str, $str) = @_;
	my %attr = ();
	my $clear      = '';
	my $return_str = '';
	
	system_tag::parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'clear'}))
	{
		system_tag::get_param($attr{'clear'}, \$clear, 1);
	}
	
	if((!(defined($clear))) || (($clear ne 'left') && ($clear ne 'right') && ($clear ne 'both') && ($clear ne 'none')))
	{
		$clear = 'both';
	}
	
	$return_str = "<br style = \"clear:$clear;\">";
	
	return $return_str;
}

#### サブルーチン ####

# CSSのクラス・ID名を取得
#  param[in]  : アトリビュート群ハッシュリファレンス
#  param[out] : CSS クラス名リファレンス
#  param[out] : CSS ID名リファレンス
sub get_css_class_id
{
	my ($ref_attr, $ref_css_class, $ref_css_id) = @_;
	
	if(exists($$ref_attr{'css_class'}))
	{
		system_tag::get_param($$ref_attr{'css_class'}, $ref_css_class, 1);
	}
	
	if(exists($$ref_attr{'css_id'}))
	{
		system_tag::get_param($$ref_attr{'css_id'}, $ref_css_id, 1);
	}
}

# BlogのURL生成
#  param[in]  : アトリビュート群ハッシュリファレンス
sub make_blog_url
{
	my ($ref_attr) = @_;
	my $return_str = '';
	my $param_num  = 0;
	my $ref_this_cgi = main::get_system_param_ref('this_cgi');
	
	if(exists($$ref_attr{'mode'}))
	{
		my $mode = '';
		
		system_tag::get_param($$ref_attr{'mode'}, \$mode, 1);
		
		$return_str = $$ref_this_cgi . "?mode=" . $mode;
		$param_num++;
	}
	elsif(exists($$ref_attr{'id'}))
	{
		my $entry_id = '';
		
		system_tag::get_param($$ref_attr{'id'}, \$entry_id, 1);
		
		$return_str = $$ref_this_cgi . "?id=" . $entry_id;
		$param_num++;
	}
	elsif(exists($$ref_attr{'comment_id'}))
	{
		my $comment_id = '';
		my $index_no   = '';
		my $entry_id   = '';
		my $comment_no = '';
		
		system_tag::get_param($$ref_attr{'comment_id'}, \$comment_id, 1);
		
		$index_no   = main::get_comment_index($comment_id);
		$entry_id   = main::get_comment_data($index_no, 'entry_id');
		$comment_no = main::get_comment_data($index_no, 'no');
		
		$return_str = $$ref_this_cgi . "?id=" . $entry_id . "#comment_" . $comment_no;
		$param_num++;
	}
	elsif(exists($$ref_attr{'category_id'}))
	{
		my $category_id = '';
		
		system_tag::get_param($$ref_attr{'category_id'}, \$category_id, 1);
		
		$return_str = $$ref_this_cgi . "?category_id=" . $category_id;
		$param_num++;
	}
	elsif(exists($$ref_attr{'user_id'}))
	{
		my $user_id = '';
		
		system_tag::get_param($$ref_attr{'user_id'}, \$user_id, 1);
		
		$return_str = $$ref_this_cgi . "?user_id=" . $user_id;
		$param_num++;
	}
	elsif(exists($$ref_attr{'date'}))
	{
		my $date= '';
		
		system_tag::get_param($$ref_attr{'date'}, \$date, 1);
		
		$return_str = $$ref_this_cgi . "?date=" . $date;
		$param_num++;
	}
	elsif(exists($$ref_attr{'tag'}))
	{
		my $tag= '';
		
		system_tag::get_param($$ref_attr{'tag'}, \$tag, 1);
		
		$tag = CGI::Util::escape($tag);
		
		$return_str = $$ref_this_cgi . "?tag=" . $tag;
		$param_num++;
	}
	elsif(exists($$ref_attr{'search'}))
	{
		my $search = '';
		my @search_keywords = ();
		
		system_tag::get_param($$ref_attr{'search'}, \$search, 1);
		
		@search_keywords = split(/ /, $search);
		
		if(@search_keywords > 0)
		{
			my $search_num = @search_keywords;
			
			$return_str = $$ref_this_cgi . "?search=";
			
			foreach my $keyword (@search_keywords)
			{
				$keyword = CGI::Util::escape($keyword);
				
				$return_str .= $keyword;
				
				$search_num--;
				if($search_num > 0)
				{
					$return_str .= '+';
				}
			}
			$param_num++;
			
			if(exists($$ref_attr{'search_type'}))
			{
				my $search_type = '';
				
				system_tag::get_param($$ref_attr{'search_type'}, \$search_type, 1);
				
				$return_str .= "&search_type=" . $search_type;
				$param_num++;
			}
			
			if(exists($$ref_attr{'search_item'}))
			{
				my $search_item = '';
				my @search_items = ();
				
				system_tag::get_param($$ref_attr{'search_item'}, \$search_item, 1);
				
				@search_items = split(/ /, $search_item);
				
				foreach my $item (@search_items)
				{
					$return_str .= "&search_item=" . $item;
				}
			}
		}
	}
	
	if(exists($$ref_attr{'page'}))
	{
		my $page= '';
		
		system_tag::get_param($$ref_attr{'page'}, \$page, 1);
		
		if($page > 0)
		{
			if($param_num == 0)
			{
				$return_str .= "?page=" . $page;
			}
			else
			{
				$return_str .= "&page=" . $page;
			}
		}
	}
	
	return $return_str;
}

1;
