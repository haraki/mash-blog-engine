###############################################################################
#
#  system_tag.pl
#
#    author : Masashi Haraki
#
###############################################################################

use strict;
#use utf8;
#use open ":utf8";
#use open ":std";
use Mash::MashDebug;

package system_tag;

# タグテーブル
my %tags =
	(
	 'if'       => { 'method' => \&method_if,       'end' => 1, },
	 'repeat'   => { 'method' => \&method_repeat,   'end' => 1, },
	 'get'      => { 'method' => \&method_get,      'end' => 0, },
	 'var_set'  => { 'method' => \&method_var_set,  'end' => 0, },
	 'var_calc' => { 'method' => \&method_var_calc, 'end' => 0, },
	 'str_cat'  => { 'method' => \&method_str_cat,  'end' => 0, },
	 'str_len'  => { 'method' => \&method_str_len,  'end' => 0, },
	 'str_str'  => { 'method' => \&method_str_str,  'end' => 0, },
	 'include'  => { 'method' => \&method_include,  'end' => 0, },
	 'sub'      => { 'method' => \&method_sub,      'end' => 1, },
	 'call'     => { 'method' => \&method_call,     'end' => 0, },
	 'push'     => { 'method' => \&method_push,     'end' => 0, },
	 'pop'      => { 'method' => \&method_pop,      'end' => 0, },
	 'parse'    => { 'method' => \&method_parse,    'end' => 0, },
	 
	 'init_account_list'     => { 'method' => \&method_init_account_list,     'end' => 0, },
	 'get_account_num'       => { 'method' => \&method_get_account_num,       'end' => 0, },
	 'get_account_data'      => { 'method' => \&method_get_account_data,      'end' => 0, },
	 'fetch_account_id'      => { 'method' => \&method_fetch_account_id,      'end' => 0, },
	 
	 'init_entry_list'       => { 'method' => \&method_init_entry_list,       'end' => 0, },
	 'push_entry_list'       => { 'method' => \&method_push_entry_list,       'end' => 0, },
	 'pop_entry_list'        => { 'method' => \&method_pop_entry_list,        'end' => 0, },
	 'get_entry_num'         => { 'method' => \&method_get_entry_num,         'end' => 0, },
	 'get_entry_data'        => { 'method' => \&method_get_entry_data,        'end' => 0, },
	 'fetch_entry_id'        => { 'method' => \&method_fetch_entry_id,        'end' => 0, },
	 'get_entry_id'          => { 'method' => \&method_get_entry_id,          'end' => 0, },
	 
	 'search_entry_list'     => { 'method' => \&method_search_entry_list,     'end' => 0, },
	 
	 'make_entry_month_list' => { 'method' => \&method_make_entry_month_list, 'end' => 0, },
	 'get_entry_month_num'   => { 'method' => \&method_get_entry_month_num,   'end' => 0, },
	 'get_entry_month_data'  => { 'method' => \&method_get_entry_month_data,  'end' => 0, },
	 
	 'init_comment_list'     => { 'method' => \&method_init_comment_list,     'end' => 0, },
	 'push_comment_list'     => { 'method' => \&method_push_comment_list,     'end' => 0, },
	 'pop_comment_list'      => { 'method' => \&method_pop_comment_list,      'end' => 0, },
	 'get_comment_num'       => { 'method' => \&method_get_comment_num,       'end' => 0, },
	 'get_comment_data'      => { 'method' => \&method_get_comment_data,      'end' => 0, },
	 'fetch_comment_id'      => { 'method' => \&method_fetch_comment_id,      'end' => 0, },
	 
	 'init_category_list'    => { 'method' => \&method_init_category_list,    'end' => 0, },
	 'push_category_list'    => { 'method' => \&method_push_category_list,    'end' => 0, },
	 'pop_category_list'     => { 'method' => \&method_pop_category_list,     'end' => 0, },
	 'get_category_num'      => { 'method' => \&method_get_category_num,      'end' => 0, },
	 'get_category_data'     => { 'method' => \&method_get_category_data,     'end' => 0, },
	 'fetch_category_id'     => { 'method' => \&method_fetch_category_id,     'end' => 0, },
	 
	 'init_file_list'        => { 'method' => \&method_init_file_list,        'end' => 0, },
	 'push_file_list'        => { 'method' => \&method_push_file_list,        'end' => 0, },
	 'pop_file_list'         => { 'method' => \&method_pop_file_list,         'end' => 0, },
	 'get_file_num'          => { 'method' => \&method_get_file_num,          'end' => 0, },
	 'get_file_data'         => { 'method' => \&method_get_file_data,         'end' => 0, },
	 'fetch_file_id'         => { 'method' => \&method_fetch_file_id,         'end' => 0, },
	 'get_file_param'        => { 'method' => \&method_get_file_param,        'end' => 0, },
	 'get_file_path'         => { 'method' => \&method_get_file_path,         'end' => 0, },
	 
	 'get_cur_search_str'    => { 'method' => \&method_get_cur_search_str,    'end' => 0, },
	 
	 'get_date'              => { 'method' => \&method_get_date,              'end' => 0, },
	 'add_date'              => { 'method' => \&method_add_date,              'end' => 0, },
	 
	 'get_time'              => { 'method' => \&method_get_time,              'end' => 0, },
	 
	 'cnv_utcsec_str'        => { 'method' => \&method_cnv_utcsec_str,        'end' => 0, },
	 'cnv_date_str'          => { 'method' => \&method_cnv_date_str,          'end' => 0, },
	 'cnv_time_str'          => { 'method' => \&method_cnv_time_str,          'end' => 0, },
	 'cnv_text_str'          => { 'method' => \&method_cnv_text_str,          'end' => 0, },
	 'cnv_tag_escape'        => { 'method' => \&method_cnv_tag_escape,        'end' => 0, },
	 
	 'init_tag_list'         => { 'method' => \&method_init_tag_list,         'end' => 0, },
	 'get_tag_num'           => { 'method' => \&method_get_tag_num,           'end' => 0, },
	 'fetch_tag_name'        => { 'method' => \&method_fetch_tag_name,        'end' => 0, },
	 'get_tag_desc'          => { 'method' => \&method_get_tag_desc,          'end' => 0, },
	 'get_tag_usage'         => { 'method' => \&method_get_tag_usage,         'end' => 0, },
	 
	 'init_log_date_list'    => { 'method' => \&method_init_log_date_list,    'end' => 0, },
	 'get_log_date_num'      => { 'method' => \&method_get_log_date_num,      'end' => 0, },
	 'fetch_log_date'        => { 'method' => \&method_fetch_log_date,        'end' => 0, },
	 
	 'init_log_list'         => { 'method' => \&method_init_log_list,         'end' => 0, },
	 'push_log_list'         => { 'method' => \&method_push_log_list,         'end' => 0, },
	 'pop_log_list'          => { 'method' => \&method_pop_log_list,          'end' => 0, },
	 'get_log_num'           => { 'method' => \&method_get_log_num,           'end' => 0, },
	 'get_log_data'          => { 'method' => \&method_get_log_data,          'end' => 0, },
	 'fetch_log_id'          => { 'method' => \&method_fetch_log_id,          'end' => 0, },
	); 

# タグテーブル取得
sub get_tags
{
	return \%tags;
}

######## 変数 ########

# ユーザ定義変数
our %user_var = ();

# ユーザ定義サブルーチン
our %user_sub = ();

# スタック
our @stack = ();

# エントリ年月リスト
my @entry_list_month = ();

# インクルードファイルキャッシュ
my %include_cache = ();

my @tag_name_list = ();
my $next_tag_name_list = -1;

my @log_date_list = ();
my $next_log_date_list = -1;

######## タグメソッド ########

# 分岐
sub method_if
{
	my ($syntax, $str) = @_;
	my $return_str = '';
	my %attr = ();
	
	if($syntax =~ m/(==|!=|<=|>=|<|>|\s+eq\s+|\s+ne\s+|\s+le\s+|\s+ge\s+|\s+lt\s+|\s+gt\s+)/)
	{
		my @param;
		my $ope_type = $1;
		
		@param = split(/\s*$ope_type\s*/, $syntax, 2);
		
		if(@param == 2)
		{
			my %ope_func =
				(
				 '==' => sub { return (($_[0] ne '') && ($_[1] ne '') && ($_[0] == $_[1])); },
				 '!=' => sub { return (($_[0] ne '') && ($_[1] ne '') && ($_[0] != $_[1])); },
				 '<=' => sub { return (($_[0] ne '') && ($_[1] ne '') && ($_[0] <= $_[1])); },
				 '>=' => sub { return (($_[0] ne '') && ($_[1] ne '') && ($_[0] >= $_[1])); },
				 '<'  => sub { return (($_[0] ne '') && ($_[1] ne '') && ($_[0] <  $_[1])); },
				 '>'  => sub { return (($_[0] ne '') && ($_[1] ne '') && ($_[0] >  $_[1])); },
				 
				 'eq' => sub { return ($_[0] eq $_[1]); },
				 'ne' => sub { return ($_[0] ne $_[1]); },
				 'le' => sub { return ($_[0] le $_[1]); },
				 'ge' => sub { return ($_[0] ge $_[1]); },
				 'lt' => sub { return ($_[0] lt $_[1]); },
				 'gt' => sub { return ($_[0] gt $_[1]); },
				);
			
			$ope_type =~ s/\s//g;
			
			if(exists($ope_func{$ope_type}))
			{
				for(my $param_no = 0;$param_no < 2;$param_no++)
				{
					get_param($param[$param_no], \$param[$param_no], 1);
					
					if(!(defined($param[$param_no])))
					{
						$param[$param_no] = '';
					}
				}
				
				if($ope_func{$ope_type}($param[0], $param[1]))
				{
					$return_str = $str;
				}
			}
		}
	}
	
	return $return_str;
}

# 繰り返し
sub method_repeat
{
	my ($attr_str, $str) = @_;
	my %attr = ();
	my $return_str = '';
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'num'}))
	{
		my $loop_num = '';
		
		get_param($attr{'num'}, \$loop_num, 1);
		
		if(($loop_num ne '') && ($loop_num >= 0))
		{
			for(my $loop = 0;$loop < $loop_num;$loop++)
			{
				$return_str .= $str;
			}
		}
	}
	
	return $return_str;
}

# パラメータ取得
sub method_get
{
	my ($attr_str) = @_;
	my %attr = ();
	my $param = '';
	
	parse_attr_str($attr_str, \%attr);
	
	if((exists($attr{'param'})) && (get_param($attr{'param'}, \$param) == 0))
	{
		if(exists($attr{'var'}))
		{
			if(check_var_name($attr{'var'}) == 0)
			{
				$user_var{$attr{'var'}} = $param;
			}
		}
		else
		{
			return $param;
		}
	}
	
	return '';
}

# ユーザ任意の変数に値を設定
sub method_var_set
{
	my ($attr_str) = @_;
	my %attr = ();
	
	parse_attr_str($attr_str, \%attr);
	
	if((exists($attr{'var'})) && (check_var_name($attr{'var'}) == 0))
	{
		my $param = '';
		
		if(exists($attr{'param'}))
		{
			get_param($attr{'param'}, \$param, 1);
		}
		
		$user_var{$attr{'var'}} = $param;
	}
	
	return '';
}

# ユーザ任意の変数に数値演算
sub method_var_calc
{
	my ($attr_str) = @_;
	my %attr = ();
	
	parse_attr_str($attr_str, \%attr);
	
	if((exists($attr{'type'})) && (exists($attr{'var'})) && (check_var_name($attr{'var'}) == 0) && (exists($user_var{$attr{'var'}})))
	{
		my $param = '';
		
		if(exists($attr{'param'}))
		{
			get_param($attr{'param'}, \$param, 1);
		}
		
		if($attr{'type'} eq 'add')
		{
			$user_var{$attr{'var'}} += $param;
		}
		elsif($attr{'type'} eq 'sub')
		{
			$user_var{$attr{'var'}} -= $param;
		}
		elsif($attr{'type'} eq 'mul')
		{
			$user_var{$attr{'var'}} *= $param;
		}
		elsif($attr{'type'} eq 'div')
		{
			$user_var{$attr{'var'}} /= $param;
		}
	}
	
	return '';
}

# 文字列合成
sub method_str_cat
{
	my ($attr_str) = @_;
	my %attr = ();
	
	parse_attr_str($attr_str, \%attr);
	
	if((exists($attr{'str1'})) && (exists($attr{'str2'})))
	{
		my $str1 = '';
		my $str2 = '';
		
		get_param($attr{'str1'}, \$str1, 1);
		get_param($attr{'str2'}, \$str2, 1);
		
		$str1 .= $str2;
		
		if(exists($attr{'var'}))
		{
			if(check_var_name($attr{'var'}) == 0)
			{
				$user_var{$attr{'var'}} = $str1;
			}
		}
		else
		{
			return $str1;
		}
	}
	
	return '';
}

# 文字列長さ取得
sub method_str_len
{
	my ($attr_str) = @_;
	my %attr = ();
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'str'}))
	{
		my $str    = '';
		my $length = 0;
		
		get_param($attr{'str'}, \$str, 1);
		
		$length = length($str);
		
		if(exists($attr{'var'}))
		{
			if(check_var_name($attr{'var'}) == 0)
			{
				$user_var{$attr{'var'}} = $length;
			}
		}
		else
		{
			return $length;
		}
	}
	
	return '';
}

# 文字列内に指定文字列が存在するか確認
sub method_str_str
{
	my ($attr_str) = @_;
	my %attr = ();
	
	parse_attr_str($attr_str, \%attr);
	
	if((exists($attr{'str1'})) && (exists($attr{'str2'})))
	{
		my $str1  = '';
		my $str2  = '';
		my $index = -1;
		
		get_param($attr{'str1'}, \$str1, 1);
		get_param($attr{'str2'}, \$str2, 1);
		
		$index = index($str1, $str2);
		
		if(exists($attr{'var'}))
		{
			if(check_var_name($attr{'var'}) == 0)
			{
				$user_var{$attr{'var'}} = $index;
			}
		}
		else
		{
			return $index;
		}
	}
	
	return '';
}

# インクルード
sub method_include
{
	my ($attr_str, $str) = @_;
	my %attr = ();
	my $return_str = '';
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'file'}))
	{
		my $include_file = '';
		
		get_param($attr{'file'}, \$include_file, 1);
		
		if($include_file ne '')
		{
			if(exists($include_cache{$include_file}))
			{
				$return_str = $include_cache{$include_file};
			}
			elsif(-e($include_file))
			{
				if(open(FILE, $include_file))
				{
					$return_str = join('', <FILE>);
					close(FILE);
					
					$include_cache{$include_file} = $return_str;
				}
				else
				{
					$return_str = "\"$include_file\" open error.\n";
				}
			}
			else
			{
				$return_str = "nothing include file \"$include_file\"\n";
			}
		}
	}
	
	return $return_str;
}

# サブルーチン定義
sub method_sub
{
	my ($attr_str, $str) = @_;
	my %attr = ();
	
	parse_attr_str($attr_str, \%attr);
	
	if((exists($attr{'name'})) && ($attr{'name'} =~ m/\w*/))
	{
		$user_sub{$attr{'name'}} = $str;
	}
	
	return '';
}

# サブルーチン呼び出し
sub method_call
{
	my ($attr_str) = @_;
	my %attr = ();
	my $return_str = '';
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'name'}))
	{
		$return_str = $user_sub{$attr{'name'}};
	}
	
	return $return_str;
}

# 値をスタックにプッシュ
sub method_push
{
	my ($attr_str, $str) = @_;
	my %attr = ();
	my $param = '';
	
	parse_attr_str($attr_str, \%attr);
	
	get_param($attr{'param'}, \$param, 1);
	
	push(@stack, $param);
	
	return '';
}

# 値をスタックからポップ
sub method_pop
{
	my ($attr_str, $str) = @_;
	my %attr = ();
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'var'}))
	{
		if(check_var_name($attr{'var'}) == 0)
		{
			$user_var{$attr{'var'}} = pop(@stack);
		}
	}
	else
	{
		return pop(@stack);
	}
	
	return '';
}

# 指定文字列を解析する
sub method_parse
{
	my ($attr_str, $str) = @_;
	my %attr = ();
	
	parse_attr_str($attr_str, \%attr);
	
	if((exists($attr{'str'})) && (exists($attr{'permission'})))
	{
		my $parse_str  = '';
		my $permission = '';
		
		get_param($attr{'str'}, \$parse_str, 1);
		get_param($attr{'permission'}, \$permission, 1);
		
		main::parse_tag($permission, \$parse_str);
		
		if(exists($attr{'var'}))
		{
			if(check_var_name($attr{'var'}) == 0)
			{
				$user_var{$attr{'var'}} = $parse_str;
			}
		}
		else
		{
			return $parse_str;
		}
	}
	
	return '';
}

#### アカウント ####

# アカウントリスト初期化
sub method_init_account_list
{
	my ($attr_str) = @_;
	my %attr    = ();
	my $start   = 0;
	my $max_num = 0;
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'start'}))
	{
		get_param($attr{'start'}, \$start, 1);
		
		delete($attr{'start'});
	}
	
	if(exists($attr{'max_num'}))
	{
		get_param($attr{'max_num'}, \$max_num, 1);
		
		delete($attr{'max_num'});
	}
	
	foreach my $type (keys(%attr))
	{
		my $param = '';
		
		get_param($attr{$type}, \$param, 1);
		
		$attr{$type} = $param;
	}
	
	main::init_account_db_list(\%attr, $start, $max_num);
	
	return '';
}

# アカウント数取得
sub method_get_account_num
{
	my ($attr_str) = @_;
	my %attr = ();
	my $account_num = main::get_account_db_list_size();
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'var'}))
	{
		if(check_var_name($attr{'var'}) == 0)
		{
			$user_var{$attr{'var'}} = $account_num;
		}
	}
	else
	{
		return $account_num;
	}
	
	return '';
}

# アカウントデータ取得
sub method_get_account_data
{
	my ($attr_str) = @_;
	my %attr = ();
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'param'}))
	{
		my $index_no = -1;
		
		if(exists($attr{'id'}))
		{
			my $user_id = '';
			
			get_param($attr{'id'}, \$user_id, 1);
			
			if($user_id ne '')
			{
				$index_no = main::get_account_index($user_id);
			}
			else
			{
				$index_no = -2;
			}
		}
		
		if($index_no >= -1)
		{
			my $param = '';
			my $data  = '';
			
			get_param($attr{'param'}, \$param, 1);
			
			if($param ne '')
			{
				$data = main::get_account_data($index_no, $param);
				
				if(exists($attr{'var'}))
				{
					if(check_var_name($attr{'var'}) == 0)
					{
						$user_var{$attr{'var'}} = $data;
					}
				}
				else
				{
					return $data;
				}
			}
		}
	}
	
	return '';
}

# アカウントIDを返す
sub method_fetch_account_id
{
	my ($attr_str) = @_;
	my %attr = ();
	my $ref_hash = main::fetch_account_db_list();
	
	if((defined($ref_hash)) && (exists($$ref_hash{'id'})))
	{
		parse_attr_str($attr_str, \%attr);
		
		if(exists($attr{'var'}))
		{
			if(check_var_name($attr{'var'}) == 0)
			{
				$user_var{$attr{'var'}} = $$ref_hash{'id'};
			}
		}
		else
		{
			return $$ref_hash{'id'};
		}
	}
	
	return '';
}

#### エントリ #### 

# エントリリスト初期化
sub method_init_entry_list
{
	my ($attr_str) = @_;
	my %attr = ();
	
	my $start   = 0;
	my $max_num = 0;
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'start'}))
	{
		get_param($attr{'start'}, \$start, 1);
		
		delete($attr{'start'});
	}
	
	if(exists($attr{'max_num'}))
	{
		get_param($attr{'max_num'}, \$max_num, 1);
		
		delete($attr{'max_num'});
	}
	
	foreach my $type (keys(%attr))
	{
		my $param = '';
		
		get_param($attr{$type}, \$param, 1);
		
		$attr{$type} = $param;
	}
	
	main::init_entry_db_list(\%attr, $start, $max_num);
	
	return '';
}

# エントリDB用検索ルーチン
#  param[in] : チェック対象のハッシュリファレンス
#  param[in] : 条件アトリビュートリファレンス
#  return : チェック結果
sub search_and_entry
{
	my ($ref_hash, $ref_attr, $ref_option) = @_;
	my $ref_keywords = $$ref_option{'keyword'};
	my $ref_items    = $$ref_option{'item'};
	
	if(main::condition_entry($ref_hash, $ref_attr))
	{
		foreach my $keyword (@$ref_keywords)
		{
			my $hit_flag = 0;
			
			foreach my $item (@$ref_items)
			{
				if(exists($$ref_hash{$item}))
				{
					my $value    = $$ref_hash{$item};
					my $ref_type = ref($value);
					
					if($ref_type eq '')
					{
						if(index($value, $keyword) != -1)
						{
							$hit_flag = 1;
							
							last;
						}
					}
					elsif($ref_type eq 'ARRAY')
					{
						foreach my $array_value (@$value)
						{
							if(index($array_value, $keyword) != -1)
							{
								$hit_flag = 1;
								
								last;
							}
						}
						
						if($hit_flag == 1)
						{
							last;
						}
					}
				}
			}
			
			if($hit_flag == 0)
			{
				# キーワードが検索項目のいずれか1つにもヒットしなかったら該当なしで終了
				return 0;
			}
		}
		
		# 全てのキーワードが検索項目のいずれかにヒットしたので該当ありで終了
		return 1;
	}
	
	return 0;
}

# エントリDB用検索ルーチン
#  param[in] : チェック対象のハッシュリファレンス
#  param[in] : 条件アトリビュートリファレンス
#  return : チェック結果
sub search_or_entry
{
	my ($ref_hash, $ref_attr, $ref_option) = @_;
	my $ref_keywords = $$ref_option{'keyword'};
	my $ref_items    = $$ref_option{'item'};
	
	if(main::condition_entry($ref_hash, $ref_attr))
	{
		foreach my $keyword (@$ref_keywords)
		{
			my $hit_flag = 0;
			
			foreach my $item (@$ref_items)
			{
				if(exists($$ref_hash{$item}))
				{
					my $value    = $$ref_hash{$item};
					my $ref_type = ref($value);
					
					if($ref_type eq '')
					{
						if(index($value, $keyword) != -1)
						{
							$hit_flag = 1;
							
							last;
						}
					}
					elsif($ref_type eq 'ARRAY')
					{
						foreach my $array_value (@$value)
						{
							if(index($array_value, $keyword) != -1)
							{
								$hit_flag = 1;
								
								last;
							}
						}
						
						if($hit_flag == 1)
						{
							last;
						}
					}
				}
			}
			
			if($hit_flag == 1)
			{
				# キーワードが検索項目のいずれか1つにもヒットしたら該当ありで終了
				return 1;
			}
		}
		
		# 全てのキーワードが検索項目のいずれにもヒットしなかったので該当なしで終了
		return 0;
	}
	
	return 0;
}

# 検索条件に合致したエントリリスト初期化
sub method_search_entry_list
{
	my ($attr_str) = @_;
	my %attr = ();
	
	my $start       = 0;
	my $max_num     = 0;
	my $search_type = '';
	my @keywords    = ();
	my @items       = ();
	my $cond_func   = undef;
	my %option      = ();
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'start'}))
	{
		get_param($attr{'start'}, \$start, 1);
		
		delete($attr{'start'});
	}
	
	if(exists($attr{'max_num'}))
	{
		get_param($attr{'max_num'}, \$max_num, 1);
		
		delete($attr{'max_num'});
	}
	
	if(exists($attr{'search_type'}))
	{
		get_param($attr{'search_type'}, \$search_type, 1);
		
		delete($attr{'search_type'});
	}
	
	if(exists($attr{'keyword'}))
	{
		my $keyword = '';
		
		get_param($attr{'keyword'}, \$keyword, 1);
		
		delete($attr{'keyword'});
		
		if($keyword ne '')
		{
			@keywords = split(/\s+/, $keyword);
		}
	}
	
	if(exists($attr{'item'}))
	{
		my $item = '';
		
		get_param($attr{'item'}, \$item, 1);
		
		delete($attr{'item'});
		
		if($item ne '')
		{
			@items = split(/\s+/, $item);
		}
	}
	
	# アトリビュート
	foreach my $type (keys(%attr))
	{
		my $param = '';
		
		get_param($attr{$type}, \$param, 1);
		
		$attr{$type} = $param;
	}
	
	if((@keywords > 0) && (@items > 0))
	{
		# 検索条件及び検索関数の設定
		$option{'keyword'} = \@keywords;
		$option{'item'}    = \@items;
		
		if(!(defined($search_type)) || (($search_type ne 'and') && ($search_type ne 'or')))
		{
			$search_type = 'and';
		}
		
		if($search_type eq 'and')
		{
			$cond_func = \&search_and_entry;
		}
		else
		{
			$cond_func = \&search_or_entry;
		}
	}
	
	main::init_entry_db_list(\%attr, $start, $max_num, $cond_func, \%option);
	
	return '';
}

# エントリリストのプッシュ
sub method_push_entry_list
{
	main::push_entry_db_list();
	
	return '';
}

# エントリリストのポップ
sub method_pop_entry_list
{
	main::pop_entry_db_list();
	
	return '';
}

# エントリリストのエントリ数取得
sub method_get_entry_num
{
	my ($attr_str) = @_;
	my %attr = ();
	my $entry_num = main::get_entry_db_list_size();
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'var'}))
	{
		if(check_var_name($attr{'var'}) == 0)
		{
			$user_var{$attr{'var'}} = $entry_num;
		}
	}
	else
	{
		return $entry_num;
	}
	
	return '';
}

# エントリIDからエントリ各種データ取得
sub method_get_entry_data
{
	my ($attr_str) = @_;
	my %attr = ();
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'param'}))
	{
		my $entry_id = '';
		
		if(exists($attr{'id'}))
		{
			get_param($attr{'id'}, \$entry_id, 1);
			
			if($entry_id eq '')
			{
				return '';
			}
		}
		
		my $param = '';
		
		if(get_param($attr{'param'}, \$param) != 0)
		{
			$param = delete_quote($attr{'param'});
		}
		
		if($param ne '')
		{
			my $parse      = '';
			my $escape     = '';
			my $element_no = '';
			my $data       = '';
			
			if(exists($attr{'parse'}))
			{
				get_param($attr{'parse'}, \$parse, 1);
				
				if($parse eq '')
				{
					$parse = 'invalid';
				}
			}
			else
			{
				$parse = 'invalid';
			}
			
			if(exists($attr{'escape'}))
			{
				get_param($attr{'escape'}, \$escape, 1);
			}
			
			if($param =~ m/(\w+)(\{\s*(\$?\w+)\s*\})/)
			{
				my $key = $3;
				
				$param = $1;
				
				if(check_var_name($key) == 0)
				{
					if(exists($user_var{$key}))
					{
						$element_no = $user_var{$key};
					}
				}
				elsif($key !~ m/[^0-9]/)
				{
					$element_no = $key;
				}
			}
			
			$data = main::get_entry_data($entry_id, $param, $element_no);
			
			if($parse eq 'invalid')
			{
				main::invalid_tag(\$data);
			}
			elsif($parse eq 'none')
			{
				$data =~ s/\[/&#091;/g;
				$data =~ s/\]/&#093;/g;
#				$data = "[no_parse]" . $data . "[/no_parse]";
			}
			else
			{
				main::parse_tag($parse, \$data);
			}
			
			if($escape eq 'true')
			{
				$data =~ s/</&lt;/g;
				$data =~ s/>/&gt;/g;
				$data =~ s/"/&quot;/g;
				$data =~ s/'/&#039;/g;
			}
			
			if(exists($attr{'var'}))
			{
				if(check_var_name($attr{'var'}) == 0)
				{
					$user_var{$attr{'var'}} = $data;
				}
			}
			else
			{
				return $data;
			}
		}
	}
	
	return '';
}

# エントリIDを返す
sub method_fetch_entry_id
{
	my ($attr_str) = @_;
	my %attr = ();
	my $ref_hash = main::fetch_entry_db_list();
	
	if((defined($ref_hash)) && (exists($$ref_hash{'id'})))
	{
		parse_attr_str($attr_str, \%attr);
		
		if(exists($attr{'var'}))
		{
			if(check_var_name($attr{'var'}) == 0)
			{
				$user_var{$attr{'var'}} = $$ref_hash{'id'};
			}
		}
		else
		{
			return $$ref_hash{'id'};
		}
	}
	
	return '';
}

# 指定IDからn番目のエントリIDを返す
sub method_get_entry_id
{
	my ($attr_str) = @_;
	my %attr = ();
	my $entry_id = '';
	my $offset   = 0;
	my $ret_id   = '';
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'id'}))
	{
		get_param($attr{'id'}, \$entry_id, 1);
	}
	
	if(exists($attr{'offset'}))
	{
		get_param($attr{'offset'}, \$offset, 1);
	}
	
	$ret_id = main::get_entry_id($entry_id, $offset);
	
	if(exists($attr{'var'}))
	{
		if(check_var_name($attr{'var'}) == 0)
		{
			$user_var{$attr{'var'}} = $ret_id;
		}
	}
	else
	{
		return $ret_id;
	}
	
	return '';
}

# エントリ年月リスト生成
sub method_make_entry_month_list
{
	main::make_entry_date_list('month', \@entry_list_month);
	
	return '';
}

# エントリ年月リスト数取得
sub method_get_entry_month_num
{
	my ($attr_str) = @_;
	my %attr = ();
	my $list_num = @entry_list_month;
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'var'}))
	{
		if(check_var_name($attr{'var'}) == 0)
		{
			$user_var{$attr{'var'}} = $list_num;
		}
	}
	else
	{
		return $list_num;
	}
	
	return '';
}

# エントリ年月データ取得
sub method_get_entry_month_data
{
	my ($attr_str) = @_;
	my %attr = ();
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'index'}))
	{
		my $index_no = -1;
		
		if(get_param($attr{'index'}, \$index_no) != 0)
		{
			$index_no = delete_quote($attr{'index'});
		}
		
		if($index_no >= 0)
		{
			my $month_str = $entry_list_month[$index_no];
			
			if(exists($attr{'var'}))
			{
				if(check_var_name($attr{'var'}) == 0)
				{
					$user_var{$attr{'var'}} = $month_str;
				}
			}
			else
			{
				return $month_str;
			}
		}
	}
	
	return '';
}

#### コメント #### 

# コメントリスト初期化
sub method_init_comment_list
{
	my ($attr_str) = @_;
	my %attr    = ();
	my $start   = 0;
	my $max_num = 0;
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'start'}))
	{
		get_param($attr{'start'}, \$start, 1);
		
		delete($attr{'start'});
	}
	
	if(exists($attr{'max_num'}))
	{
		get_param($attr{'max_num'}, \$max_num, 1);
		
		delete($attr{'max_num'});
	}
	
	foreach my $type (keys(%attr))
	{
		my $param = '';
		
		get_param($attr{$type}, \$param, 1);
		
		$attr{$type} = $param;
	}
	
	main::init_comment_db_list(\%attr, $start, $max_num);
	
	return '';
}

# コメントリストのプッシュ
sub method_push_comment_list
{
	main::push_comment_db_list();
	
	return '';
}

# コメントリストのポップ
sub method_pop_comment_list
{
	main::pop_comment_db_list();
	
	return '';
}

# コメントリストのコメントデータ数取得
sub method_get_comment_num
{
	my ($attr_str) = @_;
	my %attr = ();
	my $comment_num = main::get_comment_db_list_size();
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'var'}))
	{
		if(check_var_name($attr{'var'}) == 0)
		{
			$user_var{$attr{'var'}} = $comment_num;
		}
	}
	else
	{
		return $comment_num;
	}
	
	return '';
}

# コメントIDからコメント各種データ取得
sub method_get_comment_data
{
	my ($attr_str) = @_;
	my %attr = ();
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'param'}))
	{
		my $index_no = -1;
		
		if(exists($attr{'id'}))
		{
			my $comment_id = '';
			
			get_param($attr{'id'}, \$comment_id, 1);
			
			if($comment_id ne '')
			{
				$index_no = main::get_comment_index($comment_id);
			}
			else
			{
				$index_no = -2;
			}
		}
		
		if($index_no >= -1)
		{
			my $param = '';
			
			if(get_param($attr{'param'}, \$param) != 0)
			{
				$param = delete_quote($attr{'param'});
			}
			
			if($param ne '')
			{
				my $parse  = '';
				my $escape = '';
				my $data   = '';
				
				if(get_param($attr{'parse'}, \$parse) != 0)
				{
					$parse = delete_quote($attr{'parse'});
					
					if((!defined($parse)) || ($parse eq ''))
					{
						$parse = 'invalid';
					}
				}
				
				if(exists($attr{'escape'}))
				{
					get_param($attr{'escape'}, \$escape, 1);
				}
				
				$data = main::get_comment_data($index_no, $param);
				
				if($parse eq 'invalid')
				{
					main::invalid_tag(\$data);
				}
				elsif($parse eq 'none')
				{
					main::invalid_tag(\$data);
				}
				else
				{
					main::parse_tag($parse, \$data);
				}
				
				if($escape eq 'true')
				{
					$data =~ s/</&lt;/g;
					$data =~ s/>/&gt;/g;
					$data =~ s/"/&quot;/g;
					$data =~ s/'/&#039;/g;
				}
				
				if(exists($attr{'var'}))
				{
					if(check_var_name($attr{'var'}) == 0)
					{
						$user_var{$attr{'var'}} = $data;
					}
				}
				else
				{
					return $data;
				}
			}
		}
	}
	
	return '';
}

# コメントIDを返す
sub method_fetch_comment_id
{
	my ($attr_str) = @_;
	my %attr = ();
	my $ref_hash = main::fetch_comment_db_list();
	
	if((defined($ref_hash)) && (exists($$ref_hash{'id'})))
	{
		parse_attr_str($attr_str, \%attr);
		
		if(exists($attr{'var'}))
		{
			if(check_var_name($attr{'var'}) == 0)
			{
				$user_var{$attr{'var'}} = $$ref_hash{'id'};
			}
		}
		else
		{
			return $$ref_hash{'id'};
		}
	}
	
	return '';
}

#### カテゴリ #### 

# カテゴリリスト初期化
sub method_init_category_list
{
	my ($attr_str) = @_;
	my %attr    = ();
	my $start   = 0;
	my $max_num = 0;
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'start'}))
	{
		if(get_param($attr{'start'}, \$start) != 0)
		{
			$start = delete_quote($attr{'start'});
		}
		
		delete($attr{'start'});
	}
	
	if(exists($attr{'max_num'}))
	{
		if(get_param($attr{'max_num'}, \$max_num) != 0)
		{
			$max_num = delete_quote($attr{'max_num'});
		}
		
		delete($attr{'max_num'});
	}
	
	foreach my $type (keys(%attr))
	{
		my $param = '';
		
		if(get_param($attr{$type}, \$param) != 0)
		{
			$param = delete_quote($attr{$type});
		}
		
		$attr{$type} = $param;
	}
	
	main::init_category_db_list(\%attr, $start, $max_num);
	
	return '';
}

# カテゴリリストのプッシュ
sub method_push_category_list
{
	main::push_category_db_list();
	
	return '';
}

# カテゴリリストのポップ
sub method_pop_category_list
{
	main::pop_category_db_list();
	
	return '';
}

# カテゴリリストのカテゴリデータ数取得
sub method_get_category_num
{
	my ($attr_str) = @_;
	my %attr = ();
	my $category_num = main::get_category_db_list_size();
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'var'}))
	{
		if(check_var_name($attr{'var'}) == 0)
		{
			$user_var{$attr{'var'}} = $category_num;
		}
	}
	else
	{
		return $category_num;
	}
	
	return '';
}

# カテゴリIDからカテゴリ各種データ取得
sub method_get_category_data
{
	my ($attr_str) = @_;
	my %attr = ();
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'param'}))
	{
		my $index_no = -1;
		
		if(exists($attr{'id'}))
		{
			my $category_id = '';
			
			if(get_param($attr{'id'}, \$category_id) != 0)
			{
				$category_id = delete_quote($attr{'id'});
			}
			
			if($category_id ne '')
			{
				$index_no = main::get_category_index($category_id);
			}
			else
			{
				$index_no = -2;
			}
		}
		
		if($index_no >= -1)
		{
			my $param = '';
			my $data  = '';
			
			if(get_param($attr{'param'}, \$param) != 0)
			{
				$param = delete_quote($attr{'param'});
			}
			
			if($param ne '')
			{
				$data = main::get_category_data($index_no, $param);
				
				if(exists($attr{'var'}))
				{
					if(check_var_name($attr{'var'}) == 0)
					{
						$user_var{$attr{'var'}} = $data;
					}
				}
				else
				{
					return $data;
				}
			}
		}
	}
	
	return '';
}

# カテゴリIDを返す
sub method_fetch_category_id
{
	my ($attr_str) = @_;
	my %attr = ();
	my $ref_hash = main::fetch_category_db_list();
	
	if((defined($ref_hash)) && (exists($$ref_hash{'id'})))
	{
		parse_attr_str($attr_str, \%attr);
		
		if(exists($attr{'var'}))
		{
			if(check_var_name($attr{'var'}) == 0)
			{
				$user_var{$attr{'var'}} = $$ref_hash{'id'};
			}
		}
		else
		{
			return $$ref_hash{'id'};
		}
	}
	
	return '';
}

#### ファイル #### 

# ファイルリスト初期化
sub method_init_file_list
{
	my ($attr_str) = @_;
	my %attr    = ();
	my $start   = 0;
	my $max_num = 0;
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'start'}))
	{
		if(get_param($attr{'start'}, \$start) != 0)
		{
			$start = delete_quote($attr{'start'});
		}
		
		delete($attr{'start'});
	}
	
	if(exists($attr{'max_num'}))
	{
		if(get_param($attr{'max_num'}, \$max_num) != 0)
		{
			$max_num = delete_quote($attr{'max_num'});
		}
		
		delete($attr{'max_num'});
	}
	
	foreach my $type (keys(%attr))
	{
		my $param = '';
		
		if(get_param($attr{$type}, \$param) != 0)
		{
			$param = delete_quote($attr{$type});
		}
		
		$attr{$type} = $param;
	}
	
	main::init_file_db_list(\%attr, $start, $max_num);
	
	return '';
}

# ファイルリストのプッシュ
sub method_push_file_list
{
	main::push_file_db_list();
	
	return '';
}

# ファイルリストのポップ
sub method_pop_file_list
{
	main::pop_file_db_list();
	
	return '';
}

# ファイルリストのファイルデータ数取得
sub method_get_file_num
{
	my ($attr_str) = @_;
	my %attr = ();
	my $file_num = main::get_file_db_list_size();
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'var'}))
	{
		if(check_var_name($attr{'var'}) == 0)
		{
			$user_var{$attr{'var'}} = $file_num;
		}
	}
	else
	{
		return $file_num;
	}
	
	return '';
}

# ファイルIDからファイル各種データ取得
sub method_get_file_data
{
	my ($attr_str) = @_;
	my %attr = ();
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'param'}))
	{
		my $index_no = -1;
		
		if(exists($attr{'id'}))
		{
			my $file_id = '';
			
			if(get_param($attr{'id'}, \$file_id) != 0)
			{
				$file_id = delete_quote($attr{'id'});
			}
			
			if($file_id ne '')
			{
				$index_no = main::get_file_index($file_id);
			}
			else
			{
				$index_no = -2;
			}
		}
		
		if($index_no >= -1)
		{
			my $param = '';
			my $data  = '';
			
			if(get_param($attr{'param'}, \$param) != 0)
			{
				$param = delete_quote($attr{'param'});
			}
			
			if($param ne '')
			{
				$data = main::get_file_data($index_no, $param);
				
				if(exists($attr{'var'}))
				{
					if(check_var_name($attr{'var'}) == 0)
					{
						$user_var{$attr{'var'}} = $data;
					}
				}
				else
				{
					return $data;
				}
			}
		}
	}
	
	return '';
}

# ファイルIDを返す
sub method_fetch_file_id
{
	my ($attr_str) = @_;
	my %attr = ();
	my $ref_hash = main::fetch_file_db_list();
	
	if((defined($ref_hash)) && (exists($$ref_hash{'id'})))
	{
		parse_attr_str($attr_str, \%attr);
		
		if(exists($attr{'var'}))
		{
			if(check_var_name($attr{'var'}) == 0)
			{
				$user_var{$attr{'var'}} = $$ref_hash{'id'};
			}
		}
		else
		{
			return $$ref_hash{'id'};
		}
	}
	
	return '';
}

# ファイルの指定パラメータを取得
sub method_get_file_param
{
	my ($attr_str) = @_;
	my %attr = ();
	
	parse_attr_str($attr_str, \%attr);
	
	if((exists($attr{'path'})) && (exists($attr{'param'})))
	{
		my $path  = '';
		my $param = '';
		my $data  = '';
		
		if(get_param($attr{'path'}, \$path) != 0)
		{
			$path = delete_quote($attr{'path'});
		}
		
		if(get_param($attr{'param'}, \$param) != 0)
		{
			$param = delete_quote($attr{'param'});
		}
		
		if($path ne '')
		{
			if($param eq 'size')
			{
				$data = -s $path;
			}
			else
			{
				return '';
			}
			
			if(exists($attr{'var'}))
			{
				if(check_var_name($attr{'var'}) == 0)
				{
					$user_var{$attr{'var'}} = $data;
				}
			}
			else
			{
				return $data;
			}
		}
	}
	
	return '';
}

# ファイルIDが指すファイルのパスを取得
sub method_get_file_path
{
	my ($attr_str) = @_;
	my %attr = ();
	
	parse_attr_str($attr_str, \%attr);
	
	my $index_no = -1;
	
	if(exists($attr{'id'}))
	{
		my $file_id = '';
		
		if(get_param($attr{'id'}, \$file_id) != 0)
		{
			$file_id = delete_quote($attr{'id'});
		}
		
		if($file_id ne '')
		{
			$index_no = main::get_file_index($file_id);
		}
		else
		{
			$index_no = -2;
		}
	}
	
	if($index_no >= -1)
	{
		my $ref_root = main::get_system_param_ref('file_root_path');
		my $path     = $$ref_root . main::get_file_data($index_no, "path");
		
		if(exists($attr{'var'}))
		{
			if(check_var_name($attr{'var'}) == 0)
			{
				$user_var{$attr{'var'}} = $path;
			}
		}
		else
		{
			return $path;
		}
	}
	
	return '';
}

#### 日付・時刻 ####

# 日付(yyyy-mm-dd)を取得
sub method_get_date
{
	my ($attr_str) = @_;
	my %attr = ();
	my $date_str = '';
	
	parse_attr_str($attr_str, \%attr);
	
	Mash::MashTime::get_current_date_time(\$date_str);
	
	if(exists($attr{'var'}))
	{
		if(check_var_name($attr{'var'}) == 0)
		{
			$user_var{$attr{'var'}} = $date_str;
		}
	}
	else
	{
		return $date_str;
	}
	
	
	return '';
}

# 日付(yyyy-mm-dd)に指定した年数・月数・日数を加算する（紀元前は考慮しない）
sub method_add_date
{
	my ($attr_str) = @_;
	my %attr = ();
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'date'}))
	{
		my $date_str  = '';
		my $add_year  = 0;
		my $add_month = 0;
		my $add_day   = 0;
		
		if(get_param($attr{'date'}, \$date_str) != 0)
		{
			$date_str = delete_quote($attr{'date'});
		}
		
		if(get_param($attr{'add_year'}, \$add_year) != 0)
		{
			$add_year = delete_quote($attr{'add_year'});
		}
		
		if(get_param($attr{'add_month'}, \$add_month) != 0)
		{
			$add_month = delete_quote($attr{'add_month'});
		}
		
		if(get_param($attr{'add_day'}, \$add_day) != 0)
		{
			$add_day = delete_quote($attr{'add_day'});
		}
		
		if(!(defined($add_year)))
		{
			$add_year = 0;
		}
		
		if(!(defined($add_month)))
		{
			$add_month = 0;
		}
		
		if(!(defined($add_day)))
		{
			$add_day = 0;
		}
		
		if($date_str ne '')
		{
			my ($year, $month, $day) = split(/-/, $date_str, 3);
			
			if((defined($year)) && ($year > 0))
			{
				if((!defined($month)) || ($month <= 0))
				{
					$month = 1;
				}
				
				if((!defined($day)) || ($day <= 0))
				{
					$day = 1;
				}
				
				($year, $month, $day) = Mash::MashTime::add_date($year, $month, $day, $add_year, $add_month, $add_day);
				
				$date_str = sprintf("%.4d-%.2d-%.2d", $year, $month, $day);
				
				if(exists($attr{'var'}))
				{
					if(check_var_name($attr{'var'}) == 0)
					{
						$user_var{$attr{'var'}} = $date_str;
					}
				}
				else
				{
					return $date_str;
				}
			}
		}
	}
	
	return '';
}

# 時刻(hh:mm:ss)を取得
sub method_get_time
{
	my ($attr_str) = @_;
	my %attr = ();
	my $time_str = '';
	
	Mash::MashTime::get_current_date_time(undef, \$time_str);
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'var'}))
	{
		if(check_var_name($attr{'var'}) == 0)
		{
			$user_var{$attr{'var'}} = $time_str;
		}
	}
	else
	{
		return $time_str;
	}
	
	
	return '';
}

#### 文字列変換 ####

# 1970/1/1 00:00:00 UTCからの秒数を指定書式に変換
sub method_cnv_utcsec_str
{
	my ($attr_str) = @_;
	my %attr = ();
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'param'}))
	{
		my $param = 0;
		
		if(get_param($attr{'param'}, \$param) != 0)
		{
			$param = delete_quote($attr{'param'})
		}
		
		if(exists($attr{'format'}))
		{
			my $format_str = '';
			my $ret_str    = '';
			my ($second, $minute, $hour, $day, $month, $year) = localtime($param);
			
			if(get_param($attr{'format'}, \$format_str) != 0)
			{
				$format_str = delete_quote($attr{'format'})
			}
			
			$year  += 1900;
			$month += 1;
			
			$format_str = main::ymd_2_format_str($format_str, $year, $month,  $day);
			$ret_str    = main::hmd_2_format_str($format_str, $hour, $minute, $second);
			
			if(exists($attr{'var'}))
			{
				if(check_var_name($attr{'var'}) == 0)
				{
					$user_var{$attr{'var'}} = $ret_str;
				}
			}
			else
			{
				return $ret_str;
			}
		}
	}
	
	return '';
}

# 年月日(yyyy-mm-dd)を指定書式に変換
sub method_cnv_date_str
{
	my ($attr_str) = @_;
	my %attr = ();
	
	parse_attr_str($attr_str, \%attr);
	
	if((exists($attr{'date'})) && (exists($attr{'format'})))
	{
		my $date_str   = '';
		my $format_str = '';
		
		if(get_param($attr{'date'}, \$date_str) != 0)
		{
			$date_str = delete_quote($attr{'date'});
		}
		
		if(get_param($attr{'format'}, \$format_str) != 0)
		{
			$format_str = delete_quote($attr{'format'})
		}
		
		if(main::check_date_str($date_str) == 0)
		{
			my ($year, $month, $day) = split(/-/, $date_str, 3);
			my $ret_str = main::ymd_2_format_str($format_str, $year, $month, $day);
			
			if(exists($attr{'var'}))
			{
				if(check_var_name($attr{'var'}) == 0)
				{
					$user_var{$attr{'var'}} = $ret_str;
				}
			}
			else
			{
				return $ret_str;
			}
		}
	}
	
	return '';
}

# 時刻(hh:mm:dd)を指定書式に変換
sub method_cnv_time_str
{
	my ($attr_str) = @_;
	my %attr = ();
	
	parse_attr_str($attr_str, \%attr);
	
	if((exists($attr{'time'})) && (exists($attr{'format'})))
	{
		my $time_str   = '';
		my $format_str = '';
		
		if(get_param($attr{'time'}, \$time_str) != 0)
		{
			$time_str = delete_quote($attr{'time'});
		}
		
		if(get_param($attr{'format'}, \$format_str) != 0)
		{
			$format_str = delete_quote($attr{'format'});
		}
		
		if(main::check_time_str($time_str) == 0)
		{
			my ($hour, $minute, $second) = split(/:/, $time_str, 3);
			my $ret_str = main::hmd_2_format_str($format_str, $hour, $minute, $second);
			
			if(exists($attr{'var'}))
			{
				if(check_var_name($attr{'var'}) == 0)
				{
					$user_var{$attr{'var'}} = $ret_str;
				}
			}
			else
			{
				return $ret_str;
			}
		}
	}
	
	return '';
}

# テキストを指定書式に変換
sub method_cnv_text_str
{
	my ($attr_str) = @_;
	my %attr = ();
	
	parse_attr_str($attr_str, \%attr);
	
	if((exists($attr{'text'})) && (exists($attr{'format'})))
	{
		my $text_str   = '';
		my $format_str = '';
		my $return_str = '';
		
		if(get_param($attr{'text'}, \$text_str) != 0)
		{
			$text_str = delete_quote($attr{'text'});
		}
		
		if(get_param($attr{'format'}, \$format_str) != 0)
		{
			$format_str = delete_quote($attr{'format'});
		}
		
		my @text_strs = split(/\r\n/, $text_str);
		
		foreach my $text (@text_strs)
		{
			my $fmt = $format_str;
			
			$fmt =~ s/{line}/$text/;
			
			$return_str .= $fmt;
		}
		
		if(exists($attr{'var'}))
		{
			if(check_var_name($attr{'var'}) == 0)
			{
				$user_var{$attr{'var'}} = $return_str;
			}
		}
		else
		{
			return $return_str;
		}
	}
	
	return '';
}

# タグ文字列をエスケープ
sub method_cnv_tag_escape
{
	my ($attr_str) = @_;
	my %attr = ();
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'tag'}))
	{
		my $tag_str = '';
		
		if(get_param($attr{'tag'}, \$tag_str) != 0)
		{
			$tag_str = delete_quote($attr{'tag'});
		}
		
		if($tag_str ne '')
		{
			main::escape_string(\$tag_str);
			
			if(exists($attr{'var'}))
			{
				if(check_var_name($attr{'var'}) == 0)
				{
					$user_var{$attr{'var'}} = $tag_str;
				}
			}
			else
			{
				return $tag_str;
			}
		}
	}
	
	return '';
}

#### タグ一覧 ####

# 使用可能なタグ名一覧を生成する
sub method_init_tag_list
{
	my ($attr_str) = @_;
	my %attr = ();
	my $permission = '';
	my $sort       = '';
	my $ret_str    = '';
	
	@tag_name_list = ();
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'permission'}))
	{
		get_param($attr{'permission'}, \$permission, 1);
	}
	
	if(exists($attr{'sort'}))
	{
		get_param($attr{'sort'}, \$sort, 1);
	}
	
	main::get_tag_name_list($permission, \@tag_name_list, $sort);
	
	if(@tag_name_list > 0)
	{
		$next_tag_name_list = 0;
	}
	else
	{
		$next_tag_name_list = -1;
	}
	
	return '';
}

# タグ名一覧のタグ数を取得
sub method_get_tag_num
{
	my ($attr_str) = @_;
	my %attr = ();
	my $tag_list_num = @tag_name_list;
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'var'}))
	{
		if(check_var_name($attr{'var'}) == 0)
		{
			$user_var{$attr{'var'}} = $tag_list_num;
		}
	}
	else
	{
		return $tag_list_num;
	}
	
	return '';
}

# タグ名一覧からタグ名取得
sub method_fetch_tag_name
{
	my ($attr_str) = @_;
	my %attr = ();
	my $tag_name = '';
	
	parse_attr_str($attr_str, \%attr);
	
	if($next_tag_name_list >= 0)
	{
		$tag_name = $tag_name_list[$next_tag_name_list];
		
		$next_tag_name_list++;
		if($next_tag_name_list >= @tag_name_list)
		{
			$next_tag_name_list = -1;
		}
	}
	
	if(exists($attr{'var'}))
	{
		if(check_var_name($attr{'var'}) == 0)
		{
			$user_var{$attr{'var'}} = $tag_name;
		}
	}
	else
	{
		return $tag_name;
	}
	
	return '';
}

sub method_get_tag_desc
{
	my ($attr_str) = @_;
	my %attr = ();
	my $tag_name  = '';
	my $tag_param = '';
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'tag_name'}))
	{
		get_param($attr{'tag_name'}, \$tag_name, 1);
	}
	
	$tag_param = main::get_tag_param($tag_name, 'description');
	$tag_param =~ s/\[/&#091;/g;
	$tag_param =~ s/\]/&#093;/g;
	
	if(exists($attr{'var'}))
	{
		if(check_var_name($attr{'var'}) == 0)
		{
			$user_var{$attr{'var'}} = $tag_param;
		}
	}
	else
	{
		return $tag_param;
	}
	
	return '';
}

sub method_get_tag_usage
{
	my ($attr_str) = @_;
	my %attr = ();
	my $tag_name  = '';
	my $tag_param = '';
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'tag_name'}))
	{
		get_param($attr{'tag_name'}, \$tag_name, 1);
	}
	
	$tag_param = main::get_tag_param($tag_name, 'usage');
	$tag_param =~ s/\[/&#091;/g;
	$tag_param =~ s/\]/&#093;/g;
	
	if(exists($attr{'var'}))
	{
		if(check_var_name($attr{'var'}) == 0)
		{
			$user_var{$attr{'var'}} = $tag_param;
		}
	}
	else
	{
		return $tag_param;
	}
	
	return '';
}

#### ログ ####

# ログ日付一覧を生成する
sub method_init_log_date_list
{
	my ($attr_str) = @_;
	my %attr = ();
	my $sort = '';
	my $ret_str = '';
	
	@log_date_list = ();
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'sort'}))
	{
		get_param($attr{'sort'}, \$sort, 1);
	}
	
	main::get_log_date_list(\@log_date_list, $sort);
	
	if(@log_date_list > 0)
	{
		$next_log_date_list = 0;
	}
	else
	{
		$next_log_date_list = -1;
	}
	
	return '';
}

# ログ日付一覧の数を取得
sub method_get_log_date_num
{
	my ($attr_str) = @_;
	my %attr = ();
	my $log_date_list_num = @log_date_list;
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'var'}))
	{
		if(check_var_name($attr{'var'}) == 0)
		{
			$user_var{$attr{'var'}} = $log_date_list_num;
		}
	}
	else
	{
		return $log_date_list_num;
	}
	
	return '';
}

# ログ日付一覧からログ日付取得
sub method_fetch_log_date
{
	my ($attr_str) = @_;
	my %attr = ();
	my $log_date = '';
	
	parse_attr_str($attr_str, \%attr);
	
	if($next_log_date_list >= 0)
	{
		$log_date = $log_date_list[$next_log_date_list];
		
		$next_log_date_list++;
		if($next_log_date_list >= @log_date_list)
		{
			$next_log_date_list = -1;
		}
	}
	
	if(exists($attr{'var'}))
	{
		if(check_var_name($attr{'var'}) == 0)
		{
			$user_var{$attr{'var'}} = $log_date;
		}
	}
	else
	{
		return $log_date;
	}
	
	return '';
}

# ログリスト初期化
sub method_init_log_list
{
	my ($attr_str) = @_;
	my %attr    = ();
	my $date    = '';
	my $start   = 0;
	my $max_num = 0;
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'date'}))
	{
		if(get_param($attr{'date'}, \$date) != 0)
		{
			$date = delete_quote($attr{'date'});
		}
		
		delete($attr{'date'});
	}
	
	if(exists($attr{'start'}))
	{
		if(get_param($attr{'start'}, \$start) != 0)
		{
			$start = delete_quote($attr{'start'});
		}
		
		delete($attr{'start'});
	}
	
	if(exists($attr{'max_num'}))
	{
		if(get_param($attr{'max_num'}, \$max_num) != 0)
		{
			$max_num = delete_quote($attr{'max_num'});
		}
		
		delete($attr{'max_num'});
	}
	
	foreach my $type (keys(%attr))
	{
		my $param = '';
		
		if(get_param($attr{$type}, \$param) != 0)
		{
			$param = delete_quote($attr{$type});
		}
		
		$attr{$type} = $param;
	}
	
	my $ref_log_db_root_path = main::get_system_param_ref('log_db_root_path');
	
	my $result = main::init_log_db($$ref_log_db_root_path, $date);
	
	if($result == 0)
	{
		main::init_log_db_list(\%attr, $start, $max_num);
	}
	
	return '';
}

# ログリストのプッシュ
sub method_push_log_list
{
	main::push_log_db_list();
	
	return '';
}

# ログリストのポップ
sub method_pop_log_list
{
	main::pop_log_db_list();
	
	return '';
}

# ログリストのログデータ数取得
sub method_get_log_num
{
	my ($attr_str) = @_;
	my %attr = ();
	my $log_num = main::get_log_db_list_size();
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'var'}))
	{
		if(check_var_name($attr{'var'}) == 0)
		{
			$user_var{$attr{'var'}} = $log_num;
		}
	}
	else
	{
		return $log_num;
	}
	
	return '';
}

# ログIDからログ各種データ取得
sub method_get_log_data
{
	my ($attr_str) = @_;
	my %attr = ();
	
	parse_attr_str($attr_str, \%attr);
	
	if(exists($attr{'param'}))
	{
		my $index_no = -1;
		
		if(exists($attr{'id'}))
		{
			my $log_id = '';
			
			if(get_param($attr{'id'}, \$log_id) != 0)
			{
				$log_id = delete_quote($attr{'id'});
			}
			
			if($log_id ne '')
			{
				$index_no = main::get_log_index($log_id);
			}
			else
			{
				$index_no = -2;
			}
		}
		
		if($index_no >= -1)
		{
			my $param = '';
			my $data  = '';
			
			if(get_param($attr{'param'}, \$param) != 0)
			{
				$param = delete_quote($attr{'param'});
			}
			
			if($param ne '')
			{
				$data = main::get_log_data($index_no, $param);
				
				if(exists($attr{'var'}))
				{
					if(check_var_name($attr{'var'}) == 0)
					{
						$user_var{$attr{'var'}} = $data;
					}
				}
				else
				{
					return $data;
				}
			}
		}
	}
	
	return '';
}

# ログIDを返す
sub method_fetch_log_id
{
	my ($attr_str) = @_;
	my %attr = ();
	my $ref_hash = main::fetch_log_db_list();
	
	if((defined($ref_hash)) && (exists($$ref_hash{'id'})))
	{
		parse_attr_str($attr_str, \%attr);
		
		if(exists($attr{'var'}))
		{
			if(check_var_name($attr{'var'}) == 0)
			{
				$user_var{$attr{'var'}} = $$ref_hash{'id'};
			}
		}
		else
		{
			return $$ref_hash{'id'};
		}
	}
	
	return '';
}

#### サブルーチン ####

# 文字列の両端にある'、"を削除する
#  param[in] : 文字列
#  return    : 両端の'、"を削除した文字列
sub delete_quote
{
	my $str = $_[0];
	
	if(defined($str))
	{
		$str =~ s/^['"](.*)['"]$/$1/;
	}
	
	return $str;
}

# 変数名が命名規則に則っているかチェック
# ※ユーザ変数の命名規則 … 先頭に$、以降は英数字またはアンダースコア(_)
sub check_var_name
{
	if(defined($_[0]))
	{
		if($_[0] =~ m/^\$\w*/)
		{
			return 0;
		}
	}
	
	return -1;
}

# パラメータ取得
#  param[in]  : パラメータ名
#  param[out] : パラメータ格納ワークリファレンス
#  param[in]  : パラメータ取得失敗時、パラメータ名を格納ワークリファレンスに格納
#  retval  0  : 成功
#  retval -1  : 失敗（失敗時、格納リファレンスには何も代入しない）
sub get_param
{
	my ($param, $ref_ret, $set_param_name) = @_;
	my $result = -1;
	
	if(defined($param))
	{
		if(check_var_name($param) == 0)
		{
			# ユーザ変数を返す
			if(exists($user_var{$param}))
			{
				$$ref_ret = $user_var{$param};
			}
			else
			{
				# ユーザ変数が存在しない場合
				$$ref_ret = '';
			}
			
			$result = 0;
		}
		else
		{
			# システムパラメータを返す
			my $key = '';
			my $sys_param_ref;
			
			if($param =~ m/(\w+)(\{\s*(\$?\w+)\s*\})/)
			{
				$param = $1;
				$key   = $3;
			}
			
			$sys_param_ref = main::get_system_param_ref($param);
			
			if(defined($sys_param_ref))
			{
				my $ref_type = ref($sys_param_ref);
				
				if($ref_type eq 'SCALAR')
				{
					$$ref_ret = $$sys_param_ref;
					
					$result = 0;
				}
				elsif($key ne '')
				{
					if((check_var_name($key) == 0) && (exists($user_var{$key})))
					{
						$key = $user_var{$key};
					}
					
					if($key ne '')
					{
						if(($ref_type eq 'ARRAY') && ($#{$sys_param_ref} >= $key))
						{
							$$ref_ret = $$sys_param_ref[$key];
							
							$result = 0;
						}
						elsif(($ref_type eq 'HASH') && (exists($$sys_param_ref{$key})))
						{
							$$ref_ret = $$sys_param_ref{$key};
							
							$result = 0;
						}
					}
				}
				else
				{
					if($ref_type eq 'ARRAY')
					{
						$$ref_ret = @$sys_param_ref;
						
						$result = 0;
					}
					elsif($ref_type eq 'HASH')
					{
						my @hash_keys = keys(%$sys_param_ref);
						
						$$ref_ret = @hash_keys;
						
						$result = 0;
					}
				}
			}
			
			if(($result == -1) && (defined($set_param_name)) && ($set_param_name == 1))
			{
				# パラメータ取得失敗時、パラメータ名をそのまま使用する
				$$ref_ret = delete_quote($param);
				
				$result = 0;
			}
		}
	}
	
	return $result;
}

# アトリビュート文字列解析
#  param[in]  : アトリビュート文字列
#  param[out] : アトリビュート群ハッシュリファレンス
sub parse_attr_str
{
	my ($attr_str, $ref_attr) = @_;
	
	if($attr_str ne '')
	{
		# アトリビュートを取得
		my @attrs = split(/\s*;\s*/, $attr_str);
		
		foreach(@attrs)
		{
			my $attr = $_;
			
			if($attr !~ m/(!=|==|>=|<=)/)			# 比較式がある場合はキーと値に分割しない
			{
				my ($attr_key, $attr_var) = split(/\s*=\s*/, $_, 2);
				
				$attr_key = lc($attr_key);
				
				$$ref_attr{$attr_key} = $attr_var;
			}
			else
			{
				$$ref_attr{$attr} = '';
			}
		}
	}
}

1;
