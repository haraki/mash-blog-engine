######## テンプレート ########

package Mash::MashTemplate;

use strict;
#use utf8;

use constant ONE_FILE_TAG_MAX => 1000;			# 1ファイルあたりのタグ最大数

# コンストラクタ
#  return    : インスタンス
sub new
{
	my ($myclass, $def_perm) = @_;
	my %tags = ();
	
	if(!(defined($def_perm)))
	{
		$def_perm = '';
	}
	
	# メンバ
	my $member = {
		'tags'     => \%tags,			# タグ
		'def_perm' => $def_perm,		# デフォルトパーミッション
	};
	
	return bless $member, $myclass;
}

# タグ読み込み
#  param[in] : タグモジュールディレクトリ
sub load_tag
{
	my ($this, $module_dir) = @_;
	my $base_tag_no = 0;
	
	for my $file (glob("$module_dir/*.*"))
	{
		if(-e($file))
		{
			require $file;
			
			my $tag_module = $file;
			
			$tag_module =~ s/.*?(\w+?)\.pl$/$1/;
			
			my $new_tags = $tag_module->get_tags();
			
			# タグに通し番号を付ける
			my @tag_keys = keys(%$new_tags);
			foreach my $tag_key (@tag_keys)
			{
				my $tag_no = $base_tag_no;
				
				if(exists($new_tags->{$tag_key}{'no'}))
				{
					$tag_no += $new_tags->{$tag_key}{'no'};
				}
				
				$new_tags->{$tag_key}{'no'} = $tag_no;
			}
			
			%{$this->{'tags'}} = (%{$this->{'tags'}}, %$new_tags);
			
			$base_tag_no += ONE_FILE_TAG_MAX;
		}
	}
}

# タグ解析
#  param[in]     : パーミッション
#  param[in/out] : 文字列リファレンス
sub parse_tag
{
	my ($this, $permission, $ref_str) = @_;
	
	while($$ref_str =~ m/(\[\s*(\/?\w+)\s*([^\]]*)\])/isg)
	{
		my $tag_str       = $1;					# タグ文字列
		my $tag_rep       = '';					# 解析結果の文字列
		my $tag_key       = lc($2);				# タグ名
		my $tag_attr_str  = $3;					# アトリビュート文字列
		my $parse_flag    = 0;
		my $tag_start_pos = (pos $$ref_str) - length($tag_str);		# 開始タグの位置
		
		# タグを解析するか判定
		if((defined($permission)) && ($permission ne ''))
		{
			if(exists($this->{'tags'}{$tag_key}))
			{
				if(exists($this->{'tags'}{$tag_key}{'permission'}))
				{
					# タグにパーミッションが設定されている場合はパーミッション配列を検索
					foreach my $perm (@{$this->{'tags'}{$tag_key}{'permission'}})
					{
						if($permission eq $perm)
						{
							$parse_flag = 1;
							
							last;
						}
					}
				}
				else
				{
					# タグにパーミッションが設定されていない場合はデフォルトパーミッションを確認
					if($permission eq $this->{'def_perm'})
					{
						$parse_flag = 1;
					}
				}
			}
		}
		else
		{
			# パーミッション指定が無い場合は無条件でタグ解析
			$parse_flag = 1;
		}
		
		if($parse_flag)
		{
			# タグは解析対象
			$tag_attr_str =~ s/\s*$//;
			
			if((exists($this->{'tags'}{$tag_key}{'end'})) && ($this->{'tags'}{$tag_key}{'end'} != 0))
			{
				# 文字列を取得する
				my $end_type      = $this->{'tags'}{$tag_key}{'end'};
				my $param_str_pos = $tag_start_pos + length($tag_str);
				my $tag_end_str   = '';
				my $tag_end_pos   = 0;
				my $nest_count    = 0;
				
				# 検索開始位置を設定
				pos $$ref_str = $param_str_pos;
				
				if($end_type == 2)
				{
					$tag_end_pos = length($$ref_str);
				}
				else
				{
					# 終端タグを検索
					while($$ref_str =~ m/(\[\s*(\/?$tag_key)\s*[^\]]*\])/isg)
					{
						$tag_end_pos = pos $$ref_str;
						
						if($tag_end_pos < $param_str_pos)
						{
							# 検索開始位置よりも手前で見つかった場合は該当する終端タグが見つからなかったので終了
							last;
						}
						
						if($2 eq "/$tag_key")
						{
							if($nest_count == 0)
							{
								# 該当する終端タグが見つかった
								$tag_end_str = $1;						# 終端タグ文字列
								$tag_end_pos -= length($tag_end_str);	# 終端タグ開始位置
								
								last;
							}
							
							$nest_count--;
						}
						else
						{
							# タグがネストしている
							$nest_count++;
						}
					}
				}
				
				if(($end_type == 2) || ($tag_end_str ne ''))
				{
					my $tag_bw_str     = substr($$ref_str, $param_str_pos, $tag_end_pos - $param_str_pos);			# 開始タグ～終端タグ間の文字列を抜き出す
					my $tag_bw_str_bak = $tag_bw_str;
					
					# 文字列先頭の改行コードを削除
					$tag_bw_str =~ s/^\r//;
					$tag_bw_str =~ s/^\n//;
					
					$tag_rep = $this->{'tags'}{$tag_key}{'method'}($tag_attr_str, $tag_bw_str);		# タグに対応したメソッドを呼び出し、文字列を取得
					
					$tag_str = $tag_str . $tag_bw_str_bak . $tag_end_str;
				}
				else
				{
					# タグをスキップ
					$tag_rep = $tag_str;
					$tag_start_pos += length($tag_rep);
				}
			}
			else
			{
				$tag_rep = $this->{'tags'}{$tag_key}{'method'}($tag_attr_str, '');		# タグに対応したメソッドを呼び出し、文字列を取得
			}
		}
		else
		{
			# タグをスキップ
			$tag_rep = $tag_str;
			$tag_start_pos += length($tag_rep);
		}
		
		# メタ文字を正規表現として適用されないように頭に\を付ける
		$tag_str =~ s/([\.\*\+\?\^\$\\\|\(\)\[\]\{\}])/\\$1/g;
		
		$$ref_str =~ s/$tag_str/$tag_rep/;
		
		pos $$ref_str = $tag_start_pos;
	}
}

# タグ名リスト取得
#  param[in]     : パーミッション
#  param[in/out] : リストリファレンス
#  param[in]     : ソート('char', 'rchar', 'no', 'rno' or 'none'(default))
sub get_tag_name_list
{
	my ($this, $permission, $ref_tag_list, $sort) = @_;
	
	if(!(defined($ref_tag_list)))
	{
		return;
	}
	
	if(!(defined($permission)))
	{
		$permission = '';
	}
	
	my @tag_keys = keys(%{$this->{'tags'}});
	
	if((defined($permission)) && ($permission ne ''))
	{
		foreach my $tag_key (@tag_keys)
		{
			if(exists($this->{'tags'}{$tag_key}))
			{
				if(exists($this->{'tags'}{$tag_key}{'permission'}))
				{
					# タグにパーミッションが設定されている場合はパーミッション配列を検索
					foreach my $perm (@{$this->{'tags'}{$tag_key}{'permission'}})
					{
						if($permission eq $perm)
						{
							push(@$ref_tag_list, $tag_key);
						
							last;
						}
					}
				}
				else
				{
					# タグにパーミッションが設定されていない場合はデフォルトパーミッションを確認
					if($permission eq $this->{'def_perm'})
					{
						push(@$ref_tag_list, $tag_key);
					}
				}
			}
		}
	}
	else
	{
		@$ref_tag_list = @tag_keys;
	}
	
	if(defined($sort))
	{
		if($sort eq 'char')
		{
			@$ref_tag_list = sort { $a cmp $b } @$ref_tag_list;
		}
		elsif($sort eq 'rchar')
		{
			@$ref_tag_list = sort { $b cmp $a } @$ref_tag_list;
		}
		elsif($sort eq 'no')
		{
			@$ref_tag_list = sort { $this->{'tags'}{$a}{'no'} <=> $this->{'tags'}{$b}{'no'} } @$ref_tag_list;
		}
		elsif($sort eq 'rno')
		{
			@$ref_tag_list = sort { $this->{'tags'}{$b}{'no'} <=> $this->{'tags'}{$a}{'no'} } @$ref_tag_list;
		}
	}
}

# 指定タグ名のパラメータ取得
#  param[in] : タグ名
#  param[in] : パラメータ名
#  return    : パラメータ
sub get_tag_param
{
	my ($this, $tag_name, $tag_param_name) = @_;
	
	if((!(defined($tag_name))) || (!(exists($this->{'tags'}{$tag_name}))) || (!(exists($this->{'tags'}{$tag_name}{$tag_param_name}))))
	{
		return undef;
	}
	
	return $this->{'tags'}{$tag_name}{$tag_param_name};
}

1;
