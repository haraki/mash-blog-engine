######## デバッグ ########

package Mash::MashDebug;

use strict;
#use utf8;

# コンストラクタ
#  return    : インスタンス
sub new
{
	my ($myclass) = @_;
	
	# メンバ
	my $member = {
		'log' => '',			# ログ
	};
	
	return bless $member, $myclass;
}

# ログを取得
#  return : ログ
sub get_log
{
	my ($this) = @_;
	
	return $this->{'log'};
}

# ログの出力
sub print_log
{
	my ($this)  = @_;
	my $dbg_log = $this->get_log();
	
	$dbg_log =~ s/</&lt;/g;
	$dbg_log =~ s/>/&gt;/g;
	
	print $dbg_log;
}

# ログをフラッシュ
sub flush_log
{
	my ($this) = @_;
	
	$this->{'log'} = '';
}

# 値をログに追加
#  param[in] : 値
#  param[in] : ネストレベル
sub add_log_value
{
	my ($this, $value, $nest_level) = @_;
	
	if(!(defined($nest_level)))
	{
		$nest_level = 0;
	}
	
	for(my $nest_loop = 0;$nest_loop < $nest_level;$nest_loop++)
	{
		$this->{'log'} .= "  ";
	}
	
	$this->{'log'} .= "$value";
}

# 配列の内容をログに追加
#  param[in] : 配列のリファレンス
#  param[in] : ネストレベル
sub add_log_array
{
	my ($this, $ref_array, $nest_level) = @_;
	my $element_no = 0;
	
	if(!(defined($nest_level)))
	{
		$nest_level = 0;
	}
	
	foreach my $value (@$ref_array)
	{
		my $ref_type = ref($value);
		
		for(my $nest_loop = 0;$nest_loop < $nest_level;$nest_loop++)
		{
			$this->add_log_value("  ");
		}
		
		$this->add_log_value("[$element_no] = ");
		
		if($ref_type eq 'SCALAR')
		{
			my $scalar = $$value;
			
			if(!(defined($scalar)))
			{
				$scalar = '';
			}
			
			$this->add_log_value("\"$scalar\"\r\n");
		}
		elsif($ref_type eq 'ARRAY')
		{
			$this->add_log_value("ARRAY\r\n");
			$this->add_log_array($value, $nest_level + 1);
		}
		elsif($ref_type eq 'HASH')
		{
			$this->add_log_value("HASH\r\n");
			$this->add_log_hash($value, $nest_level + 1);
		}
		else
		{
			$this->add_log_value("\"$value\"\r\n");
		}
		
		$element_no++;
	}
}

# ハッシュの内容をログに追加
#  param[in] : ハッシュのリファレンス
#  param[in] : ネストレベル
sub add_log_hash
{
	my ($this, $ref_hash, $nest_level) = @_;
	
	if(!(defined($nest_level)))
	{
		$nest_level = 0;
	}
	
	foreach my $key (keys(%$ref_hash))
	{
		my $value    = $$ref_hash{$key};
		my $ref_type = ref($value);
		
		for(my $nest_loop = 0;$nest_loop < $nest_level;$nest_loop++)
		{
			$this->add_log_value("  ");
		}
		
		$this->add_log_value("{$key} = ");
		
		if($ref_type eq 'SCALAR')
		{
			my $scalar = $$value;
			
			if(!(defined($scalar)))
			{
				$scalar = '';
			}
			
			$this->add_log_value("\"$scalar\"\r\n");
		}
		elsif($ref_type eq 'ARRAY')
		{
			$this->add_log_value("ARRAY\r\n");
			$this->add_log_array($value, $nest_level + 1);
		}
		elsif($ref_type eq 'HASH')
		{
			$this->add_log_value("HASH\r\n");
			$this->add_log_hash($value, $nest_level + 1);
		}
		else
		{
			$this->add_log_value("\"$value\"\r\n");
		}
	}
}

1;
