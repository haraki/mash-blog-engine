######## データベース ########

# \0 … データベースの終端
# \n … データベースの行の区切り
# , … データベースの1データの区切り
# <> … 1データ内の配列の要素の区切り

package Mash::MashDB;

use strict;
use utf8;
#use open ":utf8";
#use open ":std";
use Mash::MashTime;
use Mash::MashDebug;

my $db_lock_file = ".db_lock";		# データベースロック用ファイル名

use constant TYPE_BOOLEAN => 1;				# ブーリアン(true/false)
use constant TYPE_INTEGER => 2;				# 整数値
use constant TYPE_FLOAT   => 3;				# 浮動小数値
use constant TYPE_TEXT    => 4;				# 文字列
use constant TYPE_DATE    => 5;				# 日付(YYYY/MM/DD)
use constant TYPE_TIME    => 6;				# 時刻(HH:MM:SS)

use constant TYPE_ARRAY   => (1 << 6);		# 配列
use constant TYPE_PRIMARY => (1 << 7);		# 主キー
use constant TYPE_FLAG    => (TYPE_ARRAY | TYPE_PRIMARY);

# 型文字列→数値変換ハッシュ
my %format_s2n_hash =
	(
	 'boolean' => TYPE_BOOLEAN,
	 'integer' => TYPE_INTEGER,
	 'float'   => TYPE_FLOAT,
	 'text'    => TYPE_TEXT,
	 'date'    => TYPE_DATE,
	 'time'    => TYPE_TIME,
	 'array'   => TYPE_ARRAY,
	 'primary' => TYPE_PRIMARY,
	);

# コンストラクタ
#  param[in] : ファイル名
#  return    : インスタンス
sub new
{
	my ($myclass, $filename) = @_;
	
	# メンバ
	my $member =
	{
		'filename'     => $filename,		# ファイル名
		'format'       => (),				# フォーマット
		'db'           => [],				# データベース
		'primary_name' => '',				# 主キーとなる項目名
		'primary_hash' => (),				# 主キーからDB内のデータを取得するためのハッシュ
		
		'index_list'   => [],				# 条件に合致したハッシュのインデックスリスト
		'list_ref_no'  => 0,				# 次に取得するインデックスリストの参照No.
		
		'stack_index_list'  => [],			# インデックスリストのスタック
		'stack_list_ref_no' => [],			# 参照No.のスタック
	};
	
	return bless $member, $myclass;
}

# 消去
sub clean
{
	my ($this) = @_;
	
	$this->{'format'}       = ();
	$this->{'db'}           = [];
	$this->{'primary_name'} = '';
	$this->{'primary_hash'} = ();
	
	$this->{'index_list'}   = ();
	$this->{'list_ref_no'}  = 0;
	
	$this->{'stack_index_list'}  = [];
	$this->{'stack_list_ref_no'} = [];
}

# データベース読み込み
#  retval  0 : 成功
#  retval -1 : オープン失敗（ファイルが存在しない）
#  retval -2 : 読み込み失敗
#  retval -3 : フォーマット文字列に主キー重複エラー
#  retval -4 : フォーマット文字列に主キー未設定エラー
sub read
{
	my ($this) = @_;
	
	$this->clean();
	
	if(!open(FILE, $this->{'filename'}))
	{
		return -1;
	}
	
	my @db_list = <FILE>;
	
	close(FILE);
	
	if(@db_list == 0)
	{
		return -2;
	}
	
	# フォーマット文字列の解析
	my $format_str = shift(@db_list);
	
	$format_str =~ s/[\r\n]$//;
	
	my @format_list = split(/,/, $format_str);
	
	foreach my $fmt_str (@format_list)
	{
		my ($fmt_key, $fmt_value) = split(/=/, $fmt_str, 2);
		my @fmt_values = split(/<>/, $fmt_value);
		my $fmt_type_value = 0;
		
		foreach my $fmt_type (@fmt_values)
		{
			if(exists($format_s2n_hash{$fmt_type}))
			{
				$fmt_type_value |= $format_s2n_hash{$fmt_type};
				
				if($fmt_type_value & TYPE_PRIMARY)
				{
					# 既に主キーが存在していないかチェック
					if($this->{'primary_name'} eq '')
					{
						$this->{'primary_name'} = $fmt_key;
					}
					else
					{
						# 主キーは2つ以上存在できないのでエラー
						return -3;
					}
				}
			}
		}
		
		$this->{'format'}{$fmt_key} = $fmt_type_value;
	}
	
	if($this->{'primary_name'} eq '')
	{
		# 主キーは存在していないのでエラー
		$this->clean();
		
		return -4;
	}
	
	my $primary_name = $this->{'primary_name'};
	
	# データ文字列の解析
	foreach my $db_str (@db_list)
	{
		$db_str =~ s/[\r\n]$//;
		
		my @data_list = split(/,/, $db_str);
		my %db_hash = ();
		
		foreach my $data_str (@data_list)
		{
			my ($data_key, $data_value) = split(/=/, $data_str, 2);
			my $data_type = 0;
			
			if(!exists($this->{'format'}{$data_key}))
			{
				$this->{'format'}{$data_key} = TYPE_TEXT;
			}
			
			$data_type = $this->{'format'}{$data_key};
			
			if($data_type & TYPE_ARRAY)
			{
				my @data_values = split(/<>/, $data_value);
				
				for(my $index = 0;$index < @data_values;$index++)
				{
					$data_values[$index] =~ s/&lt;&gt;/<>/g;
					$data_values[$index] =~ s/&#044;/,/g;
					$data_values[$index] =~ s/&#10;/\r\n/g;
				}
				
				$db_hash{$data_key} = \@data_values;
			}
			else
			{
				$data_value =~ s/&lt;&gt;/<>/g;
				$data_value =~ s/&#044;/,/g;
				$data_value =~ s/&#10;/\r\n/g;
				
				$db_hash{$data_key} = $data_value;
			}
		}
		
		if(!exists($db_hash{$primary_name}))
		{
			# 主キーが存在していないのでエラー
			$this->clean();
			
			return -5;
		}
		else
		{
			my $pri_data = $db_hash{$primary_name};
			
			if(exists($this->{'primary_hash'}{$pri_data}))
			{
				# 主キーが重複しているのでエラー
				$this->clean();
				
				return -6;
			}
			else
			{
				push(@{$this->{'db'}}, \%db_hash);
				
				$this->{'primary_hash'}{$pri_data} = $#{$this->{'db'}};
			}
		}
	}
	
	$this->init_db_list();
	
	return 0;
}

# データベース書き込み
#  retval  0 : 成功
#  retval -1 : オープン失敗
#  retval -2 : 書き込み失敗
sub write
{
	my ($this) = @_;
	
	if(!open(FILE, ">$this->{'filename'}"))
	{
		return -1;
	}
	
	# フォーマットの出力
	my @fmt_keys = sort(keys(%{$this->{'format'}}));
	foreach my $fmt_key (@fmt_keys)
	{
		my $fmt_type_str = _get_format_str($this->{'format'}{$fmt_key});
		
		print(FILE "$fmt_key=$fmt_type_str,");
	}
	
	print(FILE "\n");
	
	# データの出力
	foreach my $ref_db_hash (@{$this->{'db'}})
	{
		foreach my $data_key (@fmt_keys)
		{
			my $data_str = _get_data_str($$ref_db_hash{$data_key});
			
			print(FILE "$data_key=$data_str,");
		}
		
		print(FILE "\n");
	}
	
	close(FILE);
	
	if(!-e($this->{'filename'}))
	{
		return -2;
	}
	
	return 0;
}

# データベース生成
#  param[in] : 型情報ハッシュリファレンス
#  retval  0 : 成功
#  retval -1 : オープン失敗
#  retval -2 : 書き込み失敗
#  retval -3 : 型情報異常
sub create
{
	my ($this, $ref_hash) = @_;
	
	$this->{'db'}     = ();
	$this->{'format'} = ();
	
	foreach my $fmt_key (keys(%$ref_hash))
	{
		my @fmt_values = split(/,/, $$ref_hash{$fmt_key});
		my $fmt_type_value = 0;
		
		foreach my $fmt_type (@fmt_values)
		{
			if(exists($format_s2n_hash{$fmt_type}))
			{
				$fmt_type_value |= $format_s2n_hash{$fmt_type};
			}
			else
			{
				return -3;
			}
		}
		
		$this->{'format'}{$fmt_key} = $fmt_type_value;
	}
	
	return $this->write();
}

# データベースに項目追加
#  param[in] : 項目名
#  param[in] : 型情報文字列
#  retval  0 : 成功
#  retval -3 : 型情報異常
#  retval -4 : 項目は既に存在している
sub add_column
{
	my ($this, $column_name, $format) = @_;
	
	my $temp_format;
	if($this->get_column_info($column_name, $temp_format) == 0)
	{
		return -4;
	}
	
	my @fmt_values = split(/,/, $format);
	my $fmt_type_value = 0;
	
	foreach my $fmt_type (@fmt_values)
	{
		if(exists($format_s2n_hash{$fmt_type}))
		{
			$fmt_type_value |= $format_s2n_hash{$fmt_type};
		}
		else
		{
			return -3;
		}
	}
	
	$this->{'format'}{$column_name} = $fmt_type_value;
}

# データベースの指定項目の情報取得
#  param[in]  : 項目名
#  param[out] : 型情報文字列格納先リファレンス
#  retval  0  : 成功
#  retval -5  : 指定項目は存在しない
sub get_column_info
{
	my ($this, $column_name, $ref_format) = @_;
	
	if(!exists($this->{'format'}{$column_name}))
	{
		return -5;
	}
	
	my $fmt_type_value = $this->{'format'}{$column_name};
	
	$$ref_format = _get_format_str($fmt_type_value);
	
	return 0;
}

# ハッシュ取得
#  param[in] : インデックス番号
#  return    : ハッシュリファレンス
sub get_hash
{
	my ($this, $index_no) = @_;
	
	if((defined($index_no)) && ($index_no ne '') && ($index_no >= 0) && ($index_no < $this->get_hash_num()))
	{
		return $this->{'db'}[$index_no];
	}
	
	return undef;
}

# データ取得
#  param[in] : インデックス番号
#  param[in] : パラメータ名
#  param[in] : 要素番号（パラメータが配列の場合のみ）
#  return    : 値／要素数（パラメータが配列且つ要素番号指定なし）
sub get_data
{
	my ($this, $index_no, $param, $element_no) = @_;
	my $ref_hash = $this->get_hash($index_no);
	
	if(defined($ref_hash))
	{
		if((!defined($param)) || ($param eq ''))
		{
			return $ref_hash;
		}
		
		if(exists($$ref_hash{$param}))
		{
			my $value    = $$ref_hash{$param};
			my $ref_type = ref($value);
			
			if($ref_type eq 'ARRAY')
			{
				my $array_size = @{$value};
				
				if((!defined($element_no)) || ($element_no eq ''))
				{
					return $array_size;
				}
				elsif($element_no < $array_size)
				{
					return ${$value}[$element_no];
				}
			}
			else
			{
				return $value;
			}
		}
	}
	
	return undef;
}

# ハッシュ数取得
#  return    : ハッシュ数
sub get_hash_num
{
	my ($this) = @_;
	my $num    = 0;
	
	if((defined($this->{'db'})) && (ref($this->{'db'}) eq 'ARRAY'))
	{
		$num = @{$this->{'db'}};
	}
	
	return $num;
}

# ハッシュ登録
#  param[in] : ハッシュリファレンス
#  return    : DB内のインデックス番号（エラーの場合は-1）
sub regist_hash
{
	my ($this, $ref_hash) = @_;
	my $index_no;
	
	$index_no = $this->insert_hash($ref_hash);
	
	if($index_no < 0)
	{
		$index_no = $this->update_hash($ref_hash);
		
		if($index_no < 0)
		{
			return -1;
		}
	}
	
	return $index_no;
}

# ハッシュ更新
#  param[in] : ハッシュリファレンス
#  return    : DB内のインデックス番号（エラーの場合は-1）
sub update_hash
{
	my ($this, $ref_hash) = @_;
	
	if((defined($ref_hash)) && (ref($ref_hash) eq 'HASH') && (exists($$ref_hash{$this->{'primary_name'}})))
	{
		my $index_no = $this->_get_index($$ref_hash{$this->{'primary_name'}});
		
		if($index_no >= 0)
		{
			my $ref_regist_hash = $this->make_new_hash($ref_hash, $this->{'db'}[$index_no]);
			
			$this->{'db'}[$index_no] = $ref_regist_hash;
			
			return $index_no;
		}
	}
	
	return -1;
}

# ハッシュ挿入
#  param[in] : ハッシュリファレンス
#  return    : DB内のインデックス番号（エラーの場合は-1）
sub insert_hash
{
	my ($this, $ref_hash) = @_;
	
	if((defined($ref_hash)) && (ref($ref_hash) eq 'HASH') && (exists($$ref_hash{$this->{'primary_name'}})))
	{
		my $index_no = $this->_get_index($$ref_hash{$this->{'primary_name'}});
		
		if($index_no == -1)
		{
			my $ref_regist_hash = $this->make_new_hash($ref_hash);
			
			$index_no = $this->get_hash_num();
			
			splice(@{$this->{'db'}}, $index_no, 0, $ref_regist_hash);
			
			return $index_no;
		}
	}
	
	return -1;
}

# DB登録用の新規ハッシュ生成
#  param[in] : ハッシュリファレンス
#  param[in] : 元ハッシュリファレンス
#  return    : 登録用ハッシュリファレンス
sub make_new_hash
{
	my ($this, $ref_hash, $ref_old_hash) = @_;
	my @key_list = keys(%{$this->{'format'}});
	my %regist_hash = ();
	
	foreach my $key (@key_list)
	{
		if(exists($$ref_hash{$key}))
		{
			# ハッシュに登録されている値をそのまま適用する
			$regist_hash{$key} = $$ref_hash{$key};
		}
		elsif((defined($ref_old_hash)) && (exists($$ref_old_hash{$key})))
		{
			# 元ハッシュの値を適用
			$regist_hash{$key} = $$ref_old_hash{$key};
		}
		else
		{
			# デフォルト値を適用する
			if($this->{'format'}{$key} & TYPE_ARRAY)
			{
				my @dummy_array = ();
				
				$regist_hash{$key} = \@dummy_array;
			}
			else
			{
				$regist_hash{$key} = '';
			}
		}
	}
	
	return \%regist_hash;
}

# ハッシュ削除
#  param[in] : 主キー
#  return    : DB内のインデックス番号（エラーの場合は-1）
sub delete_hash
{
	my ($this, $primary_key) = @_;
	
	if((defined($primary_key)) && ($primary_key ne ''))
	{
		my $index_no = $this->_get_index($primary_key);
		
		if($index_no >= 0)
		{
			splice(@{$this->{'db'}}, $index_no, 1);
			
			return $index_no;
		}
	}
	
	return -1;
}

# 主キーに合致するインデックス番号を返す（内部関数）
#  param[in] : 主キー
#  return    : インデックス番号（存在しない場合は-1）
sub _get_index
{
	my ($this, $primary_key) = @_;
	
	for(my $index_no = 0;$index_no < @{$this->{'db'}};$index_no++)
	{
		my $ref_hash = $this->{'db'}[$index_no];
		
		if($$ref_hash{$this->{'primary_name'}} eq $primary_key)
		{
			return $index_no;
		}
	}
	
	return -1;
}

# データベースリストを指定条件で初期化
#  param[in] : 指定条件ルーチンリファレンス
#  param[in] : 指定条件アトリビュートリファレンス
#  param[in] : 指定条件ルーチン用オプション
#  param[in] : ソート比較用ルーチンリファレンス
#  param[in] : ソート昇順(0) / 降順(0以外)
#  param[in] : 行オフセット
#  param[in] : 行最大数（0の場合は制限無し）
sub init_db_list
{
	my ($this, $ref_cond, $ref_attr, $ref_cond_option, $ref_sort, $order, $start, $max_num) = @_;
	my $hash_num = $this->get_hash_num();
	
	@{$this->{'index_list'}} = ();
	$this->{'list_ref_no'}   = 0;
	
	for(my $index_no = 0;$index_no < $hash_num;$index_no++)
	{
		my $push_flag = 1;
		
		if(defined($ref_cond))
		{
			my $ref_hash = $this->get_hash($index_no);
			
			if(!(&$ref_cond($ref_hash, $ref_attr, $ref_cond_option)))
			{
				$push_flag = 0;
			}
		}
		
		if($push_flag)
		{
			push(@{$this->{'index_list'}}, $index_no);
		}
	}
	
	if((defined($ref_sort)) && (defined($order)))
	{
		if($order == 0)
		{
			@{$this->{'index_list'}} = sort { &$ref_sort($this->get_hash($a), $this->get_hash($b)); } @{$this->{'index_list'}};
		}
		else
		{
			@{$this->{'index_list'}} = sort { &$ref_sort($this->get_hash($b), $this->get_hash($a)); } @{$this->{'index_list'}};
		}
	}
	
	if((defined($start)) && ($start > 0))
	{
		splice(@{$this->{'index_list'}}, 0, $start);
	}
	
	if((defined($max_num)) && ($max_num > 0) && ($max_num < @{$this->{'index_list'}}))
	{
		splice(@{$this->{'index_list'}}, $max_num - @{$this->{'index_list'}});
	}
}

# データベースリスト及びリストの参照No.をスタックにプッシュ
sub push_db_list
{
	my ($this) = @_;
	
	push(@{$this->{'stack_index_list'}}, [@{$this->{'index_list'}}]);
	push(@{$this->{'stack_list_ref_no'}}, $this->{'list_ref_no'});
}

# データベースリストリスト及びリストの参照No.をスタックからポップ
sub pop_db_list
{
	my ($this) = @_;
	
	@{$this->{'index_list'}} = @{pop(@{$this->{'stack_index_list'}})};
	$this->{'list_ref_no'}   = pop(@{$this->{'stack_list_ref_no'}});
}

# データベースリストからフェッチ
#  return    : ハッシュリファレンス(全てフェッチ済の場合はundef)
sub fetch_db_list_hashref
{
	my ($this) = @_;
	
	if(($this->{'list_ref_no'} >= 0) && ($this->{'list_ref_no'} < @{$this->{'index_list'}}))
	{
		my $index_no = ${$this->{'index_list'}}[$this->{'list_ref_no'}];
		
		$this->{'list_ref_no'}++;
		
		return ${$this->{'db'}}[$index_no];
	}
	
	return undef;
}

# データベースリストリストのサイズを取得
#  return    : データベースリストリストサイズ
sub get_db_list_size
{
	my ($this) = @_;
	my $ret = @{$this->{'index_list'}};
	
	return $ret;
}

# データベースリスト内の、指定インデックスNo.からオフセット値分離れた位置にあるインデックスNo.を返す
#  param[in] : インデックスNo.
#  param[in] : オフセット
#  return    : 該当するインデックスNo.(該当するインデックスNo.が無い場合は-1を返す)
sub search_db_list
{
	my ($this, $index_no, $offset) = @_;
	my $list_size    = $this->get_db_list_size();
	my $ret_index_no = -1;
	
	if(!(defined($offset)))
	{
		$offset = 0;
	}
	
	for(my $list_no = 0;$list_no < $list_size;$list_no++)
	{
		if(($index_no == $this->{'index_list'}[$list_no]) && (($list_no + $offset) >= 0) && (($list_no + $offset) < $list_size))
		{
			$ret_index_no = $this->{'index_list'}[$list_no + $offset];
			
			last;
		}
	}
	
	return $ret_index_no;
}

# データベースの内容をデバッグ用ログに追加
#  param[in] : デバッグインスタンスのリファレンス
sub add_log
{
	my ($this, $ref_dbg) = @_;
	
	if(defined($ref_dbg))
	{
		my $ref_hash = $this->get_first_hash();
		
		while(defined($ref_hash))
		{
			$$ref_dbg->add_log_hash($ref_hash, 1);
			
			$ref_hash = $this->get_next_hash();
		}
	}
}

# フォーマット種別値からフォーマット文字列取得
#  param[in] : フォーマット種別値
#  return    : フォーマット文字列
sub _get_format_str
{
	my ($fmt_type)     = @_;
	my $fmt_type_str   = '';
	my $fmt_type_value = $fmt_type & ~(TYPE_FLAG);
	
	foreach my $type (keys(%format_s2n_hash))
	{
		if($format_s2n_hash{$type} eq $fmt_type_value)
		{
			$fmt_type_str = $type;
			
			last;
		}
	}
	
	if($fmt_type & TYPE_ARRAY)
	{
		$fmt_type_str .= '<>array';
	}
	elsif($fmt_type & TYPE_PRIMARY)
	{
		$fmt_type_str .= '<>primary';
	}
	
	return $fmt_type_str;
}

# データからデータ文字列取得
#  param[in] : データ
#  return    : データ文字列
sub _get_data_str
{
	my ($data_value) = @_;
	my $data_str     = '';
	
	if(ref($data_value) eq 'ARRAY')
	{
		foreach(@$data_value)
		{
			my $value = $_;
			
			$value =~ s/<>/&lt;&gt;/g;
			
			$data_str .= "$value<>";
		}
	}
	else
	{
		$data_str = $data_value;
		
		$data_str =~ s/<>/&lt;&gt;/g;
	}
	
	$data_str =~ s/,/&#044;/g;
	$data_str =~ s/\r\n/&#10;/g;
	$data_str =~ s/\r/&#10;/g;
	$data_str =~ s/\n/&#10;/g;
	
	return $data_str;
}

# データベースのロック
#  retval  0 : 成功
#  retval -1 : 失敗
sub lock_db
{
	if(open(DB_LOCK, ">$db_lock_file"))
	{
		eval{ flock(DB_LOCK, 2); };
		
		if($@)
		{
			# flock が使用できないシステム
		}
		else
		{
			my $date_str  = '';
			my $time_str  = '';
			
			Mash::MashTime::get_current_date_time(\$date_str, \$time_str);
			
			print(DB_LOCK "lock:" . $date_str . ' ' . $time_str . "\n");
			
			return 0;
		}
	}
	
	return -1;
}

# データベースのロック解除
#  retval  0 : 成功
#  retval -1 : 失敗
sub unlock_db
{
	my $date_str  = '';
	my $time_str  = '';
	
	Mash::MashTime::get_current_date_time(\$date_str, \$time_str);
	
	print(DB_LOCK "unlock:" . $date_str . ' ' . $time_str . "\n");
	
	close(DB_LOCK);
	
	return 0;
}

1;
