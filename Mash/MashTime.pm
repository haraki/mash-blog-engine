######## 日時 ########

package Mash::MashTime;

use strict;
#use utf8;

# 現在の日時を文字列で取得
#  param[in] : 年月日文字列リファレンス
#  param[in] : 時刻文字列リファレンス
sub get_current_date_time
{
	my ($ref_date_str, $ref_time_str) = @_;
	my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime();
	
	if((defined($ref_date_str)) && (ref($ref_date_str) eq 'SCALAR'))
	{
		$$ref_date_str = sprintf("%.4d-%.2d-%.2d", $year + 1900, $mon + 1, $mday);
	}
	
	if((defined($ref_time_str)) && (ref($ref_time_str) eq 'SCALAR'))
	{
		$$ref_time_str = sprintf("%.2d:%.2d:%.2d", $hour, $min, $sec);
	}
}

# 年月日に指定した年数・月数・日数を加算する（紀元前は考慮しない）
#  param[in] : 年
#  param[in] : 月
#  param[in] : 日
#  param[in] : 加算年数
#  param[in] : 加算月数
#  param[in] : 加算日数
#  return    : [0] … 年
#  return    : [1] … 月
#  return    : [2] … 日
sub add_date
{
	my ($year, $month, $day, $add_year, $add_month, $add_day) = @_;
	
	$year += $add_year;
	
	$month += $add_month;
	
	if($month <= 0)
	{
		do
		{
			$month += 12;
			
			$year--;
		}
		while($month <= 0);
	}
	elsif($month > 12)
	{
		do
		{
			$month -= 12;
			
			$year++;
		}
		while($month > 12);
	}
	
	$day += $add_day;
	
	if($day <= 0)
	{
		do
		{
			$month--;
			if($month <= 0)
			{
				$month += 12;
				
				$year--;
			}
			
			$day += get_day_max($year, $month);
		}
		while($day <= 0);
	}
	else
	{
		my $day_max = get_day_max($year, $month);
		
		if($day > $day_max)
		{
			do
			{
				$day -= $day_max;
				
				$month++;
				if($month > 12)
				{
					$month -= 12;
					
					$year++;
				}
				
				$day_max = get_day_max($year, $month);
			}
			while($day > $day_max);
		}
	}
	
	return ($year, $month, $day);
}

# 指定年月の日の最大値を取得
#  param[in] : 年
#  param[in] : 月
#  return    : 日の最大値
sub get_day_max
{
	my ($year, $month) = @_;
	
	if((defined($year))  && ($year > 0)   &&
	   (defined($month)) && ($month >= 1) && ($month <= 12))
	{
		my @day_table  = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
		
		if(($month == 2) && ((($year % 400) == 0) || ((($year % 4) == 0) && (($year % 100) != 0))))
		{
			# うるう年の2月の場合
			return 29;
		}
		
		return $day_table[$month - 1];
	}
	
	return 0;
}

# 指定年月日の曜日を取得
#  param[in] : 年
#  param[in] : 月
#  param[in] : 日
#  return    : 曜日(0～6)
sub get_wday
{
	my ($year, $month, $day) = @_;
	
	if($month <= 2)
	{
		$year--;
		$month += 12;
	}
	
	return ($year + int($year / 4) - int($year / 100) + int($year / 400) + int((13 * $month + 8) / 5) + $day) % 7;
}

1;
