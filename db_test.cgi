﻿#!/usr/bin/perl -w

###############################################################################
#
#  db_test.cgi
#
#    author : Masashi Haraki
#
###############################################################################

######## 設定・モジュール ########

use strict;

use CGI;
use Mash::MashDB;

######## 定数・変数 ########

my $db_file;
my $db;

######## メイン ######## 

$db_file = $ARGV[0];

if((!defined($db_file)) || ($db_file eq ''))
{
	print "db_test [db_file]\n";
	
	exit(1);
}

$db = new Mash::MashDB($db_file);

print "db_file \"$db_file\" read\n";

if($db->read() != 0)
{
	print "db read error!! : $db_file\n";
	
	exit(-1);
}

$db->search('text', 'あああ');



# フォーマット
my $ref_db_fmt = $db->{'format'};
my @db_keys    = sort(keys(%$ref_db_fmt));

print "format : \n";
foreach (@db_keys)
{
	my $fmt_str = Mash::MashDB::get_format_str($$ref_db_fmt{$_});
	
	print "\t$_ = $fmt_str\n";
}

# データ
data_print();

# 主キー

foreach my $hash_key (sort(keys(%{$db->{'primary_hash'}})))
{
	print "$db->{'primary_name'} : $hash_key = $db->{'primary_hash'}{$hash_key}\n";
}

# DB更新処理
if($db_file eq 'test_db.cgi')
{
	my %ins_hash = ('id' => 0, 'text' => "テストinsert");
	my %upd_hash = ('id' => 0, 'text' => "テストupdate");
	my $id       = $db->get_hash_num() + 1;
	my $index_no;
	
	$ins_hash{'id'} = $id;
	$upd_hash{'id'} = $id;
	
	print "\n\n";
	print "db insert.\n\n";
	
	$index_no = $db->insert_hash(\%ins_hash);
	if($index_no < 0)
	{
		print "db insert error!!\n";
		
		exit(-1);
	}
	
	data_print();
	
	print "\n\n";
	print "db update.\n\n";
	
	$index_no = $db->update_hash(\%upd_hash);
	if($index_no < 0)
	{
		print "db update error!!\n";
		
		exit(-1);
	}
	
	data_print();
	
	print "\n\n";
	print "db delete.\n\n";
	
	$index_no = $db->delete_hash($id);
	if($index_no < 0)
	{
		print "db delete error!!\n";
		
		exit(-1);
	}
	
	data_print();
	
	print "\n\n";
	print "db regist(insert).\n\n";
	
	$index_no = $db->regist_hash(\%ins_hash);
	if($index_no < 0)
	{
		print "db regist(insert) error!!\n";
		
		exit(-1);
	}
	
	data_print();
	
	print "\n\n";
	print "db regist(update).\n\n";
	
	$index_no = $db->regist_hash(\%upd_hash);
	if($index_no < 0)
	{
		print "db regist(update) error!!\n";
		
		exit(-1);
	}
	
	data_print();
	
	print "\n\n";
	print "db delete.\n\n";
	
	$index_no = $db->delete_hash($id);
	if($index_no < 0)
	{
		print "db delete error!!\n";
		
		exit(-1);
	}
	
	data_print();
	
	print "\n\n";
}

exit;


sub data_print
{
	my $hash_num = $db->get_hash_num();
	
	print "db hash num = $hash_num" . "\n";
	
	for(my $index_no = 0;$index_no < $hash_num;$index_no++)
	{
		my $ref_db_hash = $db->get_hash($index_no);
		
		print "[$index_no] : ";
		
		foreach my $data_key (@db_keys)
		{
			if(defined($$ref_db_hash{$data_key}))
			{
				my $data_str = Mash::MashDB::get_data_str($$ref_db_hash{$data_key});
				
				print "$data_str<>";
			}
			else
			{
				print "\"UNDEF\"<>";
			}
		}
		
		print "\n";
	}
}
