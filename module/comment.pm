#### コメント ####

use strict;

use Mash::MashDB;
use database;

use constant ERROR_FLAG_COMMENT_ID        => (1 <<  0);
use constant ERROR_FLAG_COMMENT_NO        => (1 <<  1);
use constant ERROR_FLAG_COMMENT_ENTRY_ID  => (1 <<  2);
use constant ERROR_FLAG_COMMENT_USER_ID   => (1 <<  3);
use constant ERROR_FLAG_COMMENT_NAME      => (1 <<  4);
use constant ERROR_FLAG_COMMENT_TITLE     => (1 <<  5);
use constant ERROR_FLAG_COMMENT_TEXT      => (1 <<  6);
use constant ERROR_FLAG_COMMENT_MAIL      => (1 <<  7);
use constant ERROR_FLAG_COMMENT_URL       => (1 <<  8);
use constant ERROR_FLAG_COMMENT_STATUS    => (1 <<  9);
use constant ERROR_FLAG_COMMENT_DB_REGIST => (1 << 10);
use constant ERROR_FLAG_COMMENT_DB_SAVE   => (1 << 11);

my $comment_db_file = 'comment_db.cgi';
my $comment_db;

my %comment_db_format =
	(
	 'id'          => 'integer,primary',
	 'no'          => 'integer',
	 'parent_id'   => 'integer',
	 'entry_id'    => 'integer',
	 'user_id'     => 'text',
	 'date'        => 'date',
	 'time'        => 'time',
	 'name'        => 'text',
	 'title'       => 'text',
	 'text'        => 'text',
	 'mail'        => 'text',
	 'url'         => 'text',
	 'status'      => 'text',
	 'update_time' => 'integer',
	);

#
# コメントハッシュフォーマット
#
#    {'id'}          コメントID
#    {'no'}          コメント��
#    {'parent_id'}   親コメントID
#    {'entry_id'}    エントリID
#    {'user_id'}     ユーザーID
#    {'date'}        投稿年月日
#    {'time'}        投稿時刻
#    {'name'}        投稿者名
#    {'title'}       タイトル
#    {'text'}        本文
#    {'mail'}        メールアドレス
#    {'url'}         URL
#    {'status'}      公開状態       ('public', 'approval', 'private' or 'delete')
#    {'update_time'} 変更時刻
#

my %current_comment_data =
	(
	 'id'          => -1,
	 'no'          => -1,
	 'parent_id'   => -1,
	 'entry_id'    => -1,
	 'user_id'     => '',
	 'date'        => '',
	 'time'        => '',
	 'name'        => '',
	 'title'       => '',
	 'text'        => '',
	 'mail'        => '',
	 'url'         => '',
	 'status'      => '',
	 'update_time' => 0,
	);

# コメントDB初期化
#  param[in] : DBルートディレクトリパス
#  return    : 結果
sub init_comment_db
{
	my $db_root_path = $_[0];
	my $result = 0;
	
	$comment_db = new Mash::MashDB($db_root_path . $comment_db_file);
	
	if(read_db($comment_db, \%comment_db_format) != 0)
	{
		$result = -1;
	}
	
	return $result;
}

# カレントコメントデータのセット
#  param[in] : コメントID
#  param[in] : コメント��
#  param[in] : 親コメントID
#  param[in] : エントリID
#  param[in] : ユーザーID
#  param[in] : 投稿年月日
#  param[in] : 投稿時刻
#  param[in] : 投稿者名
#  param[in] : タイトル
#  param[in] : 本文
#  param[in] : メールアドレス
#  param[in] : URL
#  param[in] : 公開状態       ('public', 'approval', 'private' or 'delete')
#  param[in] : 変更時刻
sub set_current_comment_data
{
	($current_comment_data{'id'},
	 $current_comment_data{'no'},
	 $current_comment_data{'parent_id'},
	 $current_comment_data{'entry_id'},
	 $current_comment_data{'user_id'},
	 $current_comment_data{'date'},
	 $current_comment_data{'time'},
	 $current_comment_data{'name'},
	 $current_comment_data{'title'},
	 $current_comment_data{'text'},
	 $current_comment_data{'mail'},
	 $current_comment_data{'url'},
	 $current_comment_data{'status'},
	 $current_comment_data{'update_time'}) = @_;
	
	escape_string_hash(\%current_comment_data);
}

# ハッシュからのカレントコメントデータのセット
#  param[in] : ハッシュ
sub set_current_comment_hash
{
	my %comment_hash = @_;
	
	foreach my $key (keys(%current_comment_data))
	{
		if(exists($comment_hash{$key}))
		{
			$current_comment_data{$key} = $comment_hash{$key};
		}
	}
	
	escape_string_hash(\%current_comment_data);
}

# コメントハッシュチェック
#  return    : 結果
#                bit  0 … コメントID
#                bit  1 … コメント番号
#                bit  2 … エントリID
#                bit  3 … ユーザーID
#                bit  4 … 名前
#                bit  5 … タイトル
#                bit  6 … 本文
#                bit  7 … メールアドレス
#                bit  8 … URL
sub check_comment_data
{
	my $result = 0;
	
	if(($current_comment_data{'id'} != -1) && (get_comment_index($current_comment_data{'id'}) == -1))
	{
		# コメントIDエラー
		$result |= ERROR_FLAG_COMMENT_ID;
	}
	
	my %attr = ('entry_id' => $current_comment_data{'entry_id'});
	init_comment_db_list(\%attr, 0, 0);
	
	if(($current_comment_data{'no'} != -1) && ($comment_db->get_db_list_size() < $current_comment_data{'no'}))
	{
		# コメント番号エラー
		$result |= ERROR_FLAG_COMMENT_NO;
	}
	
	if(!is_entry_exist($current_comment_data{'entry_id'}))
	{
		# エントリIDエラー
		$result |= ERROR_FLAG_COMMENT_ENTRY_ID;
	}
	
	if(($current_comment_data{'user_id'} ne '') && (get_account_index($current_comment_data{'user_id'}) == -1))
	{
		# ユーザーIDエラー
		$result |= ERROR_FLAG_COMMENT_USER_ID;
	}
	
	if(check_form_str($current_comment_data{'name'}, ${get_system_param_ref('comment_name_min')}, ${get_system_param_ref('comment_name_max')}) != 0)
	{
		# 名前エラー
		$result |= ERROR_FLAG_COMMENT_NAME;
	}
	
	if(check_mail_str($current_comment_data{'mail'}, ${get_system_param_ref('comment_mail_min')}, ${get_system_param_ref('comment_mail_max')}) != 0)
	{
		# メールアドレスエラー
		$result |= ERROR_FLAG_COMMENT_MAIL;
	}
	
	if(check_url_str($current_comment_data{'url'}, ${get_system_param_ref('comment_url_min')}, ${get_system_param_ref('comment_url_max')}) != 0)
	{
		# URLエラー
		$result |= ERROR_FLAG_COMMENT_URL;
	}
	
	if(check_form_str($current_comment_data{'title'}, ${get_system_param_ref('comment_title_min')}, ${get_system_param_ref('comment_title_max')}) != 0)
	{
		# タイトルエラー
		$result |= ERROR_FLAG_COMMENT_TITLE;
	}
	
	if(check_form_str($current_comment_data{'text'}, ${get_system_param_ref('comment_text_min')}, ${get_system_param_ref('comment_text_max')}) != 0)
	{
		# 本文エラー
		$result |= ERROR_FLAG_COMMENT_TEXT;
	}
	
	return $result;
}

# コメント登録
#  param[out] : 登録されたコメントのID
#  return     : 結果
#                 bit  9 … 登録エラー
#                 bit 10 … 保存エラー
sub regist_comment_data
{
	my $comment_id_ref = $_[0];
	my $result = 0;
	
	if($current_comment_data{'id'} == -1)
	{
		$current_comment_data{'id'} = get_new_comment_id();
		
		my %attr = ('entry_id' => $current_comment_data{'entry_id'});
		init_comment_db_list(\%attr, 0, 0);
		
		$current_comment_data{'no'} = $comment_db->get_db_list_size() + 1;
	}
	
	my $entry_comment_type = get_entry_data($current_comment_data{'entry_id'}, 'comment');
	
	if($entry_comment_type eq 'enable')
	{
		$current_comment_data{'status'} = 'public';
	}
	else
	{
		$current_comment_data{'status'} = 'approval';
	}
	
	if($comment_db->regist_hash(\%current_comment_data) >= 0)
	{
		if($comment_db->write() != 0)
		{
			# DB保存エラー
			$result = ERROR_FLAG_COMMENT_DB_SAVE;
		}
	}
	else
	{
		# DB登録エラー
		$result = ERROR_FLAG_COMMENT_DB_REGIST;
	}
	
	if($result == 0)
	{
		$$comment_id_ref = $current_comment_data{'id'};
	}
	
	return $result;
}

# コメントステータスチェック
#  param[in] : コメントID
#  param[in] : コメント対象のエントリID
#  param[in] : コメントステータス
#  return    : 結果
#                bit  0 … コメントID
#                bit  2 … エントリID
#                bit  3 … ユーザーID
#                bit  9 … ステータス
sub check_comment_status
{
	my ($comment_id, $comment_entry_id, $comment_status) = @_;
	my $result = 0;
	
	if(($comment_status ne 'public')        && ($comment_status ne 'private')        && ($comment_status ne 'delete')     &&
	   ($comment_status ne 'all_public')    && ($comment_status ne 'all_private')    && ($comment_status ne 'all_delete') &&
	   ($comment_status ne 'select_public') && ($comment_status ne 'select_private') && ($comment_status ne 'select_delete'))
	{
		# ステータス指定エラー
		$result = ERROR_FLAG_COMMENT_STATUS;
	}
	
	if(($comment_status eq 'public') || ($comment_status eq 'private') || ($comment_status eq 'delete'))
	{
		if($comment_id ne '')
		{
			my $comment_index_no = get_comment_index($comment_id);
		
			if($comment_index_no >= 0)
			{
				my $entry_id = get_comment_data($comment_index_no, 'entry_id');
			
				if($comment_entry_id eq $entry_id)
				{
					my $entry_user_id = get_entry_data($entry_id, 'user_id');
				
					if((!defined($entry_user_id)) || ($entry_user_id ne ${get_system_param_ref('login_user_id')}))
					{
						# ユーザーIDエラー（コメントを管理する権限がない）
						$result |= ERROR_FLAG_COMMENT_USER_ID;
					}
				}
				else
				{
					# コメント対象のエントリID指定エラー
					$result |= ERROR_FLAG_COMMENT_ENTRY_ID;
				}
			}
			else
			{
				# コメントID指定エラー
				$result |= ERROR_FLAG_COMMENT_ID;
			}
		}
		else
		{
			# コメントID指定エラー
			$result |= ERROR_FLAG_COMMENT_ID;
		}
	}
	
	return $result;
}

# コメントステータス更新
#  param[in] : コメントID
#  param[in] : コメントステータス
#  return    : 結果
#                 bit  9 … 登録エラー
#                 bit 10 … 保存エラー
sub update_comment_status
{
	my %new_status_hash = ('id' => $_[0], 'status' => $_[1]);
	my $result = 0;
	
	if($comment_db->update_hash(\%new_status_hash) >= 0)
	{
		if($comment_db->write() != 0)
		{
			# DB保存エラー
			$result = ERROR_FLAG_COMMENT_DB_SAVE;
		}
	}
	else
	{
		# DB登録エラー
		$result = ERROR_FLAG_COMMENT_DB_REGIST;
	}
	
	return $result;
}

# 指定エントリIDの全コメントステータス更新
#  param[in] : エントリID
#  param[in] : コメントステータス
#  return    : 結果
#                 bit  9 … 登録エラー
#                 bit 10 … 保存エラー
sub all_update_comment_status
{
	my ($entry_id, $status) = @_;
	my $result = 0;
	
	my %attr = ('entry_id' => $entry_id);
	init_comment_db_list(\%attr, 0, 0);
	
	while(1)
	{
		my $ref_hash = fetch_comment_db_list();
		if(!defined($ref_hash))
		{
			last;
		}
		
		if(exists($$ref_hash{'id'}))
		{
			my %new_status_hash = ('id' => $$ref_hash{'id'}, 'status' => $status);
			if($comment_db->update_hash(\%new_status_hash) < 0)
			{
				# DB登録エラー
				$result = ERROR_FLAG_COMMENT_DB_REGIST;
				
				last;
			}
		}
	}
	
	if($result == 0)
	{
		if($comment_db->write() != 0)
		{
			# DB保存エラー
			$result = ERROR_FLAG_COMMENT_DB_SAVE;
		}
	}
	
	return $result;
}

# 新規コメントID取得
#  return    : コメントID
sub get_new_comment_id
{
	return $comment_db->get_hash_num() + 1;
}

# 指定インデックスNo.のコメントデータの指定パラメータ取得
#  param[in] : インデックスNo.
#  param[in] : パラメータ種別
#  return    : データ(''の場合は該当なし)
sub get_comment_data
{
	my ($index_no, $param) = @_;
	
	if($index_no == -1)
	{
		if(exists($current_comment_data{$param}))
		{
			return $current_comment_data{$param};
		}
	}
	else
	{
		return $comment_db->get_data($index_no, $param);
	}
	
	return '';
}

# 指定コメントIDのデータベースインデックスNo.取得
#  param[in] : コメントID
#  return    : データベースインデックスNo.(-1の場合は該当なし)
sub get_comment_index
{
	my $search_id = $_[0];
	
	for(my $index_no = 0;$index_no < $comment_db->get_hash_num();$index_no++)
	{
		if($search_id == $comment_db->get_data($index_no, 'id'))
		{
			return $index_no;
		}
	}
	
	return -1;
}

# コメントDB用条件チェックルーチン
#  param[in] : チェック対象のハッシュリファレンス
#  param[in] : 条件アトリビュートリファレンス
#  return : チェック結果
sub condition_comment
{
	my ($ref_hash, $ref_attr) = @_;
	my %check_func =
		(
		 'id'        => sub { return ($_[0] == $_[1]); },
		 'parent_id' => sub { return ($_[0] == $_[1]); },
		 'entry_id'  => sub { return ($_[0] == $_[1]); },
		 'user_id'   => sub { return ($_[0] eq $_[1]); },
		 'date'      => sub { return (index($_[1], $_[0]) == 0); },
		 'time'      => sub { return (index($_[1], $_[0]) == 0); },
		 'name'      => sub { return ($_[0] eq $_[1]); },
		 'title'     => sub { return ($_[0] eq $_[1]); },
		 'text'      => sub { return ($_[0] eq $_[1]); },
		 'mail'      => sub { return ($_[0] eq $_[1]); },
		 'url'       => sub { return ($_[0] eq $_[1]); },
		 'status'    => sub { return ($_[0] eq $_[1]); },
		);
	my @types = keys(%$ref_attr);
	
	return check_condition_db($ref_hash, $ref_attr, \%check_func, \@types);
}

# 指定条件に合致するコメントデータベースリストを生成
#  param[in] : 指定条件アトリビュートリファレンス
#  param[in] : 行オフセット
#  param[in] : 行最大数（0の場合は制限無し）
sub init_comment_db_list
{
	my ($ref_attr, $start, $max_num) = @_;
	
	$comment_db->init_db_list(\&condition_comment, $ref_attr, undef, \&sort_date_time, 1, $start, $max_num);
}

# コメントデータベースリストをプッシュ
sub push_comment_db_list
{
	$comment_db->push_db_list();
}

# コメントデータベースリストをポップ
sub pop_comment_db_list
{
	$comment_db->pop_db_list();
}

# コメントデータベースリストからフェッチ
sub fetch_comment_db_list
{
	return $comment_db->fetch_db_list_hashref();
}

# コメントデータベースリストのサイズ取得
sub get_comment_db_list_size
{
	return $comment_db->get_db_list_size();
}

# 指定IDからオフセットで指定された位置のコメントIDを返す
#  param[in] : コメントID
#  param[in] : オフセット位置
#  return    : コメントID(''の場合は該当なし)
sub get_comment_id
{
	my ($comment_id, $offset) = @_;
	my $index_no = -1;
	my $ret_id   = '';
	
	if($comment_id ne '')
	{
		$index_no = get_comment_index($comment_id);
	}
	
	if($index_no >= 0)
	{
		$index_no = $comment_db->search_db_list($index_no, $offset);
		
		if($index_no >= 0)
		{
			$ret_id = get_comment_data($index_no, 'id');
		}
	}
	
	return $ret_id;
}

# コメント処理のエラーメッセージ文字列追加
#  param[in] : 結果
sub comment_error_add_error_msg
{
	my $result = $_[0];
	
	if($result & ERROR_FLAG_COMMENT_ID)
	{
		add_error_msg(\$main::res_string{'comment_id_error'});
	}
	
	if($result & ERROR_FLAG_COMMENT_NO)
	{
		add_error_msg(\$main::res_string{'comment_no_error'});
	}
	
	if($result & ERROR_FLAG_COMMENT_ENTRY_ID)
	{
		add_error_msg(\$main::res_string{'comment_entry_id_error'});
	}
	
	if($result & ERROR_FLAG_COMMENT_USER_ID)
	{
		add_error_msg(\$main::res_string{'comment_user_id_error'});
	}
	
	if($result & ERROR_FLAG_COMMENT_NAME)
	{
		add_error_msg(\$main::res_string{'comment_name_error'});
	}
	
	if($result & ERROR_FLAG_COMMENT_TITLE)
	{
		add_error_msg(\$main::res_string{'comment_title_error'});
	}
	
	if($result & ERROR_FLAG_COMMENT_TEXT)
	{
		add_error_msg(\$main::res_string{'comment_text_error'});
	}
	
	if($result & ERROR_FLAG_COMMENT_MAIL)
	{
		add_error_msg(\$main::res_string{'comment_mail_error'});
	}
	
	if($result & ERROR_FLAG_COMMENT_URL)
	{
		add_error_msg(\$main::res_string{'comment_url_error'});
	}
	
	if($result & ERROR_FLAG_COMMENT_STATUS)
	{
		add_error_msg(\$main::res_string{'comment_status_error'});
	}
	
	if($result & ERROR_FLAG_COMMENT_DB_REGIST)
	{
		add_error_msg(\$main::res_string{'comment_db_regist_error'});
	}
	
	if($result & ERROR_FLAG_COMMENT_DB_SAVE)
	{
		add_error_msg(\$main::res_string{'comment_db_save_error'});
	}
}

1;
