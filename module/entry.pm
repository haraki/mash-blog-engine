#### エントリ ####

use strict;

use Mash::MashDB;
use database;

use constant ERROR_FLAG_ENTRY_ID          => (1 <<  0);
use constant ERROR_FLAG_ENTRY_USER_ID     => (1 <<  1);
use constant ERROR_FLAG_ENTRY_PUBLIC_TYPE => (1 <<  2);
use constant ERROR_FLAG_ENTRY_DATE        => (1 <<  3);
use constant ERROR_FLAG_ENTRY_TIME        => (1 <<  4);
use constant ERROR_FLAG_ENTRY_TITLE       => (1 <<  5);
use constant ERROR_FLAG_ENTRY_TEXT        => (1 <<  6);
use constant ERROR_FLAG_ENTRY_CATEGORY_ID => (1 <<  7);
use constant ERROR_FLAG_ENTRY_TAGS        => (1 <<  8);
use constant ERROR_FLAG_ENTRY_COMMENT     => (1 <<  9);
use constant ERROR_FLAG_ENTRY_TRACKBACK   => (1 << 10);
use constant ERROR_FLAG_ENTRY_DB_REGIST   => (1 << 11);
use constant ERROR_FLAG_ENTRY_DB_SAVE     => (1 << 12);

my $entry_db_file = 'entry_db.cgi';
my $entry_db;

my %entry_db_format =
	(
	 'id'          => 'integer,primary',
	 'user_id'     => 'text',
	 'public_type' => 'text',
	 'date'        => 'date',
	 'time'        => 'time',
	 'title'       => 'text',
	 'text'        => 'text',
	 'category_id' => 'integer,array',
	 'tag'         => 'text,array',
	 'comment'     => 'text',
	 'trackback'   => 'text',
	 'update_time' => 'integer',
	);

#
# エントリハッシュフォーマット
#
#    {'id'}          エントリID
#    {'user_id'}     ユーザーID
#    {'public_type'} 公開設定           ('public', 'user' or 'private')
#    {'date'}        公開年月日         (yyyy-mm-dd)
#    {'time'}        公開時刻           (hh-mm-ss)
#    {'title'}       タイトル
#    {'text'}        本文
#    {'category_id'} カテゴリID         ('id0,id1,id2...')
#    {'tag'}         タグ               ('tag0,tag1,tag2...')
#    {'comment'}     コメント設定       ('enable', 'approval' or 'disable')
#    {'trackback'}   トラックバック設定 ('enable', 'approval' or 'disable')
#    {'update_time'} 変更時刻
#
my %current_entry_data =
	(
	 'id'          => -1,
	 'user_id'     => '',
	 'public_type' => '',
	 'date'        => '',
	 'time'        => '',
	 'title'       => '',
	 'text'        => '',
	 'category_id' => [],
	 'tag'         => [],
	 'comment'     => '',
	 'trackback'   => '',
	 'update_time' => 0,
	);

# エントリDB初期化
#  param[in] : DBルートディレクトリパス
#  return    : 結果
sub init_entry_db
{
	my $db_root_path = $_[0];
	my $result = 0;
	
	$entry_db = new Mash::MashDB($db_root_path . $entry_db_file);
	
	if(read_db($entry_db, \%entry_db_format) != 0)
	{
		$result = -1;
	}
	
	return $result;
}

# カレントエントリデータのセット
#  param[in] : エントリID
#  param[in] : アカウントID (0-9, a-z, -, _)
#  param[in] : 公開設定           ('public', 'user' or 'private')
#  param[in] : 公開年月日         (yyyy-mm-dd)
#  param[in] : 公開時刻           (hh-mm-ss)
#  param[in] : タイトル
#  param[in] : 本文
#  param[in] : カテゴリID配列リファレンス ('id0,id1,id2...')
#  param[in] : タグ配列リファレンス       ('tag0,tag1,tag2...')
#  param[in] : コメント設定       ('enable', 'approval' or 'disable')
#  param[in] : トラックバック設定 ('enable', 'approval' or 'disable')
#  param[in] : 変更時刻
sub set_current_entry_data
{
	my $text = '';
	
	($current_entry_data{'id'},
	 $current_entry_data{'user_id'},
	 $current_entry_data{'public_type'},
	 $current_entry_data{'date'},
	 $current_entry_data{'time'},
	 $current_entry_data{'title'},
	 $text,									# 本文はエスケープしない
	 $current_entry_data{'category_id'},
	 $current_entry_data{'tag'},
	 $current_entry_data{'comment'},
	 $current_entry_data{'trackback'},
	 $current_entry_data{'update_time'}) = @_;
	
	escape_string_hash(\%current_entry_data);
	
	$current_entry_data{'text'} = $text;
}

# ハッシュからのカレントエントリデータのセット
#  param[in] : ハッシュ
sub set_current_entry_hash
{
	my %entry_hash = @_;
	
	foreach my $key (keys(%current_entry_data))
	{
		if(exists($entry_hash{$key}))
		{
			$current_entry_data{$key} = $entry_hash{$key};
		}
	}
	
	my $text = $current_entry_data{'text'};			# 本文はエスケープしない
	$current_entry_data{'text'} = '';
	
	escape_string_hash(\%current_entry_data);
	
	$current_entry_data{'text'} = $text;
}

# 既存のエントリデータをカレントエントリデータにセット
#  param[in] : エントリID
#  return    : 結果
#                bit  0 … エントリID
sub set_current_entry_data_existing
{
	my $entry_id = $_[0];
	my $entry_hash_ref = get_entry_data($entry_id);
	
	if(!defined($entry_hash_ref))
	{
		return ERROR_FLAG_ENTRY_ID;
	}
	
	set_current_entry_data($entry_id,
						   ${get_system_param_ref('login_user_id')},
						   $$entry_hash_ref{'public_type'},
						   $$entry_hash_ref{'date'},
						   $$entry_hash_ref{'time'},
						   $$entry_hash_ref{'title'},
						   $$entry_hash_ref{'text'},
						   $$entry_hash_ref{'category_id'},
						   $$entry_hash_ref{'tag'},
						   $$entry_hash_ref{'comment'},
						   $$entry_hash_ref{'trackback'},
						   $$entry_hash_ref{'update_time'});
	
	return 0;
}

# エントリハッシュチェック
#  return    : 結果
#                bit  0 … エントリID
#                bit  1 … ユーザーID
#                bit  2 … 公開設定
#                bit  3 … 公開年月日
#                bit  4 … 公開時刻
#                bit  5 … タイトル
#                bit  6 … 本文
#                bit  7 … カテゴリID
#                bit  8 … タグ
#                bit  9 … コメント設定
#                bit 10 … トラックバック設定
sub check_entry_data
{
	my $result = 0;
	
	if(($current_entry_data{'id'} != -1) && (!is_entry_exist($current_entry_data{'id'})))
	{
		# エントリIDエラー
		$result |= ERROR_FLAG_ENTRY_ID;
	}
	
	if(get_account_index($current_entry_data{'user_id'}) == -1)
	{
		# ユーザーIDエラー
		$result |= ERROR_FLAG_ENTRY_USER_ID;
	}
	
	if(($current_entry_data{'public_type'} ne 'public') && ($current_entry_data{'public_type'} ne 'reserve') && ($current_entry_data{'public_type'} ne 'private'))
	{
		# 公開設定エラー
		$result |= ERROR_FLAG_ENTRY_PUBLIC_TYPE;
	}
	
	if(check_date_str($current_entry_data{'date'}) != 0)
	{
		# 公開年月日エラー
		$result |= ERROR_FLAG_ENTRY_DATE;
	}
	
	if(check_time_str($current_entry_data{'time'}) != 0)
	{
		# 公開時刻エラー
		$result |= ERROR_FLAG_ENTRY_TIME;
	}
	
	if(check_form_str($current_entry_data{'title'}, ${get_system_param_ref('entry_title_min')}, ${get_system_param_ref('entry_title_max')}) != 0)
	{
		# タイトルエラー
		$result |= ERROR_FLAG_ENTRY_TITLE;
	}
	
	if(check_form_str($current_entry_data{'text'}, ${get_system_param_ref('entry_text_min')}, ${get_system_param_ref('entry_text_max')}) != 0)
	{
		# 本文エラー
		$result |= ERROR_FLAG_ENTRY_TEXT;
	}
	
	if((${get_system_param_ref('entry_category_num_min')} > 0) && (${get_system_param_ref('entry_category_num_min')} > @{$current_entry_data{'category_id'}}))
	{
		# カテゴリIDエラー
		$result |= ERROR_FLAG_ENTRY_TAGS;
	}
	
	foreach(@{$current_entry_data{'category_id'}})
	{
		if(get_category_index($_) == -1)
		{
			# カテゴリIDエラー
			$result |= ERROR_FLAG_ENTRY_CATEGORY_ID;
			
			last;
		}
	}
	
	if((${get_system_param_ref('entry_tag_num_min')} > 0) && (${get_system_param_ref('entry_tag_num_min')} > @{$current_entry_data{'tag'}}))
	{
		# タグエラー
		$result |= ERROR_FLAG_ENTRY_TAGS;
	}
	
	foreach(@{$current_entry_data{'tag'}})
	{
		if(check_tag_str($_, ${get_system_param_ref('entry_tag_min')}, ${get_system_param_ref('entry_tag_max')}) != 0)
		{
			# タグエラー
			$result |= ERROR_FLAG_ENTRY_TAGS;
			
			last;
		}
	}
	
	if(($current_entry_data{'comment'} ne 'enable') && ($current_entry_data{'comment'} ne 'approval') && ($current_entry_data{'comment'} ne 'disable'))
	{
		# コメント設定エラー
		$result |= ERROR_FLAG_ENTRY_COMMENT;
	}
	
	if(($current_entry_data{'trackback'} ne 'enable') && ($current_entry_data{'trackback'} ne 'approval') && ($current_entry_data{'trackback'} ne 'disable'))
	{
		# トラックバック設定エラー
		$result |= ERROR_FLAG_ENTRY_TRACKBACK;
	}
	
	return $result;
}

# エントリ登録
#  param[out] : 登録されたエントリのID
#  return     : 結果
#                 bit 11 … 登録エラー
#                 bit 12 … 保存エラー
sub regist_entry_data
{
	my $entry_id_ref = $_[0];
	my $result       = 0;
	
	if($current_entry_data{'id'} == -1)
	{
		$current_entry_data{'id'} = get_new_entry_id();
	}
	
	if($entry_db->regist_hash(\%current_entry_data) >= 0)
	{
		if($entry_db->write() != 0)
		{
			# DB保存エラー
			$result = ERROR_FLAG_ENTRY_DB_SAVE;
		}
	}
	else
	{
		# DB登録エラー
		$result = ERROR_FLAG_ENTRY_DB_REGIST;
	}
	
	if($result == 0)
	{
		$$entry_id_ref = $current_entry_data{'id'};
	}
	
	return $result;
}

# 予約中のエントリをチェック
sub check_reserve_entry
{
	my $chg_num  = 0;
	my $date_str = '';
	my $time_str = '';
	my $result   = 0;
	
	Mash::MashTime::get_current_date_time(\$date_str, \$time_str);
	
	my $entry_num = $entry_db->get_hash_num();
	
	for(my $index_no = 0;$index_no < $entry_num;$index_no++)
	{
		my $ref_entry_db_data = $entry_db->get_hash($index_no);
		
		if((defined($ref_entry_db_data)) &&
		   ($$ref_entry_db_data{'public_type'} eq 'reserve') && 
		   (($$ref_entry_db_data{'date'} lt $date_str) ||
		    (($$ref_entry_db_data{'date'} eq $date_str) && ($$ref_entry_db_data{'time'} lt $time_str))))
		{
			$$ref_entry_db_data{'public_type'} = 'public';
			
			$chg_num++;
		}
	}
	
	if($chg_num > 0)
	{
		# 読み込みは成功しているので、書き込み結果は無視する
		if($entry_db->write() != 0)
		{
			# DB保存エラー
			$result = ERROR_FLAG_ENTRY_DB_SAVE;
		}
	}
	
	return $result;
}

# 指定エントリIDのエントリデータの指定パラメータ取得
#  param[in] : エントリID
#  param[in] : パラメータ種別
#  param[in] : 要素No.
#  return    : データ(''の場合は該当なし)
sub get_entry_data
{
	my ($entry_id, $param, $element_no) = @_;
	
	if((!defined($entry_id)) || ($entry_id eq '') || ($entry_id < 0))
	{
		if((defined($param)) && (exists($current_entry_data{$param})))
		{
			my $value    = $current_entry_data{$param};
			my $ref_type = ref($value);
			
			if($ref_type eq '')
			{
				return $value;
			}
			elsif($ref_type eq 'ARRAY')
			{
				my $array_size = @{$value};
				
				if((!defined($element_no)) || ($element_no eq ''))
				{
					return $array_size;
				}
				elsif($element_no < $array_size)
				{
					return ${$value}[$element_no];
				}
			}
		}
	}
	else
	{
		my $index_no = get_entry_index($entry_id);
		
		return $entry_db->get_data($index_no, $param, $element_no);
	}
	
	return '';
}

# 指定エントリIDのデータベースインデックスNo.取得
#  param[in] : エントリID
#  return    : データベースインデックスNo.(-1の場合は該当なし)
sub get_entry_index
{
	my $search_id = $_[0];
	my $entry_num = $entry_db->get_hash_num();
	
	for(my $index_no = 0;$index_no < $entry_num;$index_no++)
	{
		if($search_id == $entry_db->get_data($index_no, 'id'))
		{
			return $index_no;
		}
	}
	
	return -1;
}

sub is_entry_exist
{
	my $entry_id = $_[0];
	
	my $index_no = get_entry_index($entry_id);
	
	if($index_no >= 0)
	{
		return 1;
	}
	
	return 0;
}

# 新規エントリID取得
#  return    : エントリID
sub get_new_entry_id
{
	return $entry_db->get_hash_num() + 1;
}

# エントリ日付リスト生成
#  param[in] : タイプ('year', 'month' or 'day')
#  param[in] : リストリファレンス
#  return    : 結果
sub make_entry_date_list
{
	my ($type, $ref_list) = @_;
	
	if((defined($type)) && (($type eq 'year') || ($type eq 'month') || ($type eq 'day')) &&
	   (defined($ref_list)) && (ref($ref_list) eq 'ARRAY'))
	{
		my %date_hash = ();
		my $entry_num = $entry_db->get_hash_num();
		
		for(my $index_no = 0;$index_no < $entry_num;$index_no++)
		{
			my $ref_entry_db_data = $entry_db->get_hash($index_no);
			
			if(defined($ref_entry_db_data))
			{
				my $date_str = $$ref_entry_db_data{'date'};
				
				if($type eq 'year')
				{
					$date_str =~ s/(^[0-9]{4})\-[0-9]{2}\-[0-9]{2}/$1/;
				}
				elsif($type eq 'month')
				{
					$date_str =~ s/(^[0-9]{4}\-[0-9]{2})\-[0-9]{2}/$1/;
				}
				
				$date_hash{$date_str} = 1;
			}
		}
		
		@$ref_list = sort(keys(%date_hash));
	}
	
	return -1;
}

# エントリDB用条件チェックルーチン
#  param[in] : チェック対象のハッシュリファレンス
#  param[in] : 条件アトリビュートリファレンス
#  return : チェック結果
sub condition_entry
{
	my ($ref_hash, $ref_attr) = @_;
	
	my %check_func =
		(
		 'id'          => sub { return ($_[0] == $_[1]); },
		 'user_id'     => sub { return ($_[0] eq $_[1]); },
		 'public_type' => sub { return ($_[0] eq $_[1]); },
		 'date'        => sub { return (index($_[1], $_[0]) == 0); },
		 'time'        => sub { return (index($_[1], $_[0]) == 0); },
		 'title'       => sub { return ($_[0] eq $_[1]); },
		 'text'        => sub { return ($_[0] eq $_[1]); },
		 'category_id' => sub { return is_child_category($_[1], $_[0]); },
		 'tag'         => sub { return ($_[0] eq $_[1]); },
		 'comment'     => sub { return ($_[0] eq $_[1]); },
		 'trackback'   => sub { return ($_[0] eq $_[1]); },
		);
	my @types = keys(%$ref_attr);
	
	return check_condition_db($ref_hash, $ref_attr, \%check_func, \@types);
}

# 指定条件に合致するエントリデータベースリストを生成
#  param[in] : 指定条件アトリビュートリファレンス
#  param[in] : 行オフセット
#  param[in] : 行最大数（0の場合は制限無し）
#  param[in] : DBリスト条件関数(undefの場合はデフォルト)
#  param[in] : DBリスト条件関数用オプション(undefの場合はなし)
#  param[in] : DBリストソート関数(undefの場合はデフォルト)
sub init_entry_db_list
{
	my ($ref_attr, $start, $max_num, $ref_cond_func, $ref_cond_option, $ref_sort_func) = @_;
	
	if(!(defined($ref_cond_func)))
	{
		$ref_cond_func = \&condition_entry;
	}
	
	if(!(defined($ref_sort_func)))
	{
		$ref_sort_func = \&sort_date_time;
	}
	
	$entry_db->init_db_list($ref_cond_func, $ref_attr, $ref_cond_option, $ref_sort_func, 1, $start, $max_num);
}

# エントリデータベースリストをプッシュ
sub push_entry_db_list
{
	$entry_db->push_db_list();
}

# エントリデータベースリストをポップ
sub pop_entry_db_list
{
	$entry_db->pop_db_list();
}

# エントリデータベースリストからフェッチ
sub fetch_entry_db_list
{
	return $entry_db->fetch_db_list_hashref();
}

# エントリデータベースリストのサイズ取得
sub get_entry_db_list_size
{
	return $entry_db->get_db_list_size();
}

# 指定IDからオフセットで指定された位置のエントリIDを返す
#  param[in] : エントリID
#  param[in] : オフセット位置
#  return    : エントリID(''の場合は該当なし)
sub get_entry_id
{
	my ($entry_id, $offset) = @_;
	my $index_no = -1;
	my $ret_id   = '';
	
	if($entry_id ne '')
	{
		$index_no = get_entry_index($entry_id);
	}
	
	if($index_no >= 0)
	{
		$index_no = $entry_db->search_db_list($index_no, $offset);
		
		if($index_no >= 0)
		{
			$ret_id = $entry_db->get_data($index_no, 'id');
		}
	}
	
	return $ret_id;
}

# エントリ処理のエラーメッセージ文字列追加
#  param[in] : 結果
sub entry_error_add_error_msg
{
	my $result = $_[0];
	
	if($result & ERROR_FLAG_ENTRY_ID)
	{
		add_error_msg(\$main::res_string{'entry_id_error'});
	}
	
	if($result & ERROR_FLAG_ENTRY_USER_ID)
	{
		add_error_msg(\$main::res_string{'entry_user_id_error'});
	}
	
	if($result & ERROR_FLAG_ENTRY_PUBLIC_TYPE)
	{
		add_error_msg(\$main::res_string{'entry_public_type_error'});
	}
	
	if($result & ERROR_FLAG_ENTRY_DATE)
	{
		add_error_msg(\$main::res_string{'entry_date_error'});
	}
	
	if($result & ERROR_FLAG_ENTRY_TIME)
	{
		add_error_msg(\$main::res_string{'entry_time_error'});
	}
	
	if($result & ERROR_FLAG_ENTRY_TITLE)
	{
		add_error_msg(\$main::res_string{'entry_title_error'});
	}
	
	if($result & ERROR_FLAG_ENTRY_TEXT)
	{
		add_error_msg(\$main::res_string{'entry_text_error'});
	}
	
	if($result & ERROR_FLAG_ENTRY_CATEGORY_ID)
	{
		add_error_msg(\$main::res_string{'entry_category_id_error'});
	}
	
	if($result & ERROR_FLAG_ENTRY_TAGS)
	{
		add_error_msg(\$main::res_string{'entry_tag_error'});
	}
	
	if($result & ERROR_FLAG_ENTRY_COMMENT)
	{
		add_error_msg(\$main::res_string{'entry_comment_error'});
	}
	
	if($result & ERROR_FLAG_ENTRY_TRACKBACK)
	{
		add_error_msg(\$main::res_string{'entry_trackback_error'});
	}
	
	if($result & ERROR_FLAG_ENTRY_DB_REGIST)
	{
		add_error_msg(\$main::res_string{'entry_db_regist_error'});
	}
	
	if($result & ERROR_FLAG_ENTRY_DB_SAVE)
	{
		add_error_msg(\$main::res_string{'entry_db_save_error'});
	}
}

1;
