######## アカウント ########

use strict;

use Mash::MashDB;
use database;

use constant ERROR_FLAG_ACCOUNT_ID                => (1 <<  0);
use constant ERROR_FLAG_ACCOUNT_ID_DUPLICATE      => (1 <<  1);
use constant ERROR_FLAG_ACCOUNT_PASSWORD          => (1 <<  2);
use constant ERROR_FLAG_ACCOUNT_NAME              => (1 <<  3);
use constant ERROR_FLAG_ACCOUNT_NAME_DUPLICATE    => (1 <<  4);
use constant ERROR_FLAG_ACCOUNT_TYPE              => (1 <<  5);
use constant ERROR_FLAG_ACCOUNT_DB_REGIST         => (1 <<  6);
use constant ERROR_FLAG_ACCOUNT_DB_SAVE           => (1 <<  7);
use constant ERROR_FLAG_ACCOUNT_ONLY_ADMIN_CHANGE => (1 <<  8);
use constant ERROR_FLAG_ACCOUNT_ONLY_ADMIN_DELETE => (1 <<  9);

my $account_db_file = 'account_db.cgi';
my $account_db;

my %account_db_format =
	(
	 'id'          => 'text,primary',
	 'password'    => 'text',
	 'name'        => 'text',
	 'type'        => 'text',
	 'update_time' => 'integer',
	);

#
# アカウントハッシュフォーマット
#
#    {'id'}          ユーザーID       (0-9, a-z, -, _)
#    {'password'}    パスワード
#    {'repassword'}  再入力パスワード
#    {'name'}        ユーザー名
#    {'type'}        タイプ           ('admin' or 'normal')
#
my %current_account_data =
	(
	 'id'          => '',
	 'password'    => '',
	 'repassword'  => '',
	 'name'        => '',
	 'type'        => '',
	 'update_time' => 0,
	);

# アカウントDB初期化
#  param[in] : DBルートディレクトリパス
#  return    : 結果
sub init_account_db
{
	my $db_root_path = $_[0];
	my $result = 0;
	
	$account_db = new Mash::MashDB($db_root_path . $account_db_file);
	
	if(read_db($account_db, \%account_db_format) != 0)
	{
		$result = -1;
	}
	
	return $result;
}

# カレントアカウントデータのセット
#  param[in] : アカウントID (0-9, a-z, -, _)
#  param[in] : パスワード
#  param[in] : 再入力パスワード
#  param[in] : ユーザー名
#  param[in] : タイプ ('admin' or 'normal')
#  param[in] : 変更時刻
sub set_current_account_data
{
	($current_account_data{'id'}, $current_account_data{'name'}, $current_account_data{'password'}, $current_account_data{'repassword'}, $current_account_data{'type'}, $current_account_data{'update_time'}) = @_;
	
	escape_string_hash(\%current_account_data);
}

# 既存のアカウントデータをカレントアカウントデータにセット
#  param[in] : アカウントID (0-9, a-z, -, _)
#  return    : 結果
#                bit  0 … ユーザーID
sub set_current_account_data_existing
{
	my $account_id = $_[0];
	my $account_index_no = get_account_index($account_id);
	
	if($account_index_no < 0)
	{
		return ERROR_FLAG_ACCOUNT_ID;
	}
	
	set_current_account_data($account_id, get_account_data($account_index_no, 'name'), '', '', get_account_data($account_index_no, 'type'), get_account_data($account_index_no, 'update_time'));
	
	return 0;
}

# アカウントハッシュチェック
#  return    : 結果
#                bit  0 … ユーザーID
#                bit  1 … ユーザーID重複
#                bit  2 … パスワード
#                bit  3 … ユーザー名
#                bit  4 … ユーザー名重複
#                bit  5 … タイプ
#                bit  6 … パーミッション
sub check_account_data
{
	my $result = 0;
	
	if(($current_account_data{'id'} eq '') || ($current_account_data{'id'} =~ m/[^\-0-9a-z_]/) || ($current_account_data{'id'} eq 'guest'))
	{
		# ユーザーIDエラー
		$result |= ERROR_FLAG_ACCOUNT_ID;
	}
	
	if($current_account_data{'name'} eq '')
	{
		# ユーザー名エラー
		$result |= ERROR_FLAG_ACCOUNT_NAME;
	}
	
	my $account_num = $account_db->get_hash_num();
	
	for(my $index_no = 0;$index_no < $account_num;$index_no++)
	{
		my $ref_account_db_data = $account_db->get_hash($index_no);
		
		if(defined($ref_account_db_data))
		{
			if($$ref_account_db_data{'id'} eq $current_account_data{'id'})
			{
				# ユーザーID重複エラー
				$result |= ERROR_FLAG_ACCOUNT_ID_DUPLICATE;
			}
			
			if($$ref_account_db_data{'name'} eq $current_account_data{'name'})
			{
				# ユーザー名重複エラー
				$result |= ERROR_FLAG_ACCOUNT_NAME_DUPLICATE;
			}
		}
	}
	
	if(($current_account_data{'password'} eq '') ||
	   (length($current_account_data{'password'}) < ${get_system_param_ref('password_min')}) ||
	   ($current_account_data{'password'} ne $current_account_data{'repassword'}))
	{
		# パスワードエラー
		$result |= ERROR_FLAG_ACCOUNT_PASSWORD;
	}
	
	if($account_num == 0)
	{
		if($current_account_data{'type'} ne 'admin')
		{
			# タイプエラー
			$result |= ERROR_FLAG_ACCOUNT_TYPE;
		}
	}
	else
	{
		if(($current_account_data{'type'} ne 'admin') && ($current_account_data{'type'} ne 'normal'))
		{
			# タイプエラー
			$result |= ERROR_FLAG_ACCOUNT_TYPE;
		}
	}
	
	return $result;
}

# アカウントリネームチェック
#  return    : 結果
#                bit  0 … ユーザーID
#                bit  1 … ユーザーID重複
#                bit  2 … パスワード
#                bit  3 … ユーザー名
#                bit  4 … ユーザー名重複
#                bit  5 … タイプ
#                bit  6 … パーミッション
sub check_rename_account
{
	my $result = 0;
	
	if(($current_account_data{'id'} eq '') || ($current_account_data{'id'} =~ m/[^\-0-9a-z_]/) || ($current_account_data{'id'} eq 'guest'))
	{
		# ユーザーIDエラー
		$result |= ERROR_FLAG_ACCOUNT_ID;
	}
	
	if($current_account_data{'name'} eq '')
	{
		# ユーザー名エラー
		$result |= ERROR_FLAG_ACCOUNT_NAME;
	}
	
	if($current_account_data{'id'} ne '')
	{
		my $account_index_no = get_account_index($current_account_data{'id'});
		
		if(($account_index_no == -1) ||
		   ((${get_system_param_ref('login_type')} ne 'admin') && (${get_system_param_ref('login_user_id')} ne $current_account_data{'id'})))
		{
			# ユーザーID指定エラー
			$result |= ERROR_FLAG_ACCOUNT_ID;
		}
		else
		{
			my %attr = ('type' => 'admin');
			init_account_db_list(\%attr, 0, 0);
			
			if((get_account_data($account_index_no, 'type') eq 'admin') && ($current_account_data{'type'} eq 'normal') && ($account_db->get_db_list_size() == 1))
			{
				# 管理者が1人のみ
				$result |= ERROR_FLAG_ACCOUNT_ONLY_ADMIN_CHANGE;
			}
		}
	}
	
	my $account_num = $account_db->get_hash_num();
	
	for(my $index_no = 0;$index_no < $account_num;$index_no++)
	{
		my $ref_account_db_data = $account_db->get_hash($index_no);
		
		if((defined($ref_account_db_data)) &&
		   ($$ref_account_db_data{'id'}   ne $current_account_data{'id'}) &&
		   ($$ref_account_db_data{'name'} eq $current_account_data{'name'}))
		{
			# ユーザー名重複エラー
			$result |= ERROR_FLAG_ACCOUNT_NAME_DUPLICATE;
		}
	}
	
	if(($current_account_data{'password'} eq '') ||
	   (length($current_account_data{'password'}) < ${get_system_param_ref('password_min')}) ||
	   ($current_account_data{'password'} ne $current_account_data{'repassword'}))
	{
		# パスワードエラー
		$result |= ERROR_FLAG_ACCOUNT_PASSWORD;
	}
	
	if($account_num == 0)
	{
		if($current_account_data{'type'} ne 'admin')
		{
			# タイプエラー
			$result |= ERROR_FLAG_ACCOUNT_TYPE;
		}
	}
	else
	{
		if(($current_account_data{'type'} ne 'admin') && ($current_account_data{'type'} ne 'normal'))
		{
			# タイプエラー
			$result |= ERROR_FLAG_ACCOUNT_TYPE;
		}
	}
	
	return $result;
}

# アカウント削除チェック
#  param[in] : アカウントID
#  return    : 結果
#                bit  0 … ユーザーID
#                bit  1 … ユーザーID重複
#                bit  2 … パスワード
#                bit  3 … ユーザー名
#                bit  4 … ユーザー名重複
#                bit  5 … タイプ
#                bit  6 … パーミッション
sub check_delete_account
{
	my ($account_id) = @_;
	my $result = 0;
	
	if($account_id ne '')
	{
		my $account_index_no = get_account_index($account_id);
		
		if(($account_index_no == -1) || ((${get_system_param_ref('login_type')} ne 'admin') && (${get_system_param_ref('login_user_id')} ne $account_id)))
		{
			# ユーザーID指定エラー
			$result = ERROR_FLAG_ACCOUNT_ID;
		}
		else
		{
			my %attr = ('type' => 'admin');
			init_account_db_list(\%attr, 0, 0);
			
			if((get_account_data($account_index_no, 'type') eq 'admin') && ($account_db->get_db_list_size() == 1))
			{
				# 管理者が1人のみ
				$result = ERROR_FLAG_ACCOUNT_ONLY_ADMIN_DELETE;
			}
		}
	}
	else
	{
		# ユーザーID指定エラー
		$result = ERROR_FLAG_ACCOUNT_ID;
	}
	
	return $result;
}

# アカウント登録
#  return    : 結果
#                bit 6 … 登録エラー
#                bit 7 … 保存エラー
sub regist_account_data
{
	my $result = 0;
	
	$current_account_data{'password'} = substr(crypt($current_account_data{'password'}, ${get_system_param_ref('crypt_salt')}), 2);
	delete $current_account_data{'repassword'};
	
	if($account_db->regist_hash(\%current_account_data) >= 0)
	{
		if($account_db->write() != 0)
		{
			# DB保存エラー
			$result = ERROR_FLAG_ACCOUNT_DB_SAVE;
		}
	}
	else
	{
		# DB登録エラー
		$result = ERROR_FLAG_ACCOUNT_DB_REGIST;
	}
	
	return $result;
}

# アカウント削除
#  param[in] : インデックス番号
#  return    : 結果
#                bit 6 … 保存エラー
sub delete_account_data
{
	my ($account_index_no) = @_;
	my $result = 0;
	
	if($account_db->delete_hash($account_index_no) < 0)
	{
		# DB登録エラー
		$result = ERROR_FLAG_ACCOUNT_DB_REGIST;
	}
	
	if($result == 0)
	{
		if($account_db->write() != 0)
		{
			# 保存エラー
			$result = ERROR_FLAG_ACCOUNT_DB_SAVE;
		}
	}
	
	return $result;
}

# 指定インデックスNo.のアカウントデータの指定パラメータ取得
#  param[in] : インデックスNo.（-1の場合はカレントデータ）
#  param[in] : パラメータ種別
#  return    : データ(''の場合は該当なし)
sub get_account_data
{
	my ($index_no, $param) = @_;
	
	if($index_no == -1)
	{
		if(exists($current_account_data{$param}))
		{
			return $current_account_data{$param};
		}
	}
	else
	{
		return $account_db->get_data($index_no, $param);
	}
	
	return '';
}


# 指定ユーザーIDのデータベースインデックスNo.取得
#  param[in] : ユーザーID
#  return    : データベースインデックスNo.(-1の場合は該当なし)
sub get_account_index
{
	my $search_id = $_[0];
	
	for(my $index_no = 0;$index_no < $account_db->get_hash_num();$index_no++)
	{
		if($search_id eq $account_db->get_data($index_no, 'id'))
		{
			return $index_no;
		}
	}
	
	return -1;
}

# アカウントDB用条件チェックルーチン
#  param[in] : チェック対象のハッシュリファレンス
#  param[in] : 条件アトリビュートリファレンス
#  return : チェック結果
sub condition_account
{
	my ($ref_hash, $ref_attr) = @_;
	my %check_func =
		(
		 'id'       => sub { return ($_[0] eq $_[1]); },
		 'name'     => sub { return ($_[0] eq $_[1]); },
		 'password' => sub { return ($_[0] eq $_[1]); },
		 'type'     => sub { return ($_[0] eq $_[1]); },
		);
	my @types = keys(%$ref_attr);
	
	return check_condition_db($ref_hash, $ref_attr, \%check_func, \@types);
}

# 指定条件に合致するアカウントデータベースリストを生成
#  param[in] : 指定条件アトリビュートリファレンス
#  param[in] : 行オフセット
#  param[in] : 行最大数（0の場合は制限無し）
sub init_account_db_list
{
	my ($ref_attr, $start, $max_num) = @_;
	
	$account_db->init_db_list(\&condition_account, $ref_attr, undef, undef, undef, $start, $max_num);
}

# アカウントデータベースリストからフェッチ
#  return    : アカウントハッシュリファレンス
sub fetch_account_db_list
{
	return $account_db->fetch_db_list_hashref();
}

# アカウントデータベースリストのサイズ取得
#  return    : リストの行数
sub get_account_db_list_size
{
	return $account_db->get_db_list_size();
}

# 指定IDからオフセットで指定された位置のユーザーIDを返す
#  param[in] : ユーザーID
#  param[in] : オフセット位置
#  return    : ユーザーID(''の場合は該当なし)
sub get_account_id
{
	my ($account_id, $offset) = @_;
	my $index_no = -1;
	my $ret_id   = '';
	
	if($account_id ne '')
	{
		$index_no = get_account_index($account_id);
	}
	
	if($index_no >= 0)
	{
		$index_no = $account_db->search_db_list($index_no, $offset);
		
		if($index_no >= 0)
		{
			$ret_id = get_account_data($index_no, 'id');
		}
	}
	
	return $ret_id;
}

# アカウント処理のエラーメッセージ文字列追加
#  param[in] : 結果
sub account_error_add_error_msg
{
	my $result = $_[0];
	
	if($result & ERROR_FLAG_ACCOUNT_ID)
	{
		add_error_msg(\$main::res_string{'account_id_error'});
	}
	
	if($result & ERROR_FLAG_ACCOUNT_ID_DUPLICATE)
	{
		add_error_msg(\$main::res_string{'account_id_duplicate_error'});
	}
	
	if($result & ERROR_FLAG_ACCOUNT_PASSWORD)
	{
		add_error_msg(\$main::res_string{'account_password_error'});
	}
	
	if($result & ERROR_FLAG_ACCOUNT_NAME)
	{
		add_error_msg(\$main::res_string{'account_name_error'});
	}
	
	if($result & ERROR_FLAG_ACCOUNT_NAME_DUPLICATE)
	{
		add_error_msg(\$main::res_string{'account_name_duplicate_error'});
	}
	
	if($result & ERROR_FLAG_ACCOUNT_TYPE)
	{
		add_error_msg(\$main::res_string{'account_type_error'});
	}
	
	if($result & ERROR_FLAG_ACCOUNT_DB_REGIST)
	{
		add_error_msg(\$main::res_string{'account_db_regist_error'});
	}
	
	if($result & ERROR_FLAG_ACCOUNT_DB_SAVE)
	{
		add_error_msg(\$main::res_string{'account_db_save_error'});
	}
	
	if($result & ERROR_FLAG_ACCOUNT_ONLY_ADMIN_CHANGE)
	{
		add_error_msg(\$main::res_string{'account_only_admin_change_error'});
	}
	
	if($result & ERROR_FLAG_ACCOUNT_ONLY_ADMIN_DELETE)
	{
		add_error_msg(\$main::res_string{'account_only_admin_delete_error'});
	}
}

1;
