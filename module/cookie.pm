######## Cookie ########

use strict;

our %settings;

use CGI::Cookie;

my %cookies;					# Cookie

my $login_cookie_name   = '';		# ログインCookie名
my $comment_cookie_name = '';		# コメントCookie名
my $temp_cookie_name    = '';		# テンポラリCookie名

# ログイン成功時Cookie文字列を生成
#  param[in]  : Prefix
sub init_cookie
{
	my $prefix = $_[0];
	
	%cookies = fetch CGI::Cookie;
	
	$login_cookie_name   = $prefix . '_login';		# ログインCookie名
	$comment_cookie_name = $prefix . '_comment';	# コメントCookie名
	$temp_cookie_name    = $prefix . '_temp';		# テンポラリCookie名
}

# ログイン成功時Cookie文字列を生成
#  param[in]  : ログインID
#  param[in]  : ログインパスワード
#  param[in]  : 自動ログイン
#  return     : Cookie文字列
sub make_cookie_login_success
{
	my ($id, $pass, $auto) = @_;
	my $keep_day = 0;
	
	if($auto eq 'enable')
	{
		$keep_day = $settings{'login_auto_keep_day'};
	}
	
	return make_cookie_login($id, $pass, $auto, $keep_day);
}

# ログインCookie文字列を生成
#  param[in]  : ログインID
#  param[in]  : ログインパスワード
#  param[in]  : 自動ログイン
#  param[in]  : Cookie保存日数（0の場合は有効期限設定なし、0未満であれば名前のみ）
#  return     : Cookie文字列
sub make_cookie_login
{
	my ($id, $pass, $auto, $save_days) = @_;
	my $cookie_str;
	
	if($save_days < 0)
	{
		$cookie_str = new CGI::Cookie(-name => $login_cookie_name, -value => { 'id' => '', 'pass' => '', 'auto' => '', });
	}
	elsif($save_days > 0)
	{
		my $expires_str = '+' . $save_days . 'd';
		
		$cookie_str = new CGI::Cookie(-name => $login_cookie_name, -value => { 'id' => $id, 'pass' => $pass, 'auto' => $auto }, -expires => $expires_str);
	}
	else
	{
		$cookie_str = new CGI::Cookie(-name => $login_cookie_name, -value => { 'id' => $id, 'pass' => $pass, 'auto' => $auto });
	}
	
	return "Set-Cookie: " . $cookie_str . "\r\n";
}

# ログインID／パスワードをCookieから取得
#  param[out] : ログインID
#  param[out] : ログインパスワード
#  param[out] : 自動ログイン
sub get_cookie_login
{
	my ($ref_id, $ref_pass, $ref_auto) = @_;
	
	if(exists($cookies{$login_cookie_name}))
	{
		my %value = @{$cookies{$login_cookie_name}{'value'}};
		
		if(exists($value{'id'}))
		{
			$$ref_id   = $value{'id'};
		}
		if(exists($value{'pass'}))
		{
			$$ref_pass = $value{'pass'};
		}
		if(exists($value{'auto'}))
		{
			$$ref_auto = $value{'auto'};
		}
	}
}

# コメントCookie文字列を生成
#  param[in]  : コメント投稿者名
#  param[in]  : コメント投稿者メールアドレス
#  param[in]  : Cookie保存日数（0の場合は有効期限設定なし、0未満であれば名前のみ）
#  return     : Cookie文字列
sub make_cookie_comment
{
	my ($name, $mail, $save_days) = @_;
	my $cookie_str;
	
	if($save_days < 0)
	{
		$cookie_str = new CGI::Cookie(-name => $comment_cookie_name);
	}
	elsif($save_days > 0)
	{
		my $expires_str = '+' . $save_days . 'd';
		
		$cookie_str = new CGI::Cookie(-name => $comment_cookie_name, -value => { 'name' => $name, 'mail' => $mail }, -expires => $expires_str);
	}
	else
	{
		$cookie_str = new CGI::Cookie(-name => $comment_cookie_name, -value => { 'name' => $name, 'mail' => $mail });
	}
	
	return "Set-Cookie: " . $cookie_str . "\r\n";
}

# コメントCookieからパラメータを取得
#  param[out] : コメント投稿者名
#  param[out] : コメント投稿者メールアドレス
sub get_cookie_comment
{
	my ($ref_name, $ref_mail) = @_;
	
	if(exists($cookies{$comment_cookie_name}))
	{
		my %value = @{$cookies{$comment_cookie_name}{'value'}};
		
		if(exists($value{'name'}))
		{
			$$ref_name = $value{'name'};
		}
		if(exists($value{'mail'}))
		{
			$$ref_mail = $value{'mail'};
		}
	}
}

# テンポラリCookie文字列を生成
#  param[in]  : テンポラリCookieのvalueとして保存するハッシュ
#  return     : Cookie文字列
sub make_cookie_temp
{
	my (%value) = @_;
	my $cookie_str = new CGI::Cookie(-name => $temp_cookie_name, -value => \%value);
	
	return "Set-Cookie: " . $cookie_str . "\r\n";
}

# テンポラリCookieからパラメータを取得
#  return : パラメータハッシュ
sub get_cookie_temp
{
	my %value = ();
	
	if((exists($cookies{$temp_cookie_name})) && (exists($cookies{$temp_cookie_name}{'value'})))
	{
		%value = @{$cookies{$temp_cookie_name}{'value'}};
	}
	
	return %value;
}

1;
