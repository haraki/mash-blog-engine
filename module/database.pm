######## データベース ########

use strict;

use Mash::MashDB;

# データベースのロック
#  retval  0 : 成功
#  retval -1 : 失敗
sub lock_db
{
	return Mash::MashDB::lock_db();
}

# データベースのロック解除
#  retval  0 : 成功
#  retval -1 : 失敗
sub unlock_db
{
	return Mash::MashDB::unlock_db();
}

# データベース読み込み（まだ存在していない場合は生成する）
#  param[in] : データベース
#  param[in] : フォーマットハッシュリファレンス
sub read_db
{
	my ($db, $ref_format) = @_;
	my $ret;
	
	$ret = $db->read();
	
	if($ret == -1)
	{
		$ret = $db->create($ref_format);
		
		if($ret == 0)
		{
			$ret = $db->read();
		}
	}
	
	return $ret;
}

# 条件に適合したハッシュかチェック
#  param[in] : チェック対象のハッシュリファレンス
#  param[in] : 条件アトリビュートリファレンス
#  param[in] : 比較式ハッシュリファレンス
#  param[in] : チェック対象のハッシュ種別リストリファレンス
#  return : チェック結果
sub check_condition_db
{
	my ($ref_hash, $ref_attr, $ref_check_func, $ref_types) = @_;
	
	if(defined($ref_types))
	{
		foreach my $type (@$ref_types)
		{
			if((exists($$ref_hash{$type})) && (exists($$ref_check_func{$type})))
			{
				my $value      = $$ref_hash{$type};
				my $ref_type   = ref($value);
				my $attr_param = $$ref_attr{$type};
				
				if($attr_param =~ /^!(.*)/)
				{
					$attr_param = $1;
					
					if($ref_type eq '')
					{
						if(($$ref_check_func{$type}($attr_param, $value)))
						{
							return 0;
						}
					}
					elsif($ref_type eq 'ARRAY')
					{
						my $chk = 0;
						
						foreach my $array_value (@$value)
						{
							if(!($$ref_check_func{$type}($attr_param, $array_value)))
							{
								$chk = 1;
								
								last;
							}
						}
						
						if($chk == 0)
						{
							return 0;
						}
					}
				}
				else
				{
					if($ref_type eq '')
					{
						if(!($$ref_check_func{$type}($attr_param, $value)))
						{
							return 0;
						}
					}
					elsif($ref_type eq 'ARRAY')
					{
						my $chk = 0;
						
						foreach my $array_value (@$value)
						{
							if($$ref_check_func{$type}($attr_param, $array_value))
							{
								$chk = 1;
								
								last;
							}
						}
						
						if($chk == 0)
						{
							return 0;
						}
					}
				}
			}
		}
	}
	
	return 1;
}

# 年月日・時刻ソート用比較ルーチン
#  param[in] : 比較対象のハッシュリファレンスa
#  param[in] : 比較対象のハッシュリファレンスb
#  return : 比較結果(-1 / 0 / 1)
sub sort_date_time
{
	my ($a, $b) = @_;
	my $ret;
	
	$ret = ($$a{'date'} cmp $$b{'date'});
	
	if($ret == 0)
	{
		$ret = ($$a{'time'} cmp $$b{'time'});
		
		if($ret == 0)
		{
			$ret = ($$a{'id'} <=> $$b{'id'});
		}
	}
	
	return $ret;
}

1;
