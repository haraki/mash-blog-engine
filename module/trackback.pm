#### トラックバック ####

use strict;

use Mash::MashDB;
use database;

my $trackback_db_file = 'trackback_db.cgi';
my $trackback_db;

my %trackback_db_format =
	(
	 'id'          => 'integer,primary',
	 'update_time' => 'integer',
	);

# トラックバックDB初期化
#  param[in] : DBルートディレクトリパス
#  return    : 結果
sub init_trackback_db
{
	my $db_root_path = $_[0];
	my $result = 0;
	
	$trackback_db = new Mash::MashDB($db_root_path . $trackback_db_file);
	
	if(read_db($trackback_db, \%trackback_db_format) != 0)
	{
		$result = -1;
	}
	
	return $result;
}

1;
