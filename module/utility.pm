######## ユーティリティ ########

use strict;

our %locale_settings;

######## ファイル ########

# ディレクトリを生成する
#  param[in] : ディレクトリパス
#  retval  0 : 成功
#  retval  1 : 指定されたディレクトリは既に存在している
#  retval -1 : 失敗
sub make_dir
{
	my ($dir_path) = @_;
	
	if(-d $dir_path)
	{
		return 1;
	}
	
	eval
	{
		mkpath($dir_path);		# File::Path::mkpath() はエラーが発生した場合は例外になるのでeval
	};
	
	if($@)						# 例外が発生したか確認（例外の内容が $@ に格納される）
	{
		return -1;
	}
	
	if(!(-d $dir_path))
	{
		return -1;
	}
	
	return 0;
}

######## 文字列 ######## 

# Content-type文字列生成
#  param[in] : パラメータハッシュ
sub make_content_type_str
{
	my (%value) = @_;
	my $return_str = "Content-type: text/html;";
	
	foreach my $key (keys(%value))
	{
		$return_str .= ' ' . $key . '=' . $value{$key};
	}
	
	return $return_str . "\r\n\r\n";
}

# Location文字列生成
#  param[in] : CGI
#  param[in] : パラメータハッシュ
sub make_location_str
{
	my ($this_cgi, %value) = @_;
	my $return_str = "Location: $this_cgi";
	my $link_chr = '?';
	
	foreach my $key (keys(%value))
	{
		$return_str .= $link_chr . $key . '=' . $value{$key};
		
		$link_chr = '&';
	}
	
	return $return_str . "\r\n\r\n";
}

# フォームから入力された文字列チェック
#  param[in] : 文字列
#  param[in] : 最小文字数(0の場合は未入力可)
#  param[in] : 最大文字数(0の場合は無制限)
#  return    : 結果
sub check_form_str
{
	my ($str, $min, $max) = @_;
	my $strlen = length($str);
	
	if(($min > 0) && ($strlen < $min))
	{
		return -1;
	}
	
	if(($max > 0) && ($strlen > $max))
	{
		return -2;
	}
	
	if(($strlen > 0) && (!($str =~ m/[^\s]/)))
	{
		return -3;
	}
	
	return 0;
}

# タグ文字列チェック
#  param[in] : タグ文字列
#  param[in] : 最小文字数(0の場合は未入力可)
#  param[in] : 最大文字数(0の場合は無制限)
#  return    : 結果
sub check_tag_str
{
	my ($tag_str, $min, $max) = @_;
	my $ret = check_form_str($tag_str, $min, $max);
	
	if($ret != 0)
	{
		return $ret;
	}
	
	return 0;
}

# メールアドレス文字列チェック
#  param[in] : メールアドレス文字列
#  param[in] : 最小文字数(0の場合は未入力可)
#  param[in] : 最大文字数(0の場合は無制限)
#  return    : 結果
sub check_mail_str
{
	my ($mail_str, $min, $max) = @_;
	my $ret = check_form_str($mail_str, $min, $max);
	
	if($ret != 0)
	{
		return $ret;
	}
	
	return 0;
}

# URL文字列チェック
#  param[in] : URL文字列
#  param[in] : 最小文字数(0の場合は未入力可)
#  param[in] : 最大文字数(0の場合は無制限)
#  return    : 結果
sub check_url_str
{
	my ($url_str, $min, $max) = @_;
	my $ret = check_form_str($url_str, $min, $max);
	
	if($ret != 0)
	{
		return $ret;
	}
	
	return 0;
}

# 年月日文字列チェック
#  param[in] : 年月日文字列
#  return    : 結果
sub check_date_str
{
	my $date_str = $_[0];
	my ($year, $month, $day) = split(/-/, $date_str, 3);
	my $day_max;
	
	if(($year !~ m/^[0-9]{4}$/) || ($year < 1))
	{
		return -1;
	}
	
	if((defined($month)) && ($month ne '') && (($month !~ m/^[0-9]{2}$/) || ($month < 1) || ($month > 12)))
	{
		return -2;
	}
	
	$day_max = Mash::MashTime::get_day_max($year, $month);
	
	if((defined($day)) && ($day ne '') && (($day !~ /^[0-9]{2}$/) || ($day < 1) || ($day > $day_max)))
	{
		return -3;
	}
	
	return 0;
}

# 時刻文字列チェック
#  param[in] : 時刻文字列
#  return    : 結果
sub check_time_str
{
	my $time_str = $_[0];
	my ($hour, $minute, $second) = split(/:/, $time_str, 3);
	
	if(($hour !~ m/^[0-9]{2}$/) || ($hour < 0) || ($hour > 23))
	{
		return -1;
	}
	
	if(($minute ne '') && (($minute !~ m/^[0-9]{2}$/) || ($minute < 0) || ($minute > 59)))
	{
		return -2;
	}
	
	if(($second ne '') && (($second !~ m/^[0-9]{2}$/) || ($second < 0) || ($second > 59)))
	{
		return -3;
	}
	
	return 0;
}

# ハッシュ内の文字列をエスケープ（エンティティ化）
#  param[in] : ハッシュリファレンス
sub escape_string_hash
{
	my ($ref_hash) = @_;
	
	foreach my $key (keys(%$ref_hash))
	{
		my $ref_str = ref($$ref_hash{$key});
		
		if($ref_str eq 'HASH')
		{
			escape_string_hash($$ref_hash{$key});
		}
		elsif($ref_str eq 'ARRAY')
		{
			escape_string_array($$ref_hash{$key});
		}
		else
		{
			escape_string(\$$ref_hash{$key});
		}
	}
}

# 配列内の文字列をエスケープ（エンティティ化）
#  param[in] : 配列リファレンス
sub escape_string_array
{
	my ($ref_array) = @_;
	
	for(my $index = 0;$index < @$ref_array;$index++)
	{
		my $ref_str = ref($$ref_array[$index]);
		
		if($ref_str eq 'HASH')
		{
			escape_string_hash($$ref_array[$index]);
		}
		elsif($ref_str eq 'ARRAY')
		{
			escape_string_array($$ref_array[$index]);
		}
		else
		{
			escape_string(\$$ref_array[$index]);
		}
	}
}

# 文字列をエスケープ（エンティティ化）
#  param[in] : 文字列リファレンス
sub escape_string
{
	my ($ref_str) = @_;
	
	$$ref_str =~ s/</&lt;/g;
	$$ref_str =~ s/>/&gt;/g;
}

# 文字列中のタグ無効化
#  param[in/out] : 文字列リファレンス
sub invalid_tag
{
	my ($ref_str) = @_;
	
	$$ref_str =~ s/\[/&#091;/g;
	$$ref_str =~ s/\]/&#093;/g;
}

# 年月日を指定フォーマットに変換
#  param[in]  : フォーマット文字列
#  param[in]  : 年
#  param[in]  : 月
#  param[in]  : 日
#  return     : 変換後文字列
sub ymd_2_format_str
{
	my ($format_str, $year, $month, $day) = @_;
	my $year_value  = 0;
	my $month_value = 0;
	my $day_value   = 0;
	my $wday_value  = 0;
	
	if(defined($year))
	{
		$year_value = $year + 0;
	}
	if($year_value <= 0)
	{
		$year       = "0001";
		$year_value = 1;
	}
	elsif($year_value < 1000)
	{
		$year = sprintf("%04d", $year_value);
	}
	
	if(defined($month))
	{
		$month_value = $month + 0;
	}
	if($month_value <= 0)
	{
		$month       = "01";
		$month_value = 1;
	}
	elsif($month_value < 10)
	{
		$month = '0' . $month_value;
	}
	
	if(defined($day))
	{
		$day_value = $day + 0;
	}
	if($day_value <= 0)
	{
		$day       = "01";
		$day_value = 1;
	}
	elsif($day_value < 10)
	{
		$day = '0' . $day_value;
	}
	
	$wday_value = Mash::MashTime::get_wday($year_value, $month_value, $day_value);
	
	$format_str =~ s/\{YYYY\}/$year/g;
	$year %= 100;
	if($year < 10)
	{
		$year = '0' . $year;
	}
	$format_str =~ s/\{YY\}/$year/g;
	
	$format_str =~ s/\{MMMM\}/$locale_settings{'month_table_l'}[$month_value - 1]/g;
	$format_str =~ s/\{MMM\}/$locale_settings{'month_table'}[$month_value - 1]/g;
	$format_str =~ s/\{MM\}/$month/g;
	$format_str =~ s/\{M\}/$month_value/g;
	
	$format_str =~ s/\{DD\}/$day/g;
	$format_str =~ s/\{D\}/$day_value/g;
	
	$format_str =~ s/\{WW\}/$locale_settings{'wday_table_l'}[$wday_value]/g;
	$format_str =~ s/\{W\}/$locale_settings{'wday_table'}[$wday_value]/g;
	
	return $format_str;
}

# 時分秒を指定フォーマットに変換
#  param[in]  : フォーマット文字列
#  param[in]  : 時
#  param[in]  : 分
#  param[in]  : 秒
#  return     : 変換後文字列
sub hmd_2_format_str
{
	my ($format_str, $hour, $minute, $second) = @_;
	my $hour_value   = 0;
	my $minute_value = 0;
	my $second_value = 0;
	my $hour12_value = 0;
	my $hour12;
	
	if(defined($hour))
	{
		$hour_value = $hour + 0;
	}
	if($hour_value < 0)
	{
		$hour       = "01";
		$hour_value = 1;
	}
	elsif($hour_value < 10)
	{
		$hour = '0' . $hour_value;
	}
	
	$hour12_value = $hour_value % 12;
	if($hour12_value < 10)
	{
		$hour12 = '0' . $hour12_value;
	}
	else
	{
		$hour12 = $hour12_value;
	}
	
	if(defined($minute))
	{
		$minute_value = $minute + 0;
	}
	if($minute_value < 0)
	{
		$minute       = "01";
		$minute_value = 1;
	}
	elsif($minute_value < 10)
	{
		$minute = '0' . $minute_value;
	}
	
	if(defined($second))
	{
		$second_value = $second + 0;
	}
	if($second_value < 0)
	{
		$second       = "01";
		$second_value = 1;
	}
	elsif($second_value < 10)
	{
		$second = '0' . $second_value;
	}
	
	$format_str =~ s/\{hhhh\}/$hour/g;
	$format_str =~ s/\{hhh\}/$hour_value/g;
	$format_str =~ s/\{hh\}/$hour12/g;
	$format_str =~ s/\{h\}/$hour12_value/g;
	
	$format_str =~ s/\{mm\}/$minute/g;
	$format_str =~ s/\{m\}/$minute_value/g;
	
	$format_str =~ s/\{ss\}/$second/g;
	$format_str =~ s/\{s\}/$second_value/g;
	
	$hour_value /= 12;
	$format_str =~ s/\{tt\}/$locale_settings{'ampm_table_l'}[$hour_value]/g;
	$format_str =~ s/\{t\}/$locale_settings{'ampm_table'}[$hour_value]/g;
	
	return $format_str;
}

######## アドレス ######## 

# 指定アドレスがアドレス群に含まれているか
#  param[in] : IPv4アドレス
#  param[in] : IPv4アドレス群リファレンス
#  return    : 含まれていない(0)/含まれている(1)
sub check_addr
{
	my ($addr, $ref_addrs) = @_;
	
	if((defined($addr)) && ($addr ne ''))
	{
		if(is_match_addr($addr, $ref_addrs))
		{
			return 1;
		}
	}
	
	return 0;
}

# 指定ホスト名がホスト名群に含まれているか
#  param[in] : ホスト名
#  param[in] : ホスト名群リファレンス
#  return    : 含まれていない(0)/含まれている(1)
sub check_host
{
	my ($host, $ref_hosts) = @_;
	
	if((defined($host)) && ($host ne ''))
	{
		if(is_match_host($host, $ref_hosts))
		{
			return 1;
		}
	}
	
	return 0;
}

# 指定アドレスがパターンにマッチするかチェック
#  param[in] : IPv4アドレス
#  param[in] : パターン群リファレンス
#  return    : マッチしない(0)/マッチする(1)
sub is_match_addr
{
	my ($addr, $ref_pattern) = @_;
	my $addr_int;
	
	if(ipv4_to_int32($addr, \$addr_int) < 0)
	{
		# アドレスではない
		return 0;
	}
	
	foreach my $pat_addr (@$ref_pattern)
	{
		my $pat_int;
		my $pat_mask;
		
		if(ipv4_to_int32($pat_addr, \$pat_int, \$pat_mask) == 0)
		{
			if(($addr_int & $pat_mask) == $pat_int)
			{
				return 1;
			}
		}
	}
	
	return 0;
}

# IPv4アドレス文字列を32bit値とマスクに変換
#  param[in]  : IPv4アドレス文字列(CIDRで指定した場合、/xxがマスクとなる)
#  param[out] : 32bit値
#  param[out] : マスク
#  return     : 成功(0)/失敗(-1)
sub ipv4_to_int32
{
	my ($addr_str, $ref_addr, $ref_mask) = @_;
	my ($ipv4_str, $mask_num) = split(/\//, $addr_str, 2);
	
	if(defined($ref_mask))
	{
		if((!(defined($mask_num))) || ($mask_num <= 0) || ($mask_num > 32))
		{
			$mask_num = 32;
		}
		
		my @mask_tbl = (0x00000000, 0x80000000, 0xC0000000, 0xE0000000, 
						0xF0000000, 0xF8000000, 0xFC000000, 0xFE000000,
						0xFF000000, 0xFF800000, 0xFFC00000, 0xFFE00000,
						0xFFF00000, 0xFFF80000, 0xFFFC0000, 0xFFFE0000,
						0xFFFF0000, 0xFFFF8000, 0xFFFFC000, 0xFFFFE000,
						0xFFFFF000, 0xFFFFF800, 0xFFFFFC00, 0xFFFFFE00,
						0xFFFFFF00, 0xFFFFFF80, 0xFFFFFFC0, 0xFFFFFFE0,
						0xFFFFFFF0, 0xFFFFFFF8, 0xFFFFFFFC, 0xFFFFFFFE,
						0xFFFFFFFF);
		
		$$ref_mask = $mask_tbl[$mask_num];
	}
	
	$$ref_addr = 0;
	foreach my $ip (split(/\./, $ipv4_str, 4))
	{
		if(!(defined($ip)) || ($ip < 0) || ($ip > 255))
		{
			return -1;
		}
		
		$$ref_addr <<= 8;
		$$ref_addr |= $ip;
	}
	
	return 0;
}

# 指定ホストがパターンにマッチするかチェック
#  param[in] : ホスト名
#  param[in] : パターン群リファレンス
#  return    : マッチしない(0)/マッチする(1)
sub is_match_host
{
	my ($host, $ref_pattern) = @_;
	
	foreach my $pat_host (@$ref_pattern)
	{
		$pat_host =~ s/\./\\\./g;
		$pat_host =~ s/\*/\.\*/g;
		$pat_host = '^' . $pat_host . '$';
		
		if($host =~ m/$pat_host/)
		{
			return 1;
		}
	}
	
	return 0;
}

# 指定文字列が文字列群に含まれているか
#  param[in] : 文字列
#  param[in] : 文字列群リファレンス
#  return    : 含まれていない(0)/含まれている(1)
sub check_str
{
	my ($str, $ref_strs) = @_;
	
	if((defined($str)) && ($str ne ''))
	{
		foreach my $s (@$ref_strs)
		{
			if($str eq $s)
			{
				return 1;
			}
		}
	}
	
	return 0;
}

1;
