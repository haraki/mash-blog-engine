######## テンプレート ########

use strict;

use Mash::MashTemplate;

# テンプレートファイル読み込みエラーメッセージ
my $template_err_msg = <<"EOL";
<html>
<head>
<meta http-equiv = "content-type" content = "text/html">
<title>Template Read Error!!</title>
</head>

<body>

<p>Template File read error.</p>
<p></p>
<p>テンプレートファイルの読み込みでエラーが発生しました...</p>

</body>
</html>
EOL

my $template;					# テンプレートエンジンのオブジェクト

my $template_dir = '';			# テンプレートファイルが置かれているディレクトリ
my $tag_module_dir = '';		# タグモジュールファイルが置かれているディレクトリ

our %mode_table;
	
sub init_template
{
	($template_dir, $tag_module_dir) = @_;
	
	$template = new Mash::MashTemplate('template');
	
	$template->load_tag($tag_module_dir);
}

# HTML文字列生成
#  param[in] : モード
#  return    : HTML文字列
sub make_html
{
	my ($mode)   = @_;
	my $html_str = read_template($mode);
	
	parse_tag('template', \$html_str);
	
	return $html_str;
}

# テンプレートファイル読み込み
#  param[in] : モード
#  return    : テンプレートファイルの内容
sub read_template
{
	my $temp_file = $template_dir . '/' . ${$mode_table{$_[0]}}{'template'};
	my $temp_str  = '';
	
	if(-e($temp_file))
	{
		if(open(FILE, $temp_file))
		{
			$temp_str = join('', <FILE>);
			close(FILE);
		}
		else
		{
			$temp_str = $template_err_msg;
		}
	}
	else
	{
		$temp_str = $template_err_msg;
	}
	
	return $temp_str;
}

# 文字列中のタグ解析
#  param[in]     : パーミッション
#  param[in/out] : 文字列リファレンス
sub parse_tag
{
	my ($perm, $ref_str) = @_;
	
	my $permission_ref = main::get_system_param_ref('permission');
	
	$$permission_ref = $perm;
	
	$template->parse_tag($perm, $ref_str);
}

# タグ名リスト取得
#  param[in]     : パーミッション
#  param[in/out] : リストリファレンス
#  param[in]     : ソート('char', 'rchar', 'num', 'rnum' or 'none'(default))
sub get_tag_name_list
{
	my ($permission, $ref_tag_name_list, $sort) = @_;
	
	$template->get_tag_name_list($permission, $ref_tag_name_list, $sort);
}

# タグのパラメータ取得
#  param[in] : タグ名
#  param[in] : パラメータ名
#  return    : パラメータ
sub get_tag_param
{
	my ($tag_name, $tag_param_name) = @_;
	
	return $template->get_tag_param($tag_name, $tag_param_name);
}

1;
