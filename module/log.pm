######## ログ ########

use strict;

use Mash::MashDB;
use Mash::MashTime;
use database;

use constant ERROR_FLAG_LOG_DB_REGIST => (1 <<  0);
use constant ERROR_FLAG_LOG_DB_SAVE   => (1 <<  1);

my $log_db_file = 'log_db';
my $log_db_ext  = '.cgi';
my $log_db;

my %log_db_format =
	(
	 'id'                    => 'integer,primary',
	 'date'                  => 'date',
	 'time'                  => 'time',
	 'log_mode'              => 'text',
	 'login_user'            => 'text',
	 'http_referer'          => 'text',
	 'http_user_agent'       => 'text',
	 'remote_host'           => 'text',
	 'remote_addr'           => 'text',
	 'request_method'        => 'text',
	 'query_string'          => 'text',
	 'http_via'              => 'text',
	 'http_forwarded'        => 'text',
	 'http_cache_info'       => 'text',
	 'http_x_forwarded_for'  => 'text',
	 'http_cache_control'    => 'text',
	 'http_proxy_connection' => 'text',
	 'http_from'             => 'text',
	);

#
# ログハッシュフォーマット
#
#    {'id'}                    ID
#    {'date'}                  アクセス年月日
#    {'time'}                  アクセス時刻
#    {'log_mode'}              モード
#    {'login_user'}            ログインユーザー
#    {'http_referer'}          HTTP_REFERER
#    {'http_user_agent'}       HTTP_USER_AGENT
#    {'remote_host'}           REMOTE_HOST
#    {'remote_addr'}           REMOTE_ADDR
#    {'request_method'}        REQUEST_METHOD
#    {'query_string'}          QUERY_STRING
#    {'http_via'}              HTTP_VIA
#    {'http_forwarded'}        HTTP_FORWARDED
#    {'http_cache_info'}       HTTP_CACHE_INFO
#    {'http_x_forwarded_for'}  HTTP_X_FORWARDED_FOR
#    {'http_cache_control'}    HTTP_CACHE_CONTROL
#    {'http_proxy_connection'} HTTP_PROXY_CONNECTION
#    {'http_from'}             HTTP_FROM
#
my %current_log_data =
	(
	 'id'                    => 0,
	 'date'                  => '',
	 'time'                  => '',
	 'log_mode'              => '',
	 'login_user'            => '',
	 'http_referer'          => '',
	 'http_user_agent'       => '',
	 'remote_host'           => '',
	 'remote_addr'           => '',
	 'request_method'        => '',
	 'query_string'          => '',
	 'http_via'              => '',
	 'http_forwarded'        => '',
	 'http_cache_info'       => '',
	 'http_x_forwarded_for'  => '',
	 'http_cache_control'    => '',
	 'http_proxy_connection' => '',
	 'http_from'             => '',
	);

# ログDB初期化
#  param[in] : DBルートディレクトリパス
#  param[in] : 日付
#  return    : 結果
sub init_log_db
{
	my ($db_root_path, $log_date) = @_;
	my $result = 0;
	
	if($log_date eq '')
	{
		Mash::MashTime::get_current_date_time(\$log_date);
	}
	
	my $log_db_filename = $log_db_file . '_' . $log_date . $log_db_ext;
	
	$log_db = new Mash::MashDB($db_root_path . $log_db_filename);
	
	if(read_db($log_db, \%log_db_format) != 0)
	{
		$result = -1;
	}
	
	return $result;
}

# カレントログデータのセット
#  param[in] : ログID
#  param[in] : アクセス年月日
#  param[in] : アクセス時刻
#  param[in] : モード
#  param[in] : ログインユーザー
#  param[in] : HTTP_REFERER
#  param[in] : HTTP_USER_AGENT
#  param[in] : REMOTE_HOST
#  param[in] : REMOTE_ADDR
#  param[in] : REQUEST_METHOD
#  param[in] : QUERY_STRING
#  param[in] : HTTP_VIA
#  param[in] : HTTP_FORWARDED
#  param[in] : HTTP_CACHE_INFO
#  param[in] : HTTP_X_FORWARDED_FOR
#  param[in] : HTTP_CACHE_CONTROL
#  param[in] : HTTP_PROXY_CONNECTION
#  param[in] : HTTP_FROM
sub set_current_log_data
{
	($current_log_data{'id'},
	 $current_log_data{'date'},
	 $current_log_data{'time'},
	 $current_log_data{'log_mode'},
	 $current_log_data{'login_user'},
	 $current_log_data{'http_referer'},
	 $current_log_data{'http_user_agent'},
	 $current_log_data{'remote_host'},
	 $current_log_data{'remote_addr'},
	 $current_log_data{'request_method'},
	 $current_log_data{'query_string'},
	 $current_log_data{'http_via'},
	 $current_log_data{'http_forwarded'},
	 $current_log_data{'http_cache_info'},
	 $current_log_data{'http_x_forwarded_for'},
	 $current_log_data{'http_cache_control'},
	 $current_log_data{'http_proxy_connection'},
	 $current_log_data{'http_from'}) = @_;
	
	escape_string_hash(\%current_log_data);
}

# ログ登録
#  param[out] : 登録されたログのID
#  return     : 結果
#                 bit  0 … 登録エラー
#                 bit  1 … 保存エラー
sub regist_log_data
{
	my $log_id_ref = $_[0];
	my $result = 0;
	
	if($current_log_data{'id'} == -1)
	{
		$current_log_data{'id'} = get_new_log_id();
	}
	
	if($log_db->regist_hash(\%current_log_data) >= 0)
	{
		if($log_db->write() != 0)
		{
			# DB保存エラー
			$result = ERROR_FLAG_LOG_DB_REGIST;
		}
	}
	else
	{
		# DB登録エラー
		$result = ERROR_FLAG_LOG_DB_SAVE;
	}
	
	if($result == 0)
	{
		$$log_id_ref = $current_log_data{'id'};
	}
	
	return $result;
}

# 新規ログID取得
#  return    : コメントID
sub get_new_log_id
{
	return $log_db->get_hash_num() + 1;
}

# 指定インデックスNo.のログデータの指定パラメータ取得
#  param[in] : インデックスNo.
#  param[in] : パラメータ種別
#  return    : データ(''の場合は該当なし)
sub get_log_data
{
	my ($index_no, $param) = @_;
	
	if($index_no == -1)
	{
		if(exists($current_log_data{$param}))
		{
			return $current_log_data{$param};
		}
	}
	else
	{
		return $log_db->get_data($index_no, $param);
	}
	
	return '';
}

# 指定ログIDのデータベースインデックスNo.取得
#  param[in] : ログID
#  return    : データベースインデックスNo.(-1の場合は該当なし)
sub get_log_index
{
	my $search_id = $_[0];
	
	for(my $index_no = 0;$index_no < $log_db->get_hash_num();$index_no++)
	{
		if($search_id == $log_db->get_data($index_no, 'id'))
		{
			return $index_no;
		}
	}
	
	return -1;
}

# ログDB用条件チェックルーチン
#  param[in] : チェック対象のハッシュリファレンス
#  param[in] : 条件アトリビュートリファレンス
#  return : チェック結果
sub condition_log
{
	my ($ref_hash, $ref_attr) = @_;
	my %check_func =
		(
		 'id'                    => sub { return ($_[0] == $_[1]); },
		 'date'                  => sub { return (index($_[1], $_[0]) == 0); },
		 'time'                  => sub { return (index($_[1], $_[0]) == 0); },
		 'log_mode'              => sub { return ($_[0] =~ m/$_[1]/); },
		 'login_user'            => sub { return ($_[0] =~ m/$_[1]/); },
		 'http_referer'          => sub { return ($_[0] =~ m/$_[1]/); },
		 'http_user_agent'       => sub { return ($_[0] =~ m/$_[1]/); },
		 'remote_host'           => sub { return ($_[0] =~ m/$_[1]/); },
		 'remote_addr'           => sub { return ($_[0] =~ m/$_[1]/); },
		 'request_method'        => sub { return ($_[0] =~ m/$_[1]/); },
		 'query_string'          => sub { return ($_[0] =~ m/$_[1]/); },
		 'http_via'              => sub { return ($_[0] =~ m/$_[1]/); },
		 'http_forwarded'        => sub { return ($_[0] =~ m/$_[1]/); },
		 'http_cache_info'       => sub { return ($_[0] =~ m/$_[1]/); },
		 'http_x_forwarded_for'  => sub { return ($_[0] =~ m/$_[1]/); },
		 'http_cache_control'    => sub { return ($_[0] =~ m/$_[1]/); },
		 'http_proxy_connection' => sub { return ($_[0] =~ m/$_[1]/); },
		 'http_from'             => sub { return ($_[0] =~ m/$_[1]/); },
		);
	my @types = keys(%$ref_attr);
	
	return check_condition_db($ref_hash, $ref_attr, \%check_func, \@types);
}

# 指定条件に合致するログデータベースリストを生成
#  param[in] : 指定条件アトリビュートリファレンス
#  param[in] : 行オフセット
#  param[in] : 行最大数（0の場合は制限無し）
sub init_log_db_list
{
	my ($ref_attr, $start, $max_num) = @_;
	
	$log_db->init_db_list(\&condition_log, $ref_attr, undef, \&sort_date_time, 1, $start, $max_num);
}

# ログデータベースリストをプッシュ
sub push_log_db_list
{
	$log_db->push_db_list();
}

# ログデータベースリストをポップ
sub pop_log_db_list
{
	$log_db->pop_db_list();
}

# ログデータベースリストからフェッチ
sub fetch_log_db_list
{
	return $log_db->fetch_db_list_hashref();
}

# ログデータベースリストのサイズ取得
sub get_log_db_list_size
{
	return $log_db->get_db_list_size();
}

# 指定IDからオフセットで指定された位置のログIDを返す
#  param[in] : ログID
#  param[in] : オフセット位置
#  return    : ログID(''の場合は該当なし)
sub get_log_id
{
	my ($log_id, $offset) = @_;
	my $index_no = -1;
	my $ret_id   = '';
	
	if($log_id ne '')
	{
		$index_no = get_log_index($log_id);
	}
	
	if($index_no >= 0)
	{
		$index_no = $log_db->search_db_list($index_no, $offset);
		
		if($index_no >= 0)
		{
			$ret_id = get_log_data($index_no, 'id');
		}
	}
	
	return $ret_id;
}

# ログ処理のエラーメッセージ文字列追加
#  param[in] : 結果
sub log_error_add_error_msg
{
	my $result = $_[0];
	
	if($result & ERROR_FLAG_LOG_DB_REGIST)
	{
		add_error_msg(\$main::res_string{'log_db_regist_error'});
	}
	
	if($result & ERROR_FLAG_LOG_DB_SAVE)
	{
		add_error_msg(\$main::res_string{'log_db_save_error'});
	}
}

# ログDB日付一覧取得
#  param[in]     : DBルートディレクトリパス
#  param[in/out] : リストリファレンス
sub get_log_db_date_list
{
	my ($db_root_path, $ref_log_db_date_list) = @_;
	
	my @log_db_file_list = glob($db_root_path . $log_db_file . "*" . $log_db_ext);
	
	@$ref_log_db_date_list = ();
	foreach my $log_db_file (@log_db_file_list)
	{
		if($log_db_file =~ m/(\d{4}-\d{2}-\d{2})/)
		{
			push(@$ref_log_db_date_list, $1);
		}
	}
}

1;
