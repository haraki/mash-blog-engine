#### カテゴリ ####

use strict;

use Mash::MashDB;
use database;

use constant ERROR_FLAG_CATEGORY_NOINPUT   => (1 <<  0);
use constant ERROR_FLAG_CATEGORY_DUPLICATE => (1 <<  1);
use constant ERROR_FLAG_CATEGORY_NOT_FOUND => (1 <<  2);
use constant ERROR_FLAG_CATEGORY_DB_REGIST => (1 <<  3);
use constant ERROR_FLAG_CATEGORY_DB_DELETE => (1 <<  4);
use constant ERROR_FLAG_CATEGORY_DB_SAVE   => (1 <<  5);

my $category_db_file = 'category_db.cgi';
my $category_db;

my %category_db_format =
	(
	 'id'          => 'integer,primary',
	 'name'        => 'text',
	 'parent_id'   => 'integer',
	 'depth'       => 'integer',
	 'update_time' => 'integer',
	);

#
# カテゴリハッシュフォーマット
#
#    {'id'}          カテゴリID
#    {'name'}        カテゴリ名
#    {'parent_id'}   親カテゴリID
#    {'depth'}       階層
#    {'update_time'} 変更時刻
#

my %current_category_data =
	(
	 'id'          => -1,
	 'name'        => '',
	 'parent_id'   => -1,
	 'depth'       => 0,
	 'update_time' => 0,
	);

# カテゴリDB初期化
#  param[in] : DBルートディレクトリパス
#  return    : 結果
sub init_category_db
{
	my $db_root_path = $_[0];
	my $result = 0;
	
	$category_db = new Mash::MashDB($db_root_path . $category_db_file);
	
	if(read_db($category_db, \%category_db_format) != 0)
	{
		$result = -1;
	}
	
	return $result;
}

# カレントカテゴリデータのセット
#  param[in] : カテゴリID
#  param[in] : カテゴリ名
#  param[in] : 親カテゴリID
#  param[in] : 階層
#  param[in] : 変更時刻
sub set_current_category_data
{
	($current_category_data{'id'},
	 $current_category_data{'name'},
	 $current_category_data{'parent_id'},
	 $current_category_data{'depth'},
	 $current_category_data{'update_time'}) = @_;
	
	escape_string_hash(\%current_category_data);
}

sub regist_category_data
{
	my $result = 0;
	
	if($current_category_data{'name'} ne '')
	{
		if(get_category_index_from_name($current_category_data{'name'}) == -1)
		{
			my $parent_index_no = get_category_index($current_category_data{'parent_id'});
			
			if($parent_index_no >= 0)
			{
				$current_category_data{'depth'} = get_category_data($parent_index_no, 'depth') + 1;
			}
			
			$current_category_data{'id'} = $category_db->get_hash_num() + 1;
			
			if($category_db->insert_hash(\%current_category_data) >= 0)
			{
				if($category_db->write() == 0)
				{
					# 成功
					$result = 0;
				}
				else
				{
					# DB保存エラー
					$result = ERROR_FLAG_CATEGORY_DB_SAVE;
				}
			}
			else
			{
				# DB登録エラー
				$result = ERROR_FLAG_CATEGORY_DB_REGIST;
			}
		}
		else
		{
			# カテゴリ名重複エラー
			$result = ERROR_FLAG_CATEGORY_DUPLICATE;
		}
	}
	else
	{
		# カテゴリ名未入力エラー
		$result = ERROR_FLAG_CATEGORY_NOINPUT;
	}
	
	return $result;
}

sub rename_category_data
{
	my ($category_id, $rename_name) = @_;
	my $result = 0;
	
	if($category_id ne '')
	{
		my $category_index_no = get_category_index($category_id);
		
		if($category_index_no >= 0)
		{
			if($rename_name ne '')
			{
				if(get_category_index_from_name($rename_name) == -1)
				{
					my %new_name_hash = ('id' => $category_id, 'name' => $rename_name);
					
					if($category_db->update_hash(\%new_name_hash) >= 0)
					{
						if($category_db->write() == 0)
						{
							# 成功
							$result = 0;
						}
						else
						{
							# DB保存エラー
							$result = ERROR_FLAG_CATEGORY_DB_SAVE;
						}
					}
					else
					{
						# DB登録エラー
						$result = ERROR_FLAG_CATEGORY_DB_REGIST;
					}
				}
				else
				{
					# カテゴリ名重複エラー
					$result = ERROR_FLAG_CATEGORY_DUPLICATE;
				}
			}
			else
			{
				# カテゴリ名未入力エラー
				$result = ERROR_FLAG_CATEGORY_NOINPUT;
			}
		}
		else
		{
			# カテゴリ指定エラー
			$result = ERROR_FLAG_CATEGORY_NOT_FOUND;
		}
	}
	else
	{
		# カテゴリ名未入力エラー
		$result = ERROR_FLAG_CATEGORY_NOINPUT;
	}
	
	return $result;
}

sub delete_category_data
{
	my ($category_id, $sub_delete_enable) = @_;
	my $result = 0;
	
	if($category_id ne '')
	{
		my $category_index_no = get_category_index($category_id);
		
		if($category_index_no >= 0)
		{
			my $delete_name = $category_db->get_data($category_index_no, 'name');
			
			if($delete_name ne '')
			{
				my $del_result = 0;
				
				if($sub_delete_enable eq 'enable')
				{
					$del_result = delete_all_category($category_id);
				}
				else
				{
					my %new_name_hash = ('id' => $category_id, 'name' => '');
					
					$del_result = $category_db->update_hash(\%new_name_hash);
				}
				
				if($del_result >= 0)
				{
					if($category_db->write() == 0)
					{
						# 成功
						$result = 0;
					}
					else
					{
						# DB保存エラー
						$result = ERROR_FLAG_CATEGORY_DB_SAVE;
					}
				}
				else
				{
					# 削除エラー
					$result = ERROR_FLAG_CATEGORY_DB_DELETE;
				}
			}
			else
			{
				# カテゴリ指定エラー
				$result = ERROR_FLAG_CATEGORY_NOT_FOUND;
			}
		}
		else
		{
			# カテゴリ指定エラー
			$result = ERROR_FLAG_CATEGORY_NOT_FOUND;
		}
	}
	else
	{
		# カテゴリ名未入力エラー
		$result = ERROR_FLAG_CATEGORY_NOINPUT;
	}
	
	return $result;
}

# 指定カテゴリID及びサブカテゴリ全て削除
#  param[in] : カテゴリID
sub delete_all_category
{
	my ($category_id) = @_;
	
	if($category_id ne '')
	{
		# 名前を空欄にすることで削除とみなす
		my %new_name_hash = ('id' => $category_id, 'name' => '');
		
		if($category_db->update_hash(\%new_name_hash) >= 0)
		{
			my %attr = ('parent_id' => $category_id);
			
			init_category_db_list(\%attr, 0, 0);
			
			foreach my $sub_cateogry_index (@{$category_db->{'index_list'}})
			{
				my @index_list_bak  = @{$category_db->{'index_list'}};
				my $sub_category_id = $category_db->get_data($sub_cateogry_index, 'id');
				
				delete_all_category($sub_category_id);
				
				@{$category_db->{'index_list'}} = @index_list_bak;
			}
		}
		else
		{
			return -1;
		}
	}
	else
	{
		return -1;
	}
	
	return 0;
}

# 新規カテゴリID取得
#  return    : カテゴリID
sub get_new_category_id
{
	return $category_db->get_hash_num() + 1;
}

# 指定インデックスNo.のカテゴリデータの指定パラメータ取得
#  param[in] : インデックスNo.
#  param[in] : パラメータ種別
#  return    : データ(''の場合は該当なし)
sub get_category_data
{
	my ($index_no, $param) = @_;
	
	return $category_db->get_data($index_no, $param);
}

# 指定カテゴリIDのデータベースインデックスNo.取得
#  param[in] : カテゴリID
#  return    : データベースインデックスNo.(-1の場合は該当なし)
sub get_category_index
{
	my $search_id = $_[0];
	
	for(my $index_no = 0;$index_no < $category_db->get_hash_num();$index_no++)
	{
		if($search_id eq $category_db->get_data($index_no, 'id'))
		{
			return $index_no;
		}
	}
	
	return -1;
}

# 指定カテゴリ名のデータベースインデックスNo.取得
#  param[in] : カテゴリ名
#  return    : データベースインデックスNo.(-1の場合は該当なし)
sub get_category_index_from_name
{
	my $search_name = $_[0];
	
	for(my $index_no = 0;$index_no < $category_db->get_hash_num();$index_no++)
	{
		if($search_name eq $category_db->get_data($index_no, 'name'))
		{
			return $index_no;
		}
	}
	
	return -1;
}

# 指定カテゴリが指定親カテゴリと同一か、あるいはその子カテゴリか確認
#  param[in] : カテゴリID
#  param[in] : 親カテゴリID
#  return    : 結果
sub is_child_category
{
	my ($category_id, $parent_id) = @_;
	
	do
	{
		if($parent_id == $category_id)
		{
			return 1;
		}
		
		my $index_no = get_category_index($category_id);
		
		$category_id = $category_db->get_data($index_no, "parent_id");
	}while($category_id != -1);
	
	return 0;
}

# カテゴリDB用条件チェックルーチン
#  param[in] : チェック対象のハッシュリファレンス
#  param[in] : 条件アトリビュートリファレンス
#  return : チェック結果
sub condition_category
{
	my ($ref_hash, $ref_attr) = @_;
	my %check_func =
		(
		 'id'        => sub { return ($_[0] == $_[1]); },
		 'name'      => sub { return ($_[0] eq $_[1]); },
		 'parent_id' => sub { return ($_[0] == $_[1]); },
		 'depth'     => sub { return ($_[0] eq $_[1]); },
		);
	my @types = keys(%$ref_attr);
	
	return check_condition_db($ref_hash, $ref_attr, \%check_func, \@types);
}

# 指定条件に合致するカテゴリデータベースリストを生成
#  param[in] : 指定条件アトリビュートリファレンス
#  param[in] : 行オフセット
#  param[in] : 行最大数（0の場合は制限無し）
sub init_category_db_list
{
	my ($ref_attr, $start, $max_num) = @_;
	
	$category_db->init_db_list(\&condition_category, $ref_attr, undef, undef, undef, $start, $max_num);
}

# カテゴリデータベースリストをプッシュ
sub push_category_db_list
{
	$category_db->push_db_list();
}

# カテゴリデータベースリストをポップ
sub pop_category_db_list
{
	$category_db->pop_db_list();
}

# カテゴリデータベースリストからフェッチ
sub fetch_category_db_list
{
	return $category_db->fetch_db_list_hashref();
}

# カテゴリデータベースリストのサイズ取得
sub get_category_db_list_size
{
	return $category_db->get_db_list_size();
}

# 指定IDからオフセットで指定された位置のカテゴリIDを返す
#  param[in] : カテゴリID
#  param[in] : オフセット位置
#  return    : カテゴリID(''の場合は該当なし)
sub get_category_id
{
	my ($category_id, $offset) = @_;
	my $index_no = -1;
	my $ret_id   = '';
	
	if($category_id ne '')
	{
		$index_no = get_category_index($category_id);
	}
	
	if($index_no >= 0)
	{
		$index_no = $category_db->search_db_list($index_no, $offset);
		
		if($index_no >= 0)
		{
			$ret_id = get_category_data($index_no, 'id');
		}
	}
	
	return $ret_id;
}

# カテゴリ処理のエラーメッセージ文字列追加
#  param[in] : 結果
sub category_error_add_error_msg
{
	my $result = $_[0];
	
	if($result & ERROR_FLAG_CATEGORY_NOINPUT)
	{
		# カテゴリ名未入力エラー
		add_error_msg(\$main::res_string{'category_noinput_error'});
	}
	
	if($result & ERROR_FLAG_CATEGORY_DUPLICATE)
	{
		# カテゴリ名重複エラー
		add_error_msg(\$main::res_string{'category_duplicate_error'});
	}
	
	if($result & ERROR_FLAG_CATEGORY_NOT_FOUND)
	{
		# カテゴリ指定エラー
		add_error_msg(\$main::res_string{'category_notfound_error'});
	}
	
	if($result & ERROR_FLAG_CATEGORY_DB_REGIST)
	{
		# DB登録エラー
		add_error_msg(\$main::res_string{'category_db_regist_error'});
	}
	
	if($result & ERROR_FLAG_CATEGORY_DB_DELETE)
	{
		# DB削除エラー
		add_error_msg(\$main::res_string{'category_db_delete_error'});
	}
	
	if($result & ERROR_FLAG_CATEGORY_DB_SAVE)
	{
		# DB保存エラー
		add_error_msg(\$main::res_string{'category_db_save_error'});
	}
}

1;
