#### ファイル ####

use strict;

use Mash::MashDB;
use database;

use constant ERROR_FLAG_FILE_ID        => (1 <<  0);		# ファイルIDエラー
use constant ERROR_FLAG_FILE_HANDLE    => (1 <<  1);		# ファイルハンドルエラー
use constant ERROR_FLAG_FILE_NAME      => (1 <<  2);		# ファイル名エラー
use constant ERROR_FLAG_FILE_SIZE      => (1 <<  3);		# ファイルサイズエラー
use constant ERROR_FLAG_FILE_COMMENT   => (1 <<  4);		# ファイルコメントエラー
use constant ERROR_FLAG_FILE_OVERLAP   => (1 <<  5);		# ファイル重複エラー
use constant ERROR_FLAG_FILE_MAKE_DIR  => (1 <<  6);		# ファイルディレクトリ生成エラー
use constant ERROR_FLAG_FILE_SAVE      => (1 <<  7);		# ファイル保存エラー
use constant ERROR_FLAG_FILE_DELETE    => (1 <<  8);		# ファイル削除エラー
use constant ERROR_FLAG_FILE_DB_SAVE   => (1 <<  9);		# ファイルDB保存エラー
use constant ERROR_FLAG_FILE_DB_REGIST => (1 << 10);		# ファイルDB登録エラー

my $file_db_file = 'file_db.cgi';
my $file_db;

my %file_db_format =
	(
	 'id'          => 'text,primary',
	 'enable'      => 'boolean',
	 'path'        => 'text',
	 'user_id'     => 'text',
	 'comment'     => 'text',
	 'update_time' => 'integer',
	);

#
# ファイルハッシュフォーマット
#
#    {'id'}          ファイルID
#    {'enable'}      有効／無効       ('enable' or 'disable')
#    {'path'}        ファイル保存パス
#    {'user_id'}     ユーザーID       (0-9, a-z, -, _)
#    {'comment'}     コメント
#    {'update_time'} 更新時間
#
my %current_file_data =
	(
	 'id'             => '',
	 'enable'         => '',
	 'path'           => '',
	 'user_id'        => '',
	 'comment'        => '',
	 'update_time' => 0,
	);


# ファイルDB初期化
#  param[in] : DBルートディレクトリパス
#  return    : 結果
sub init_file_db
{
	my $db_root_path = $_[0];
	my $result = 0;
	
	$file_db = new Mash::MashDB($db_root_path . $file_db_file);
	
	if(read_db($file_db, \%file_db_format) != 0)
	{
		$result = -1;
	}
	
	return $result;
}

# カレントファイルデータのセット
#  param[in] : ファイルID
#  param[in] : 有効／無効       ('enable' or 'disable')
#  param[in] : ファイル保存パス
#  param[in] : ユーザーID       (0-9, a-z, -, _)
#  param[in] : コメント
#  param[in] : 更新時間
sub set_current_file_data
{
	($current_file_data{'id'}, $current_file_data{'enable'}, $current_file_data{'path'}, $current_file_data{'user_id'}, $current_file_data{'comment'}, $current_file_data{'update_time'}) = @_;
}

sub regist_file_data
{
	my ($file_handle, $file_name, $comment) = @_;
	my $result = 0;
	
	if(!(defined($file_handle)))
	{
		$result = ERROR_FLAG_FILE_HANDLE;
	}
	
	if(($file_name eq '') || (length($file_name) > ${get_system_param_ref('file_name_max')}))
	{
		# ファイル名エラー
		$result |= ERROR_FLAG_FILE_NAME;
	}
	
	if($result == 0)
	{
		my $file_size = (stat($file_handle))[7];
#		my $file_info = $cgi_query->uploadInfo($file_handle);
#		my $file_mime = $file_info->{'Content-Type'};
		
		if((${get_system_param_ref('file_size_max')} > 0) && ($file_size > ${get_system_param_ref('file_size_max')}))
		{
			# ファイルサイズエラー
			$result |= ERROR_FLAG_FILE_SIZE;
		}
		
		if((length($comment) < ${get_system_param_ref('file_comment_min')}) ||
		   ((${get_system_param_ref('file_comment_max')} > 0) && (length($comment) > ${get_system_param_ref('file_comment_max')})))
		{
			# コメントエラー
			$result |= ERROR_FLAG_FILE_COMMENT;
		}
		
		if($result == 0)
		{
			my ($mday, $mon, $year) = (localtime())[3..5];
			my $dir_path       = sprintf("%.4d/%.2d/%.2d/", $year + 1900, $mon + 1, $mday);
			my $file_path      = $dir_path . $file_name;
			my $file_full_path = ${get_system_param_ref('file_root_path')} . $file_path;
			
			$dir_path = ${get_system_param_ref('file_root_path')} . $dir_path;
			
			if(open(OUT, "$file_full_path"))
			{
				# ファイル重複エラー
				$result = ERROR_FLAG_FILE_OVERLAP;
				
				close(OUT);
			}
			elsif(make_dir($dir_path) < 0)
			{
				# ディレクトリ生成エラー
				$result = ERROR_FLAG_FILE_MAKE_DIR;
			}
			elsif(!open(OUT, ">$file_full_path"))
			{
				# ファイル保存エラー
				$result = ERROR_FLAG_FILE_SAVE;
			}
			else
			{
				# ファイルに保存
				my $buffer;
				
				binmode(OUT);
				
				while(read($file_handle, $buffer, 1024))
				{
					print OUT $buffer;
				}
				
				close(OUT);
				
				# ファイル情報をDBに保存
				my $new_id        = $file_db->get_hash_num() + 1;
				my $update_time   = time();
				my %new_data_hash = ('id' => $new_id, 'enable' => 'true', 'path' => $file_path, 'user_id' => ${get_system_param_ref('login_user_id')}, 'comment' => $comment, 'update_time' => $update_time);
				
				if($file_db->insert_hash(\%new_data_hash) >= 0)
				{
					if($file_db->write() == 0)
					{
						# 成功
						$result = 0;
					}
					else
					{
						# DB保存エラー
						$result = ERROR_FLAG_FILE_DB_SAVE;
					}
				}
				else
				{
					# DB登録エラー
					$result = ERROR_FLAG_FILE_DB_REGIST;
				}
			}
		}
	}
	
	return $result;
}

sub update_file_data
{
	my ($file_id, $comment) = @_;
	my $result = 0;
	
	if(get_file_index($file_id) == -1)
	{
		# IDエラー
		$result |= ERROR_FLAG_FILE_ID;
	}
	
	if((!defined($comment)) ||
	   (length($comment) < ${get_system_param_ref('file_comment_min')}) ||
	   ((${get_system_param_ref('file_comment_max')} > 0) && (length($comment) > ${get_system_param_ref('file_comment_max')})))
	{
		# コメントエラー
		$result |= ERROR_FLAG_FILE_COMMENT;
	}
	
	if($result == 0)
	{
		# ファイル情報をDBに保存
		my $update_time   = time();
		my %new_data_hash = ('id' => $file_id, 'comment' => $comment, 'update_time' => $update_time);
		
		if($file_db->update_hash(\%new_data_hash) >= 0)
		{
			if($file_db->write() == 0)
			{
				# 成功
				$result = 0;
			}
			else
			{
				# DB保存エラー
				$result = ERROR_FLAG_FILE_DB_SAVE;
			}
		}
		else
		{
			# DB登録エラー
			$result = ERROR_FLAG_FILE_DB_REGIST;
		}
	}
	
	return $result;
}

sub delete_file_data
{
	my ($file_id) = @_;
	my $result = 0;
	my $index_no = get_file_index($file_id);
	
	if($index_no == -1)
	{
		# IDエラー
		$result |= ERROR_FLAG_FILE_ID;
	}
	
	if($result == 0)
	{
		my $file_full_path = ${get_system_param_ref('file_root_path')} . get_file_data($index_no, 'path');
		
		if(unlink($file_full_path) == 0)
		{
			# ファイル削除エラー
			$result |= ERROR_FLAG_FILE_DELETE;
		}
		
		# ファイル情報をDBに保存
		my $update_time   = time();
		my %new_data_hash = ('id' => $file_id, 'enable' => 'false');
		
		if($file_db->update_hash(\%new_data_hash) >= 0)
		{
			if($file_db->write() == 0)
			{
				# 成功
				$result = 0;
			}
			else
			{
				# DB保存エラー
				$result = ERROR_FLAG_FILE_DB_SAVE;
			}
		}
		else
		{
			# DB登録エラー
			$result = ERROR_FLAG_FILE_DB_REGIST;
		}
	}
	
	return $result;
}

# 指定インデックスNo.のファイルデータの指定パラメータ取得
#  param[in] : インデックスNo.
#  param[in] : パラメータ種別
#  return    : データ(''の場合は該当なし)
sub get_file_data
{
	my ($index_no, $param) = @_;
	
	return $file_db->get_data($index_no, $param);
}

# 指定ファイルIDのデータベースインデックスNo.取得
#  param[in] : ファイルID
#  return    : データベースインデックスNo.(-1の場合は該当なし)
sub get_file_index
{
	my $search_id = $_[0];
	
	for(my $index_no = 0;$index_no < $file_db->get_hash_num();$index_no++)
	{
		if($search_id eq $file_db->get_data($index_no, 'id'))
		{
			return $index_no;
		}
	}
	
	return -1;
}

# ファイルDB用条件チェックルーチン
#  param[in] : チェック対象のハッシュリファレンス
#  param[in] : 条件アトリビュートリファレンス
#  return : チェック結果
sub condition_file
{
	my ($ref_hash, $ref_attr) = @_;
	my %check_func =
		(
		 'id'          => sub { return ($_[0] eq $_[1]); },
		 'enable'      => sub { return ($_[0] eq $_[1]); },
		 'path'        => sub { return ($_[0] eq $_[1]); },
		 'user_id'     => sub { return ($_[0] eq $_[1]); },
		 'comment'     => sub { return ($_[0] eq $_[1]); },
		 'update_time' => sub { return ($_[0] eq $_[1]); },
		);
	my @types = keys(%$ref_attr);
	
	return check_condition_db($ref_hash, $ref_attr, \%check_func, \@types);
}

# 指定条件に合致するファイルデータベースリストを生成
#  param[in] : 指定条件アトリビュートリファレンス
#  param[in] : 行オフセット
#  param[in] : 行最大数（0の場合は制限無し）
sub init_file_db_list
{
	my ($ref_attr, $start, $max_num) = @_;
	
	$file_db->init_db_list(\&condition_file, $ref_attr, undef, undef, undef, $start, $max_num);
}

# ファイルデータベースリストをプッシュ
sub push_file_db_list
{
	$file_db->push_db_list();
}

# ファイルデータベースリストをポップ
sub pop_file_db_list
{
	$file_db->pop_db_list();
}

# ファイルデータベースリストからフェッチ
sub fetch_file_db_list
{
	return $file_db->fetch_db_list_hashref();
}

# ファイルデータベースリストのサイズ取得
sub get_file_db_list_size
{
	return $file_db->get_db_list_size();
}

# 指定IDからオフセットで指定された位置のファイルIDを返す
#  param[in] : ファイルID
#  param[in] : オフセット位置
#  return    : ファイルID(''の場合は該当なし)
sub get_file_id
{
	my ($file_id, $offset) = @_;
	my $index_no = -1;
	my $ret_id   = '';
	
	if($file_id ne '')
	{
		$index_no = get_file_index($file_id);
	}
	
	if($index_no >= 0)
	{
		$index_no = $file_db->search_db_list($index_no, $offset);
		
		if($index_no >= 0)
		{
			$ret_id = get_file_data($index_no, 'id');
		}
	}
	
	return $ret_id;
}

sub file_error_add_error_msg
{
	my $result = $_[0];
	
	if($result & ERROR_FLAG_FILE_ID)
	{
		# ファイルIDエラー
		add_error_msg(\$main::res_string{'file_id_error'});
	}
	if($result & ERROR_FLAG_FILE_HANDLE)
	{
		# ファイルハンドルエラー
		add_error_msg(\$main::res_string{'file_handle_error'});
	}
	if($result & ERROR_FLAG_FILE_NAME)
	{
		# ファイル名エラー
		add_error_msg(\$main::res_string{'file_name_error'});
	}
	if($result & ERROR_FLAG_FILE_SIZE)
	{
		# ファイルサイズエラー
		add_error_msg(\$main::res_string{'file_size_error'});
	}
	if($result & ERROR_FLAG_FILE_COMMENT)
	{
		# ファイルコメントエラー
		add_error_msg(\$main::res_string{'file_comment_error'});
	}
	if($result & ERROR_FLAG_FILE_OVERLAP)
	{
		# ファイル重複エラー
		add_error_msg(\$main::res_string{'file_overlap_error'});
	}
	if($result & ERROR_FLAG_FILE_MAKE_DIR)
	{
		# ファイルディレクトリ生成エラー
		add_error_msg(\$main::res_string{'file_make_dir_error'});
	}
	if($result & ERROR_FLAG_FILE_SAVE)
	{
		# ファイル保存エラー
		add_error_msg(\$main::res_string{'file_save_error'});
	}
	if($result & ERROR_FLAG_FILE_DELETE)
	{
		# ファイル削除エラー
		add_error_msg(\$main::res_string{'file_delete_error'});
	}
	if($result & ERROR_FLAG_FILE_DB_SAVE)
	{
		# ファイルDB保存エラー
		add_error_msg(\$main::res_string{'file_db_save_error'});
	}
	if($result & ERROR_FLAG_FILE_DB_REGIST)
	{
		# ファイルDB登録エラー
		add_error_msg(\$main::res_string{'file_db_regist_error'});
	}
}

1;
